# AQUILO

## Software for Student Riding Competitions

Aquilo faciliates many of the office tasks during a student riding competition:  
It maintains the data of riders and teams, generates drawing lots, starting lists and differences for each round. Final results can be calculated for different competition types and displayed in various ways. Many of the rules for CHIU/SRNC, WUEC and German CHUs are supported.

## Get it running (locally)

Aquilo utilizes gradle to automate the build process.  
A basic starter is delivered with the code in form of the gradle-wrapper.jar and the platform dependent build script gradlew (.bat if you are on windows).

To start Aquilo and see your recent changes, run:

    ./gradlew(.bat) run

if you want to commit and test yout changes against the Unit tests, run:

    ./gradlew(.bat) check

to build a finalized .jar file, run:

    ./gradlew(.bat) jar

You can find your finalized jar file in *./build/libs*


## Get it running (global)

We use an automated build pipeline for Aquilo!
That means, every commited and pushed change will be automatically validated if it builds, runs and has the expected behavior regarding ranking- and file-tests.

Finally, when all tests passed and your feature is finished, we encourage you to open a pull/merge request to the development branch. That way code quality and correctness can be assured and errors may hopefully be detected before they are released to production.


