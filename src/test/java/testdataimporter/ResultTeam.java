/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package testdataimporter;

public class ResultTeam {

    private final String teamName;
    private final double state;
    private final double teamDrMark;
    private final double diff;
    private final double teamDrPoints;
    private final double points;

    ResultTeam (String name, double state, double mark, double diff, double teamDrPoints, double points){
        this.teamName = name;
        this.state = state;
        this.teamDrMark = mark;
        this.diff = diff;
        this.teamDrPoints = teamDrPoints;
        this.points = points;
    }

    public String getTeamName() {
        return teamName;
    }

    public double[] getResultArray(){
        return new double[]{state,teamDrMark,diff,teamDrPoints,points};
    }


}
