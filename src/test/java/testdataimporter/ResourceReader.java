/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package testdataimporter;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Objects;

public class ResourceReader {

    public ArrayList<ChuResult> readTeamDrData() throws Exception {

        File csvFile = new File(Objects.requireNonNull(getClass().getResource("/csv")).toURI());
        File[] csvContent = csvFile.listFiles();
        ArrayList<ChuResult> testData = new ArrayList<>();

        assert csvContent != null;
        for (File file : csvContent) {
            File data = new File(file.toString());
            testData.add(parseCSV(data));
        }

        return testData;

    }

    private ChuResult parseCSV (File data) throws Exception {
        ArrayList<ResultTeam> resTeams = new ArrayList<>();
        FileInputStream fin = new FileInputStream(data);
        BufferedReader br = new BufferedReader(new InputStreamReader(fin, StandardCharsets.UTF_8));

        String currLine;

        String name = br.readLine();
        name = name.replace(";","");
        br.readLine();

        while ((currLine = br.readLine()) != null){
            ResultTeam curr = chopString(currLine);
            resTeams.add(curr);
        }

        br.close();
        fin.close();

        return new ChuResult(name,resTeams);

    }

    private static int state;

    private static ResultTeam chopString(String resultData){
        state = 0;
        String[] s2Chop = resultData.split(";");
        double[] d2ret = new double[4];
        for (int i = 1; i < s2Chop.length; i++) {
            d2ret[i-1] = sPd(s2Chop[i]);
        }
        return new ResultTeam(s2Chop[0],state,d2ret[0],d2ret[1],d2ret[2],d2ret[3]);
    }

    private static double sPd (String parse){
        parse = parse.replace(",",".");
        try {
            return Double.parseDouble(parse);
        } catch (NumberFormatException e) {
            switch (parse){
                case "elim" :{
                    parse = "0.0";
                    state = 1;
                    break;
                }
                case "reti" :{
                    parse = "0.0";
                    state = 2;
                    break;
                }
                case "noRank":{
                    parse = "0.0";
                    state = 3;
                    break;
                }
                default:
                    state = 0;
                    break;
            }
            return Double.parseDouble(parse);
        }
    }
}
