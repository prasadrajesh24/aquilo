/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.ranking;

import aquilo.entity.Competition;
import aquilo.entity.ResultStyle;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.util.Objects;


public class GermanyTeamDressageMarkSorterTest {

    @Test
    public void calculateDiffs() throws Exception {
        Competition.readFromXml(new File(Objects.requireNonNull(getClass().getResource("/chu/CHU Aachen.chu")).toURI()));
        PointsAndRanking.assignAll();
        Competition.teams.sort(new TeamCombinedComparator());

        double[] expectedDiff = {-0.3,0.1,-0.3,-0.1,-1.5,0.3,0.3,0.3,-0.3,1.5,1.8,0.6,0.5,0.4,0.5}; //todo: write function that reads expected Results from input File
        for (int i = 0; i < Competition.teams.size(); i++) {
            assertEquals(expectedDiff[i],Competition.teams.get(i).dressageResult.getDiff(ResultStyle.STYLE),0);
        }

    }

}