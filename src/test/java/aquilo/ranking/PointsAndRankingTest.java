/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.ranking;

import aquilo.entity.Competition;
import aquilo.entity.Team;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.opentest4j.AssertionFailedError;
import testdataimporter.ChuResult;
import testdataimporter.ResourceReader;

import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

public class PointsAndRankingTest {

    private ArrayList<ChuResult> dataList= new ArrayList<>();

    @BeforeEach
    public void setup() {
        try {
            ResourceReader r = new ResourceReader();
            dataList = r.readTeamDrData();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void assignAll() {
        try {
            for (ChuResult chuResult : dataList) {
                String curr = chuResult.getName();
//                System.out.println(curr);
                int noOfTeams = chuResult.getTeams().size();

                File chuFile = new File(Objects.requireNonNull(getClass().getResource("/chu/" + curr + ".chu")).toURI());

                Competition.readFromXml(Objects.requireNonNull(chuFile));
                PointsAndRanking.assignAll();
                for (int j = 0; j < noOfTeams; j++) {
//                    for (int i = 0; i < Competition.teams.size(); i++) {
//                        allDefinite(Competition.teams.get(i));
//                    }
                    Competition.teams.sort(new TeamCombinedComparator());

                    String team = chuResult.getTeams().get(j).getTeamName();
                    double[] expected = chuResult.getTeams().get(j).getResultArray();
                    double[] actual = new double[]{ Competition.teams.get(j).dressageResult.getState(),
                                                    Competition.teams.get(j).dressageResult.getResult(0),
                                                    Competition.teams.get(j).dressageResult.getDiff(0),
                                                    Competition.teams.get(j).teamDrPoints,
                                                    Competition.teams.get(j).points[0]};

                    try {
                        assertArrayEquals(expected, actual, 0, curr + " Team " + team);
                    } catch (AssertionFailedError arrayComparisonFailure) {
                        if (expected[0] == actual[0]){
                            assertEquals(expected[3],actual[3],0);
                            assertEquals(expected[4],actual[4],0);
                        } else {
                            fail(arrayComparisonFailure.getMessage());
                        }
                    }
                }
            }
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    private void allDefinite(Team t) {
        assertArrayEquals(new boolean[]{true,true,true}, t.rankDefinite);
        for (int i = 0; i < t.getRiders().size(); i++) {
            assertArrayEquals(new boolean[]{true,true,true}, t.getRider(i).rankDefinite);
        }
    }
}