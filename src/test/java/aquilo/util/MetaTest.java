/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.util;

import org.semver4j.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import java.time.*;

public class MetaTest {

    @Test
    public void setExpireDate_Test() {
        assertEquals(Meta.setExpireDate(LocalDate.of(2021,1,1)), LocalDate.of(2021,6,Month.JUNE.maxLength()));
        assertEquals(Meta.setExpireDate(LocalDate.of(2021,6,10)), LocalDate.of(2021,12,Month.DECEMBER.maxLength()));
        assertEquals(Meta.setExpireDate(LocalDate.of(2021,8,15)), LocalDate.of(2021,12,Month.DECEMBER.maxLength()));
        assertEquals(Meta.setExpireDate(LocalDate.of(2021,12,10)), LocalDate.of(2022,6,Month.JUNE.maxLength()));
        assertEquals(Meta.setExpireDate(LocalDate.of(2022,1,1)), LocalDate.of(2022,6,Month.JUNE.maxLength()));
    }

    @Test
    public void loadVersionTest() {
        Semver property = Meta.getVersion();
        System.out.println(property.getVersion());
        assertEquals(new Semver("1.0.0"),property);
        assertNotEquals(new Semver("1.0.1"),property);
        assertNotEquals(new Semver("0.8.5"),property);
    }

    @Test
    public void compareVersionTest() {
        assertEquals(0,VersionCheck.compareVersion(new Semver("0.100.0"), new Semver("0.100.0"))); //equal
        assertEquals(-1,VersionCheck.compareVersion(new Semver("0.99.0"), new Semver("0.100.0"))); //minor ahead
        assertEquals(3,VersionCheck.compareVersion(new Semver("1.0.0"), new Semver("0.100.0"))); // major behind
        assertEquals(2,VersionCheck.compareVersion(new Semver("1.100.0"), new Semver("1.98.7"))); // two minor behind
        assertEquals(1,VersionCheck.compareVersion(new Semver("0.99.11"), new Semver("0.99.7"))); // patch behind
        assertEquals(-1,VersionCheck.compareVersion(new Semver("0.100.0"), new Semver("0.100.0-beta1")));
        assertEquals(4,VersionCheck.compareVersion(new Semver("1.100.0"), new Semver("1.98.7-beta2")));
        //assertEquals(2,VersionCheck.compareVersion(new Semver("1.100.0-beta2"), new Semver("1.100.0-beta1")));
    }

}
