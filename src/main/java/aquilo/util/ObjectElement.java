/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.util;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.w3c.dom.*;

public class ObjectElement {

    private static final Logger logger = LoggerFactory.getLogger(ObjectElement.class);

    private Element e;

    public ObjectElement(Node n) {
        e = (Element)n;
    }

    public Element getElement() {
        return e;
    }

    public Element getChildElement(String tagName) throws Exception {
        Node child = e.getFirstChild();
        while( child != null ) {
            if( child.getNodeName().equals(tagName) ) {
                return (Element)child;
            }
            child = child.getNextSibling();
        }
	    String msg = "XML read: <"+e.getNodeName()+"> contains no <"+tagName+"> element";
	    logger.warn(msg);
	    throw new Exception(msg);
    }

    public String getField(String tagName) throws Exception {
        // a bit complicated here; assumes parser did not resolve entityRefs (early JAXP)
        // process arbitrary sequence of text and entityRef nodes
        Node node = getChildElement(tagName).getFirstChild();
        if( node == null ) return "";
        StringBuffer sb = new StringBuffer(30);
        do {
            if(node.getNodeType()==Node.TEXT_NODE)
                sb.append(node.getNodeValue());
            else if(node.getNodeType()==Node.ENTITY_REFERENCE_NODE)
                sb.append(node.getFirstChild().toString());
            else
                break;
            node = node.getNextSibling();
        }
        while(node != null);
        return sb.toString();
    }
    
    public int getIntField(String tagName) throws Exception {
        String val = "";
        try {
            val = getField(tagName).trim();
            return Integer.parseInt(val);
        }
        catch(NumberFormatException ex) {
    	    String msg = "XML read: "+
    	        "Bad number format \""+val+"\" for <"+e.getNodeName()+">.<"+tagName+">";
    	    logger.warn(msg);
    	    throw new Exception(msg);
        }
    }
    public double getDoubleField(String tagName) throws Exception {
        String val = "";
        try {
            val = getField(tagName);
            return Double.parseDouble(val);
        }
        catch(NumberFormatException ex) {
    	    String msg = "XML read: "+
    	        "Bad number format \""+val+"\" for <"+e.getNodeName()+">.<"+tagName+">";
    	    logger.warn(msg);
    	    throw new Exception(msg);
        }
    }

    public boolean getBoolField(String tagName) throws Exception {
        String val = "";
        try {
            val = getField(tagName);
            return Boolean.parseBoolean(val);
        }
        catch(NumberFormatException ex) {
            String msg = "XML read: "+
                    "Bad bool format \""+val+"\" for <"+e.getNodeName()+">.<"+tagName+">";
            logger.warn(msg);
            throw new Exception(msg);
        }
    }

    public String getAttribute(String attrName) {
        return e.getAttribute(attrName);
    }

    public double getDoubleAttribute(String attrName) throws Exception {
        String val = "";
        try {
            val = e.getAttribute(attrName).trim();
            return Double.parseDouble(val);
        }
        catch(NumberFormatException ex) {
    	    String msg = "XML read: "+
    	        "Bad number format in attribute: <"+e.getNodeName()+" "+attrName+"='"+val+"'>";
    	    logger.warn(msg);
    	    throw new Exception(msg);
        }
    }

    public int getIntAttribute(String attrName) throws Exception {
        String val = "";
        try {
            val = e.getAttribute(attrName).trim();
            return Integer.parseInt(val);
        }
        catch(NumberFormatException ex) {
    	    String msg = "XML read: "+
    	        "Bad number format in attribute: <"+e.getNodeName()+" "+attrName+"='"+val+"'>";
    	    logger.warn(msg);
    	    throw new Exception(msg);
        }
    }

    public String getTagName() {
        return e.getTagName();
    }


}
