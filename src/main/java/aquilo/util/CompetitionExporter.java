/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.util;

import aquilo.entity.Competition;
import aquilo.entity.Discipline;
import aquilo.gui.MainWin;
import aquilo.gui.MessageWindow;
import aquilo.gui.StilpreisDialog;
import aquilo.htmlout.*;
import aquilo.ranking.TeamCombinedComparator;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public final class CompetitionExporter {

    private static final Logger logger = LoggerFactory.getLogger(CompetitionExporter.class);

    public static void exportCompetition(){
        if (Competition.isCompleted()){
            try {
                File ExportSrc = new File(MainWin.fileHandling.getFile().getParent() + File.separator + HtmlOutput.stdExportPath);

                HtmlOutput.isExport = true;
                HtmlOutput.showHtmlMsg = false;

                if (Competition.isCHU_GER()){
                    int done;
                    do {
                        StilpreisDialog SpDia = new StilpreisDialog();
                        boolean ignored = SpDia.getStilpreisData();
                        if (!ignored){
                            DARStilpreise.writeFile(null);
                            done = 0;
                        }else {
                            done = MessageWindow.showNoStilpreis();
                        }
                    }while (done != 0);
                }

                BasicIndividualRanking.writeHtmlFile(Discipline.getForId("DR"),null);
                BasicIndividualRanking.writeHtmlFile(Discipline.getForId("SJ"),null);
                TeamResultDetails.writeHtmlFile(null, new TeamCombinedComparator());
                RankingOverview.writeHtmlFile(null);
                ComputerReadableResults.writeFile(null);
                File penFile;
                try {
                    penFile = Penalties.getPenalitesFile();
                    copyFile(penFile,new File(ExportSrc + File.separator + "penalties.txt"));
                } catch (NullPointerException e) {
                    logger.info("no penalties");
                }
                copyFile(MainWin.fileHandling.getFile(),new File(ExportSrc + File.separator + Competition.name + ".chu"));

                try {
                    ZipExport.zipDir(MainWin.fileHandling.getFile().getParent() + File.separator + Competition.name + ".zip", ExportSrc.toString());
                    logger.info("Creating Zip-File: " + MainWin.fileHandling.getFile().getParent() + File.separator + Competition.name + ".zip");
                }catch (Exception ex){
                    System.err.println("Error while creating ZipFile" + ex);
                    ex.printStackTrace();
                }

                HtmlOutput.isExport = false;
                HtmlOutput.showHtmlMsg = true;

                try {
                    //Deleting the directory recursively.
                    delete(ExportSrc);
                    logger.info("Directory has been deleted recursively!");
                } catch (IOException IOex) {
                    String msg = "Problem occurred while deleting the directory : " + ExportSrc.getAbsolutePath();
                    logger.warn(msg);
                    IOex.printStackTrace();
                    throw new Exception(msg);
                }

                logger.info("Export successful");
                MessageWindow.ExportSuccessful(ExportSrc.getParent());

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null,ex.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
                ex.printStackTrace();
                HtmlOutput.isExport = false;
                HtmlOutput.showHtmlMsg = true;
            }

        } else {
            MessageWindow.showCompetitionIsNotCompleted();
        }
    }

    private static void copyFile(File src, File dest) throws IOException {
        Files.copy(src.toPath(),dest.toPath(), StandardCopyOption.REPLACE_EXISTING);
    }

    private static void delete(File file) throws IOException {

        for (File childFile : file.listFiles()) {

            if (childFile.isDirectory()) {
                delete(childFile);
            } else {
                if (!childFile.delete()) {
                    throw new IOException();
                }
            }
        }

        if (!file.delete()) {
            throw new IOException();
        }
    }

}
