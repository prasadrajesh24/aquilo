/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.util;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.semver4j.*;
import org.json.JSONObject;
import org.json.JSONException;

import javax.net.ssl.SSLHandshakeException;
import javax.swing.*;
import java.io.IOException;
import java.net.*;

public class VersionCheck {

    private static final Logger logger = LoggerFactory.getLogger(VersionCheck.class);

    private static final String BASE_URL = "https://studentriding.com";
    private static final String versionTestURL = BASE_URL + "/aquilo_params.json";
    private static final String downloadURL = BASE_URL;

    public static void checkVersion() {
        int diff;
        Semver latest;
        try {
            URL vTestUrl = new URL(versionTestURL);

            JSONObject json = null;
            try {
                json = JsonReader.readJsonFromUrl(vTestUrl);
            } catch (SSLHandshakeException sslhEx){
                sslhEx.printStackTrace();
                logger.warn("SSL Exception");
                //https://stackoverflow.com/questions/55439599/sslhandshakeexception-with-jlink-created-runtime
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }

            if (json == null){
                logger.warn("Connection timed out");
                return;
            }

            latest = getCurrentVersion(json);
            diff = compareVersion(latest, Meta.programVersion);

            switch (diff) {
                case -1:
                    showBetaDialog();
                    break;
                case 2:
                    showVersionUpdateDialog("There is a new " + Meta.name + " Version! \n" +
                                    "Your Version ("+ Meta.programVersion +") is still supported,\n" +
                                    "but there are new features and bugfixes in Version " + latest,true);
                    break;
                case 3:
                    showVersionUpdateDialog("You are at least one Major Version behind!\n" +
                            "Due to incompatibility the program will now stop!\n" +
                            "Please download the latest Version",false);
                    System.exit(1);
                case 4:
                    showVersionUpdateDialog("You have a beta version,\n" +
                            "which is behind the latest release!\n" +
                            "Due to incompatibility the program will now stop!\n" +
                            "Please download the latest Version",false);
                    System.exit(1);
                case 9:
                    showVersionUpdateDialog("There is a new " + Meta.name + " Version! \n"+
                                    "You are using version "+ Meta.programVersion + "\n"+
                                    "the current version is "+ latest + "\n" +
                                    "Please update now, to avoid that your results \n" +
                                    "are rejected due to an outdated version.\n",true);
                    break;
            }
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public static int compareVersion(Semver latest, Semver self) {

        if (self.isLowerThanOrEqualTo(latest)) {
            switch (self.diff(latest)) {
                case PATCH:
                    logger.info("Supported (at least one patch behind)");
                    return 1;
                case MINOR:
                    if (self.isStable()){
                        logger.info("Supported but old (at least one minor version behind)");
                        return 2;
                    }
                    logger.error("Unsupported! (Beta behind production)");
                    return 4;
                case MAJOR:
                    logger.error("Unsupported! (at least one major version behind)");
                    return 3;
                case NONE:
                    logger.info("Newest official Version");
                    return 0;
            }
        }

        if (!self.isStable() || self.isGreaterThan(latest)) {
            logger.info("Beta Version");
            return -1;
        }

        logger.error("Outdated Version");
        return 9;

    }

    private static Semver getCurrentVersion(JSONObject json) {
        JSONObject Versions = json.getJSONArray("activeVersions").getJSONObject(0);
        return new Semver(Versions.getString("current"));
    }

    private static void showVersionUpdateDialog(String message, boolean skipable){
        Object[] options;
        if (skipable) {
            options = new Object[]{"Download now", "Ignore at own risk"};
        } else {
            options = new Object[]{"Download now"};
        }
        try {
            int selected = JOptionPane.showOptionDialog(null,
                    message,
                    "Update Info",
                    JOptionPane.DEFAULT_OPTION,
                    JOptionPane.INFORMATION_MESSAGE,null,options,options[0]);
            if (selected==0){
                URI downURI = new URI(downloadURL);
                WebBrowser.browse(downURI);
                System.exit(0);
            }
        }
        catch (Exception ex){
            ex.printStackTrace();
            logger.error("Error with DAR-URI");
        }
    }

    private static void showBetaDialog(){
        JOptionPane.showMessageDialog(null,
        "You are currently using a BETA-Version! (" + Meta.programVersion +")\n"+
                "Be aware that this version may be used \n" +
                "with caution as there may be errors in it. \n" +
                "If you got this version by accident,\n" +
                "please inform the developers.",
        "Beta Version",JOptionPane.INFORMATION_MESSAGE);
    }

}
