/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.util;

import aquilo.entity.Competition;

import java.util.*;

public class CityComp {

    private final LinkedHashMap<String, String> cityMap = new LinkedHashMap<>();
    private final LinkedHashMap<String, String> countryMap = new LinkedHashMap<>();
    private final ArrayList<String[]> CityList = new ArrayList<>();

    public CityComp() {
        initCityMap();
        initCountryMap();
    }

    private void initCityMap() {
        cityMap.put("AC", "Aachen");
        cityMap.put("A", "Augsburg");
//        cityMap.put("BT", "Bayreuth");
        cityMap.put("B", "Berlin");
        cityMap.put("BI", "Bielefeld");
        cityMap.put("BODO", "Bochum-Dortmund");
        cityMap.put("BN", "Bonn");
        cityMap.put("BS", "Braunschweig");
        cityMap.put("HB", "Bremen");
        cityMap.put("BHH", "Bundeswehr Hamburg");
        cityMap.put("DA", "Darmstadt");
        cityMap.put("DD", "Dresden");
        //cityMap.put( "D","Duisburg-Essen");
        cityMap.put("D", "D\u00fcsseldorf");
        cityMap.put("ER", "Erlangen-Nürnberg");
//        cityMap.put("HA", "Fernuni Hagen");
        cityMap.put("F", "Frankfurt");
//        cityMap.put("FR", "Freiburg");
        cityMap.put("GI", "Gie\u00dfen");
        cityMap.put("G\u00d6", "G\u00f6ttingen");
        cityMap.put("HAL", "Halle");
        cityMap.put("HH", "Hamburg");
        cityMap.put("H", "Hannover");
        //cityMap.put("HD", "Heidelberg");
        cityMap.put("HD-MA", "Heidelberg-Mannheim");
        cityMap.put("HOH", "Hohenheim");
        cityMap.put("IL", "Ilmenau");
        cityMap.put("IS", "Iserlohn");
        cityMap.put("KA", "Karlsruhe");
        cityMap.put("KL", "Kaiserslautern");
        cityMap.put("KS", "Kassel");
        cityMap.put("KI", "Kiel");
//        cityMap.put("KO", "Koblenz");
        cityMap.put("K", "K\u00f6ln");
        cityMap.put("KN", "Konstanz");
        cityMap.put("L", "Leipzig");
        cityMap.put("LG", "L\u00fcneburg");
        cityMap.put("MD", "Magdeburg");
        cityMap.put("MZ", "Mainz");
        //cityMap.put("MA", "Mannheim");
        cityMap.put("MR", "Marburg");
        cityMap.put("M", "M\u00fcnchen");
        cityMap.put("MS", "M\u00fcnster");
        cityMap.put("NT", "N\u00fcrtingen");
        cityMap.put("OLD", "Oldenburg");
        cityMap.put("OS", "Osnabr\u00fcck");
        cityMap.put("PB", "Paderborn");
        cityMap.put("R", "Regensburg");
        cityMap.put("SB", "Saarbr\u00fccken");
        cityMap.put("S", "Stuttgart");
        cityMap.put("T\u00dc", "T\u00fcbingen");
        cityMap.put("UL", "Ulm");
        cityMap.put("TR", "Trier");
//        cityMap.put("VEC", "Vechta");
//        cityMap.put("W", "Wuppertal");
        cityMap.put("W\u00dc", "W\u00fcrzburg");
    }

    private void initCountryMap() {
        countryMap.put("AFG", "Afghanistan");
        countryMap.put("ALB", "Albania");
        countryMap.put("ALG", "Algeria");
        countryMap.put("AND", "Andorra");
        countryMap.put("ANG", "Angola");
        countryMap.put("ANT", "Antigua and Barbuda");
        countryMap.put("ARG", "Argentina");
        countryMap.put("ARM", "Armenia");
        countryMap.put("ARU", "Aruba");
        countryMap.put("ASA", "American Samoa");
        countryMap.put("AUS", "Australia");
        countryMap.put("AUT", "Austria");
        countryMap.put("AZE", "Azerbaijan");
        countryMap.put("BAH", "Bahamas");
        countryMap.put("BAN", "Bangladesh");
        countryMap.put("BAR", "Barbados");
        countryMap.put("BDI", "Burundi");
        countryMap.put("BEL", "Belgium");
        countryMap.put("BEN", "Benin");
        countryMap.put("BER", "Bermuda");
        countryMap.put("BHU", "Bhutan");
        countryMap.put("BIH", "Bosnia and Herzegovina");
        countryMap.put("BIZ", "Belize");
        countryMap.put("BLR", "Belarus");
        countryMap.put("BOL", "Bolivia");
        countryMap.put("BOT", "Botswana");
        countryMap.put("BRA", "Brazil");
        countryMap.put("BRN", "Bahrain");
        countryMap.put("BRU", "Brunei");
        countryMap.put("BUL", "Bulgaria");
        countryMap.put("BUR", "Burkina Faso");
        countryMap.put("CAF", "Central African Republic");
        countryMap.put("CAM", "Cambodia");
        countryMap.put("CAN", "Canada");
        countryMap.put("CAY", "Cayman Islands");
        countryMap.put("CGO", "Republic of the Congo");
        countryMap.put("CHA", "Chad");
        countryMap.put("CHI", "Chile");
        countryMap.put("CHN", "China");
        countryMap.put("CIV", "Ivory Coast");
        countryMap.put("CMR", "Cameroon");
        countryMap.put("COD", "Democratic Republic of the Congo");
        countryMap.put("COK", "Cook Islands");
        countryMap.put("COL", "Colombia");
        countryMap.put("COM", "Comoros");
        countryMap.put("CPV", "Cape Verde");
        countryMap.put("CRC", "Costa Rica");
        countryMap.put("CRO", "Croatia");
        countryMap.put("CUB", "Cuba");
        countryMap.put("CYP", "Cyprus");
        countryMap.put("CZE", "Czech Republic");
        countryMap.put("DEN", "Denmark");
        countryMap.put("DJI", "Djibouti");
        countryMap.put("DMA", "Dominica");
        countryMap.put("DOM", "Dominican Republic");
        countryMap.put("ECU", "Ecuador");
        countryMap.put("EGY", "Egypt");
        countryMap.put("ERI", "Eritrea");
        countryMap.put("ESA", "El Salvador");
        countryMap.put("ESP", "Spain");
        countryMap.put("EST", "Estonia");
        countryMap.put("ETH", "Ethiopia");
        countryMap.put("FIJ", "Fiji");
        countryMap.put("FIN", "Finland");
        countryMap.put("FRA", "France");
        countryMap.put("FSM", "Federated States of Micronesia");
        countryMap.put("GAB", "Gabon");
        countryMap.put("GAM", "The Gambia");
        countryMap.put("GBR", "Great Britain");
        countryMap.put("GBS", "Guinea-Bissau");
        countryMap.put("GEO", "Georgia");
        countryMap.put("GEQ", "Equatorial Guinea");
        countryMap.put("GER", "Germany");
        countryMap.put("GHA", "Ghana");
        countryMap.put("GRE", "Greece");
        countryMap.put("GRN", "Grenada");
        countryMap.put("GUA", "Guatemala");
        countryMap.put("GUI", "Guinea");
        countryMap.put("GUM", "Guam");
        countryMap.put("GUY", "Guyana");
        countryMap.put("HAI", "Haiti");
        countryMap.put("HKG", "Hong Kong, China");
        countryMap.put("HON", "Honduras");
        countryMap.put("HUN", "Hungary");
        countryMap.put("INA", "Indonesia");
        countryMap.put("IND", "India");
        countryMap.put("IRI", "Iran");
        countryMap.put("IRL", "Ireland");
        countryMap.put("IRQ", "Iraq");
        countryMap.put("ISL", "Iceland");
        countryMap.put("ISR", "Israel");
        countryMap.put("ISV", "Virgin Islands");
        countryMap.put("ITA", "Italy");
        countryMap.put("IVB", "British Virgin Islands");
        countryMap.put("JAM", "Jamaica");
        countryMap.put("JOR", "Jordan");
        countryMap.put("JPN", "Japan");
        countryMap.put("KAZ", "Kazakhstan");
        countryMap.put("KEN", "Kenya");
        countryMap.put("KGZ", "Kyrgyzstan");
        countryMap.put("KIR", "Kiribati");
        countryMap.put("KOR", "South Korea");
        countryMap.put("KOS", "Kosovo");
        countryMap.put("KSA", "Saudi Arabia");
        countryMap.put("KUW", "Kuwait");
        countryMap.put("LAO", "Laos");
        countryMap.put("LAT", "Latvia");
        countryMap.put("LBA", "Libya");
        countryMap.put("LBN", "Lebanon");
        countryMap.put("LBR", "Liberia");
        countryMap.put("LCA", "Saint Lucia");
        countryMap.put("LES", "Lesotho");
        countryMap.put("LIE", "Liechtenstein");
        countryMap.put("LTU", "Lithuania");
        countryMap.put("LUX", "Luxembourg");
        countryMap.put("MAD", "Madagascar");
        countryMap.put("MAR", "Morocco");
        countryMap.put("MAS", "Malaysia");
        countryMap.put("MAW", "Malawi");
        countryMap.put("MDA", "Moldova");
        countryMap.put("MDV", "Maldives");
        countryMap.put("MEX", "Mexico");
        countryMap.put("MGL", "Mongolia");
        countryMap.put("MHL", "Marshall Islands");
        countryMap.put("MKD", "North Macedonia");
        countryMap.put("MLI", "Mali");
        countryMap.put("MLT", "Malta");
        countryMap.put("MNE", "Montenegro");
        countryMap.put("MON", "Monaco");
        countryMap.put("MOZ", "Mozambique");
        countryMap.put("MRI", "Mauritius");
        countryMap.put("MTN", "Mauritania");
        countryMap.put("MYA", "Myanmar");
        countryMap.put("NAM", "Namibia");
        countryMap.put("NCA", "Nicaragua");
        countryMap.put("NED", "Netherlands");
        countryMap.put("NEP", "Nepal");
        countryMap.put("NGR", "Nigeria");
        countryMap.put("NIG", "Niger");
        countryMap.put("NOR", "Norway");
        countryMap.put("NRU", "Nauru");
        countryMap.put("NZL", "New Zealand");
        countryMap.put("OMA", "Oman");
        countryMap.put("PAK", "Pakistan");
        countryMap.put("PAN", "Panama");
        countryMap.put("PAR", "Paraguay");
        countryMap.put("PER", "Peru");
        countryMap.put("PHI", "Philippines");
        countryMap.put("PLE", "Palestine");
        countryMap.put("PLW", "Palau");
        countryMap.put("PNG", "Papua New Guinea");
        countryMap.put("POL", "Poland");
        countryMap.put("POR", "Portugal");
        countryMap.put("PRK", "North Korea");
        countryMap.put("PUR", "Puerto Rico");
        countryMap.put("QAT", "Qatar");
        countryMap.put("ROU", "Romania");
        countryMap.put("RSA", "South Africa");
        countryMap.put("RUS", "Russia");
        countryMap.put("RWA", "Rwanda");
        countryMap.put("SAM", "Samoa");
        countryMap.put("SEN", "Senegal");
        countryMap.put("SEY", "Seychelles");
        countryMap.put("SGP", "Singapore");
        countryMap.put("SKN", "Saint Kitts and Nevis");
        countryMap.put("SLE", "Sierra Leone");
        countryMap.put("SLO", "Slovenia");
        countryMap.put("SMR", "San Marino");
        countryMap.put("SOL", "Solomon Islands");
        countryMap.put("SOM", "Somalia");
        countryMap.put("SRB", "Serbia");
        countryMap.put("SRI", "Sri Lanka");
        countryMap.put("SSD", "South Sudan");
        countryMap.put("STP", "São Tomé and Príncipe");
        countryMap.put("SUD", "Sudan");
        countryMap.put("SUI", "Switzerland");
        countryMap.put("SUR", "Suriname");
        countryMap.put("SVK", "Slovakia");
        countryMap.put("SWE", "Sweden");
        countryMap.put("SWZ", "Eswatini");
        countryMap.put("SYR", "Syria");
        countryMap.put("TAN", "Tanzania");
        countryMap.put("TGA", "Tonga");
        countryMap.put("THA", "Thailand");
        countryMap.put("TJK", "Tajikistan");
        countryMap.put("TKM", "Turkmenistan");
        countryMap.put("TLS", "Timor-Leste");
        countryMap.put("TOG", "Togo");
        countryMap.put("TPE", "Chinese Taipei");
        countryMap.put("TTO", "Trinidad and Tobago");
        countryMap.put("TUN", "Tunisia");
        countryMap.put("TUR", "Turkey");
        countryMap.put("TUV", "Tuvalu");
        countryMap.put("UAE", "United Arab Emirates");
        countryMap.put("UGA", "Uganda");
        countryMap.put("UKR", "Ukraine");
        countryMap.put("URU", "Uruguay");
        countryMap.put("USA", "United States");
        countryMap.put("UZB", "Uzbekistan");
        countryMap.put("VAN", "Vanuatu");
        countryMap.put("VEN", "Venezuela");
        countryMap.put("VIE", "Vietnam");
        countryMap.put("VIN", "Saint Vincent and the Grenadines");
        countryMap.put("YEM", "Yemen");
        countryMap.put("ZAM", "Zambia");
        countryMap.put("ZIM", "Zimbabwe");

    }

    public String getAbbreviation(String cityName) {
        if (Competition.isCHU_GER() && !Competition.isDHM) {
            for (Map.Entry<String, String> entry : cityMap.entrySet()) {
                if (entry.getValue().equals(cityName)) {
                    return entry.getKey();
                }
            }
        } else {
            for (Map.Entry<String, String> entry : countryMap.entrySet()) {
                if (entry.getValue().equals(cityName)) {
                    return entry.getKey();
                }
            }
        }
        return null;
    }

    public String getCityName(String abb) {
        if ((Competition.isCHU_GER() && !Competition.isDHM) && cityMap.get(abb) != null) {
            return cityMap.get(abb);
        } else if (Competition.isInternational() && countryMap.get(abb) != null) {
            return countryMap.get(abb).trim();
        }
        return null;
    }

    private void printCityComp() {
        for (Map.Entry<String, String> entry : Competition.isInternational() ? countryMap.entrySet() : cityMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            String[] City = {key, value};
            CityList.add(City);
        }
    }

    public ArrayList<String> getCities() {
        ArrayList<String> cities = new ArrayList<>();
        for (Map.Entry<String, String> entry : cityMap.entrySet()) {
            String value = entry.getValue();
            cities.add(value);
        }
        return cities;
    }

    public String CityString() {
        printCityComp();
        String arrow = "\u21D2";
        StringBuilder str = new StringBuilder();

        for (int i = 0; i < CityList.size(); i++) {
            String[] temp = CityList.get(i);
            String name = temp[0];
            String abb = temp[1];
            str.append(name).append("  ").append(arrow).append("	    ").append(abb);
            str.append("\n");
        }

        return str.toString();
    }

    public static void main(String[] args) {
        new CityComp().printCityComp();
    }

}



