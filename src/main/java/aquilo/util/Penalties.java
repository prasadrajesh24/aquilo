/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.util;

import aquilo.entity.Competition;
import aquilo.entity.Team;
import aquilo.gui.MainWin;
import aquilo.gui.MessageWindow;
import aquilo.htmlout.HtmlOutput;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;


public class Penalties extends HtmlOutput {

    private static final Logger logger = LoggerFactory.getLogger(Penalties.class);

    public static boolean penalties;
    public static String displayName;
    public static String displayShortName;
    private static final String penFileName = "penalties.txt";

    public static File getPenalitesFile() throws NullPointerException{
        File rootPath;

        rootPath = MainWin.fileHandling.getFile();

        if (!checkPenFileExist()) {
            throw new NullPointerException("Penalties File does not exist!");
        }

        return new File(rootPath.getParent() + File.separator + "htmlOutput" + File.separator + penFileName);
    }

    public static void generate() throws Exception {

        File penaltyFile = putInOutputDir(penFileName);

        if (checkPenFileExist()) {
            int sel = MessageWindow.showOverwriteFileDialog();
            if (sel != 0) {
                return;
            }
        }

        PrintStream out = openHtmlOutputFile(penaltyFile);
        out.println("STUDENT RIDING COMPETITION PENALTY FILE, VERSION 1.0");
        out.println(Meta.nameVersionIssueDate());
        out.println(Competition.name);
        out.println("DisplayName: Penalties");
        out.println("ShortName: Pen Pts.");

        out.println();

        for (Team t:Competition.teams) {
            out.println(t.name + ": 0.0");
        }

        penalties = true;
        out.close();
    }

    public static void check() {
        File penaltyFile;
        try {
            penaltyFile = getPenalitesFile();
        } catch (NullPointerException nEx) {
            return;
        }

        HashMap<String,Double> cityPenMap;
        try {
            cityPenMap = readData(penaltyFile);
        } catch (IOException e) {
            logger.warn(e.getMessage(), e);
            return;
        }

        for (int i = 0; i < Competition.noOfTeams(); i++) {
            String cityName = Competition.teams.get(i).name;
            Competition.teams.get(i).setPenaltyPoints(cityPenMap.get(cityName));
        }

        logger.info("Penalties:");
        for (Team t:Competition.teams) {
            logger.info(t.name + ": " + t.penaltyPoints);
        }
    }

    private static HashMap<String,Double> readData(File path) throws IOException {

        HashMap<String,Double> CityPenMap = new HashMap<>();

        FileInputStream fin = new FileInputStream(path);
        BufferedReader br = new BufferedReader(new InputStreamReader(fin, StandardCharsets.UTF_8));

        String currLine;

        while ((currLine = br.readLine()) != null) {

            if (!currLine.contains(":")) {
                continue;
            }

            String[] line = currLine.split(":");
            String city = line[0];
            String penPoints = line[1];

            if (city.equals("DisplayName")) {
                displayName = penPoints;
                continue;
            }
            if (city.equals("ShortName")) {
                displayShortName = penPoints;
                continue;
            }

            CityPenMap.put(city, Double.parseDouble(penPoints));
        }


        br.close();
        fin.close();
        return CityPenMap;

    }

    private static boolean checkPenFileExist() {
        File rootPath = MainWin.fileHandling.getFile();
        File penaltyFile = new File(rootPath.getParent() + File.separator + "htmlOutput" + File.separator + penFileName);

        if (!penaltyFile.exists()) {
            penalties = false;
            return false;
        }

        penalties = true;
        return true;
    }

}
