/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

/*-----------------------------------------------------------------------------
    Fmt: Format strings and numbers: add spaces of left/right side to
    fill a constant length
    
    Caution: This class works on a single *static* character buffer and is
    NOT concurrency proof
    
    Andreas Steidle, 1998
-----------------------------------------------------------------------------*/

package aquilo.util;

import java.util.StringTokenizer;
   

public class Fmt {

	public static int dblwidth = 10;
	public static int intwidth = 5;
	public static final int MAX_EXTEND_WIDTH = 256;
	private static char[] spaces = new char[MAX_EXTEND_WIDTH];
	static {
		for( int i=0; i<spaces.length; i++ ) spaces[i] = ' ';
	}
	
	public static String extL(String s, int totallength) {
		int extendlen = totallength-s.length();
		if( extendlen>0 && extendlen<=MAX_EXTEND_WIDTH )
			return (new String(spaces,0,extendlen))+s;
		else 
			return s;
	}
	public static String extR(String s, int totallength) {
		int extendlen = totallength-s.length();
		if( extendlen>0 && extendlen<=MAX_EXTEND_WIDTH )
			return s+(new String(spaces,0,extendlen));
		else 
			return s;
	}

	public static String fmt(String s, int totallength) {
		return extR(s,totallength);
	}
	public static String fix(String s, int totallength) {
		if(s.length()>totallength) s = s.substring(0,totallength);
		return extR(s,totallength);
	}
	
	public static String fmt(int n, int totallength) {
		return extL(Integer.toString(n),totallength);
	}
	public static String fmt(int n) {
		return extL(Integer.toString(n),intwidth);
	}

	public static String fmt(double x, int totallength) {
		return extL(Double.toString(x),totallength);
	}
	public static String fmt(double x) {
		return extL(Double.toString(x),dblwidth);
	}
	
	public static String fmt(double x, int totallength, int decdig) {
		return fmt(decimalDigits(x,decdig),totallength);
	}
	public static double decimalDigits(double x,int decdig) {
		double blowup = Math.pow(10,decdig);
		return Math.floor(x*blowup+0.5) / blowup;
	}


	public static String cutAtSpaceOrMaxLen(String s, int maxlen) {
	    if(s.length()<=maxlen) return s;
	    int l = s.indexOf(' ',1);
	    if(l==-1 || l>maxlen) return s.substring(0,maxlen)+"..";
	    else return s.substring(0,l);
	}

    public static String toOrdinal(int num) {
        return num+".";
        /*switch(num%10) {
            case 1: return "1st";
            case 2: return "2nd";
            case 3: return "3rd";
            default: return num+"th";
            // !! 11th 12th not considered
        }
        */
    }      
    
    public static String propFontExtL(String num) {
        return (num.length()<2) ? "  "+num : num;
    } 
    
    //------------------------------------------------------------
    
    public static int[] parseIntList(String list) throws NumberFormatException {
        String delimiters = " ,;+";
        
        int n=0;    // count tokens to find array size first
        StringTokenizer st = new StringTokenizer(list,delimiters);
        while(st.hasMoreTokens()) { st.nextToken(); n++; }
        
        st = new StringTokenizer(list,delimiters);
        int[] values = new int[n];
		int i=0;
		while (st.hasMoreTokens()) {
            values[i++] = Integer.parseInt((String)st.nextToken());
            //System.out.println(values[i-1]);
        }
        return values;    
    }

    public static String intArrayToString(int[] values) {
        String str = "";
        if(values.length==0) return str;
        for( int i=0 ; ; i++ ) {
            str += values[i];
            if(i==values.length-1) break;
            str += ',';
        }
        return str;
    }
    
    //==================================================================
    
	public static void main(String[] argv) {
        /*
		System.out.println("["+extL("Hallo",10)+"]");
		System.out.println("["+extR("Hallo",10)+"]");
		
		System.out.println("["+cutAtSpaceOrMaxLen("Andreas Steidle",50)+"]");
		System.out.println("["+cutAtSpaceOrMaxLen("Andreas Steidle",10)+"]");
		System.out.println("["+cutAtSpaceOrMaxLen("AndreasSteidle",10)+"]");
		System.out.println("["+cutAtSpaceOrMaxLen("Andreas Steidle",6)+"]");
        */
        
        int[] intVals = parseIntList(" 1,2 3; 4 ,5 , ,1717  ");
        System.out.println(intArrayToString(intVals));
	}
}

/*
[     Hallo]
[Hallo     ]
*/
