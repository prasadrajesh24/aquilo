/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.util;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.awt.*;
import java.io.*;
import java.net.URI;

public final class WebBrowser {

    private static final Logger logger = LoggerFactory.getLogger(WebBrowser.class);

    public static void open(File htmlFile) throws Exception {
        if( ! htmlFile.canRead() ) {
            String msg = "Could not open "+htmlFile.getPath();
            throw new Exception(msg);
        }

        try {
            Desktop.getDesktop().open(htmlFile);
        }
        catch(UnsupportedOperationException ex) {
            String msg = "Unfortunately your Operating System does not support to auto open "+htmlFile.getPath()+"\n"+
                    "Try to open the file manually in your browser.";
            ex.printStackTrace();
            throw new Exception(msg);
        }
        catch (Exception ex){
            logger.error(ex.getMessage(), ex);
        }

    }

    public static void browse(URI site)throws Exception{
        try {
            Desktop.getDesktop().browse(site);
        }
        catch(UnsupportedOperationException ex) {
            String msg = "Unfortunately your Operating System does not support to auto open "+site.toString()+"\n"+
                    "Try to open the URL manually in your browser.";
            throw new Exception(msg);
        }
        catch (Exception ex){
            logger.error(ex.getMessage(), ex);
        }
    }

    //====================================================================
    // test

    public static void main(String[] args) throws Exception {
        WebBrowser.open(new File("output/RankingOverview.html"));
        WebBrowser.open(new File("xxx"));
    }
}
