/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.util;

import java.util.*;

/**
    Map some technical terms to German if German competition.
    Used only for a few words at init and for printouts.
    For now the GUI will remain in English so there is no need for the use of
    the java.util.ResourceBundle class. Furthermore very likely German will be
    the only additional language to support.
*/

public final class Lang {

    public static final int EN = 0,  DE = 1;
    private static int language = EN;
    
    public static void set(int languageId) { language = languageId; }
    
    private static Map<String,String> german;
    
    static {
        german = new HashMap<>(30);
        german.put("Dressage","Dressur");
        german.put("Showjumping","Springen");
        german.put("Round","Runde");
        german.put("Group","Abteilung");
        german.put("Start","Start");
        german.put("No","Num");
        german.put("Total","Gesamt");
        german.put("Horse","Pferd");
        german.put("Rider","Reiter");
        german.put("Boot","Stiefel");
        german.put("Team","Team");
        german.put("Result","Ergebnis");
        german.put("Results","Ergebnisse");
        german.put("Individual","Einzel");
        german.put("Combined","Kombi");
        german.put("Points","Punkte");
        german.put("Rank","Rang");
        
        german.put("Individual Ranking","Einzelwertung");
        german.put("Team Result Details","Ergebnisse, Details");
        german.put("Results Overview","Ergebnisse");
        german.put("Individual Rankings","Einzelwertungen");
        german.put("Team Ranking","Teamwertung");
        german.put("Preliminary","Vorl\u00e4ufige");
        german.put("Completed Rounds","Vollst\u00e4ndige Runden");
        german.put("ATTENTION: This Round needs ","ACHTUNG: In dieser Runde gibt es ");
        german.put("Lot Number: ","Losnummer: ");

        german.put("Abb.", "K\u00fcrzel");
        german.put("Mark", "Note");
        /*
        german.put("","");
        german.put("","");
        german.put("","");
        */
    }
    
    /** t_ranslate an English term */
    public static String t(String englishWord) {
        String translation;
        switch(language) {
            case EN: return englishWord;
            case DE: translation = german.get(englishWord); break;
            default: return englishWord;
        }
        return (translation!=null) ? translation : englishWord;
    }
    
}

