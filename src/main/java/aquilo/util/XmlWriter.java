/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */


package aquilo.util;

import java.io.PrintStream;

public class XmlWriter {

	private PrintStream out;
	private String identStr;
	public int identWidth;
	private boolean multiLine;
	private boolean emptyLine;

	public XmlWriter(PrintStream out) {
	    this.out = out;
    	identStr = "";
    	identWidth = 2;
	    setMultiLine(true);
	    out.println("<?xml version='1.0' encoding='UTF-8'?>");
	}

    //------------------------------------

    public void writeElement(String tagName, Object value) {
		String s = (value==null) ? "null" : value.toString();
        writeTag(tagName, s);
    }

    public void writeElement(String tagName, long value) {
        writeTag(tagName,Long.toString(value));
    }

    public void writeElement(String tagName, double value) {
        writeTag(tagName,Double.toString(value));
    }

    public void writeEmptyElement(String tagName, String attrsString) {
        ident();
        out.print("<"+tagName+" "+attrsString+"/>");
        newline();
    }

    public void writeTag(String tagName, Object value) {
        ident();
        out.print("<"+tagName+">" + escapeText(value.toString()) + "</"+tagName+">");
        newline();
    }

    public void writeComment(String text) {
        ident();
        out.print("<!-- "+text+" -->");
        newline();
    }
    
    public static String escapeText(String str) {
        return escape( escape(str,'&',"&amp;"), '<',"&lt;" );
    }
    
    public static String escape(String str, char reservedChar, String entity) {
        int entityLength = entity.length();
        int index = 0;
        for(;;) {
            index = str.indexOf(reservedChar,index);
            if(index == -1) return str;
            StringBuffer sb = new StringBuffer(str.length()+entityLength);
            str = sb.append(str).replace(index,index+1,entity).toString();
            index += entityLength;
        }
    }
    
    //------------------------------------

    public void startGroup(String tagName) {
        startGroup(tagName, null);
    }

    public void startGroup(String tagName, String attributes) {
        ident();
        if(attributes!=null)
            out.print('<'+tagName+' '+attributes+'>');
        else
            out.print('<'+tagName+'>');
        newline();
        identStr += "                ".substring(0,identWidth);
    }

    public void startGroup(Class cl) {
        String n = cl.getName();
        startGroup(n.substring(n.lastIndexOf('.')+1), null);
    }

    public void startGroup(Class cl, String attributes) {
        String n = cl.getName();
        startGroup(n.substring(n.lastIndexOf('.')+1), attributes);
    }

    public void endGroup(String tagName) {
        int ilen = identStr.length();
        if(ilen>=identWidth) identStr = identStr.substring(0,ilen-identWidth);
        ident();
        out.println("</"+tagName+">");
        emptyLine = true;
        out.flush();
    }

    public void endGroup(Class cl) {
        String n = cl.getName();
        endGroup(n.substring(n.lastIndexOf('.')+1));
    }

    //------------------------------------

    public void ident() {
        if(!emptyLine) return;
        out.print(identStr);
        emptyLine = false;
    }

	public void setMultiLine(boolean b) {
        multiLine = b;
    }

    public void newline() {
        if(multiLine) {
            emptyLine = true;
            out.println();
        }
        else {
            out.print(" ");
        }
    }

    //==================================================================

	public static void main(String[] args) {
	    XmlWriter xout = new XmlWriter(new PrintStream(System.out));
	    xout.startGroup("mydoc");
	    xout.writeElement("Object","Hallo<");
	    xout.writeElement("int",17);
	    xout.setMultiLine(false);
	    xout.startGroup("abc");
    	    xout.writeElement("Object","Hallo&");
    	    xout.writeElement("int",17);
    	    xout.writeElement("double",17.17);
	    xout.endGroup("abc");
	    xout.startGroup("abc");
    	    xout.writeElement("Object","Hallo");
    	    xout.writeElement("int",17);
    	    xout.writeElement("double",17.17);
	    xout.endGroup("abc");
	    xout.setMultiLine(true);
	    xout.writeElement("double",17.17);
	    xout.endGroup("mydoc");
	}
}

