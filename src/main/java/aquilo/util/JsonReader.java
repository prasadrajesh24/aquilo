/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonReader {

    private static final Logger logger = LoggerFactory.getLogger(JsonReader.class);

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    static JSONObject readJsonFromUrl(URL url) throws IOException, JSONException {
        HttpURLConnection JsonCon = (HttpURLConnection) url.openConnection();
        JsonCon.setConnectTimeout(5000);

        int status = JsonCon.getResponseCode();
        if (status != 200){
            logger.info("Connection failed! " + status);
            JsonCon.disconnect();
            return null;
        }
        else {
            logger.info("Status Code: " + status);
            InputStream is = JsonCon.getInputStream();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
                String jsonText = readAll(rd);
                JSONObject json = new JSONObject(jsonText);
                return json;
            } finally {
                is.close();
                JsonCon.disconnect();
            }
        }
    }

    public static void main(String[] args) throws IOException, JSONException {
        URL testURL = new URL("https://infra.deutscher-akademischer-reiterverband.de/aquilo_params.json");
        JSONObject json = readJsonFromUrl(testURL);
        logger.info(json.toString());
        logger.info("{}",json.getJSONObject("activeversions").get("version_1"));
    }
}