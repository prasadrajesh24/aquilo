/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.util;

import aquilo.entity.Competition;
import org.semver4j.*;

import java.io.IOException;
import java.io.InputStream;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Properties;

/**
    Meta information about the program and its file versions
*/

public final class Meta {

    private static final Properties p = new Properties();

    static {
        init();
    }

    public static final String name        = "AQUILO";
    public static final String description = "Software for Student Riding Competitions";
    public static final String author      = "Andreas Steidle";
    public static final String maintainer  = "Philipp Tegtmeyer";
    public static final String email       = "steidle@equinis.com";
    public static final String website     = "studentriding.com";
    
    public static final Semver programVersion = getVersion();
    public static final String fileVersion    = "1.0";

    private static final int maxDaysToExpireDate = 28;

    private static final DateTimeFormatter dateForm = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    private static final LocalDate issueDate = getIssueDate();
    private static final LocalDate expireDate = setExpireDate(issueDate);
    
    //---------------------------------------------------------
    
    public static String getCopyright() { 
        return "Copyright (C) "+author+" 1999-"+getIssueYear();
    }
    
    public static String nameVersionIssueDate() { 
        return name+"   "+programVersion+"   "+issueDateToString();
    }

    public static String promotion(){
        String is2018Calc = " Quali/DHM2019";
        String isDHM = "DHM";
        if (Competition.QualiDHM2019){
            return name+" "+programVersion+" "+is2018Calc;
        } else if (Competition.isDHM){
            return name+" "+programVersion+" "+isDHM;
        } else {
            return name+" "+programVersion;
        }
    }
    
    //---------------------------------------------------------
    
    public static int getIssueYear() { 
        return issueDate.getYear();
    }
    
    public static String issueDateToString() { 
        return issueDate.format(dateForm);
    }
    
    //---------------------------------------------------------
    
    public static String expireDateToString() { 
        return expireDate.format(dateForm);
    }
    
    public static boolean hasExpired() {
        return LocalDate.now().isAfter(expireDate);
    }
    
    public static String expireMessage() {
        return 
        	"To assure compliance with future DAR- and AIEC-Rules\n"+
        	"this version will stop working on " + expireDateToString();
    }

    public static LocalDate setExpireDate(LocalDate issueDate) {
        Month changeMonth = Month.JUNE;
        LocalDate exDate = LocalDate.of(issueDate.getYear(),changeMonth,changeMonth.maxLength());
        long diff = exDate.until(issueDate, ChronoUnit.DAYS);

        if (diff < 0 && Math.abs(diff) > maxDaysToExpireDate) {
            return exDate;
        } else if (diff < 0 && Math.abs(diff) < maxDaysToExpireDate) {
            return exDate.plusMonths(6).withDayOfMonth(changeMonth.plus(6).maxLength());
        } else if (diff > 0 && Math.abs(diff) < exDate.getDayOfYear() - maxDaysToExpireDate) {
            return exDate.plusMonths(6).withDayOfMonth(changeMonth.plus(6).maxLength());
        } else {
            return exDate.plusMonths(12).withDayOfMonth(changeMonth.maxLength());
        }
    }
    
    //---------------------------------------------------------
    
    public static String betaReminder() {
        return
        "!  This software is still under development and may be unreliable.\n"+
        "!  => Distrust any calculations it performs!\n"+
        "!  => Make backups of your files regularly!\n"+
        "!  => The .chu file format may change without notice before version 1.0!\n";
    }

    public static void init() {
        try {
            InputStream in = Meta.class.getClassLoader().getResourceAsStream("version.properties");
            p.load(in);
        } catch (IOException ioEx) {
            ioEx.printStackTrace();
        }
    }

    public static Semver getVersion() {
        return new Semver(p.getProperty("version"));
    }

    public static LocalDate getIssueDate() {
        return LocalDate.parse(p.getProperty("issueDate"));
    }

}

