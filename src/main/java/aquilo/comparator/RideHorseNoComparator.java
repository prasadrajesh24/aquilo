/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.comparator;

import aquilo.entity.ResultToPoints;
import aquilo.entity.Ride;

/**

*/

public class RideHorseNoComparator implements NamedComparator {

    @Override
    public int compare(ResultToPoints rtp1, ResultToPoints rtp2) {
        Ride r1 = (Ride) rtp1;
        Ride r2 = (Ride) rtp2;
        int hn1 = Integer.parseInt(r1.getHorse().getHorseNo());
        int hn2 = Integer.parseInt(r2.getHorse().getHorseNo());
        if(hn1!=hn2) return (hn1 < hn2) ? -1 : 1;
        return (r1.startNo < r2.startNo)? -1 : 1;
    }

    public String getOrderName() {
        return "HorseNo";
    }
}

