/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.comparator;

import aquilo.entity.ResultToPoints;

/**

*/

public class RideDifferenceComparator implements NamedComparator {

    @Override
    public int compare(ResultToPoints r1, ResultToPoints r2) {
        return r1.getResult().compareDiff(r2.getResult());
    }

    public String getOrderName() {
        return "Difference";
    }
}



