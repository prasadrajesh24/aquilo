/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.entity;

/**
    Allow algorithm in PointsAndRanking.resultToPoints() common access to
    Team and Ride.
*/

public interface ResultToPoints {

    // questions delegated to Result attribute of Ride or Team
    String getResultInfo();
    boolean resultPreventsRanking();
    
    // output of resultToPoints() handed on RankedObject (Rider or Team)
    void setPoints(double pointsMin, double points, int disciplineIndex);
    void setNoRanking(int disciplineIndex);

    Result getResult();

}

