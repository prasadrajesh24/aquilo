/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.entity;

import aquilo.util.*;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**

    Common base class for Rider and Team, which both appear in more than one ranking.

*/

public abstract class RankedObject {

    private static final Logger logger = LoggerFactory.getLogger(RankedObject.class);

    // see Discipline for index conventions (COMB=0, DR=1, SJ=2)
    public transient double[] pointsMin;
    public transient double[] points;
    public transient double penaltyPoints;
    public transient int[]     rank;
    public transient boolean[] rankDefinite;
    
    final static double INF_BAD_POINTS = 999.0;

    public RankedObject() {
        pointsMin    = new double[Discipline.MAX_DISCIPLINE];
        points       = new double[Discipline.MAX_DISCIPLINE];
        rank         = new int[Discipline.MAX_DISCIPLINE];
        rankDefinite = new boolean[Discipline.MAX_DISCIPLINE];
    }
    
    //----------------------------------------------------------------------
    // see rules "Ranking of Non-qualified Riders", .."retires before he starts riding"..
    
    public void setNoRanking(int disciplineIndex) {
        pointsMin[disciplineIndex] = INF_BAD_POINTS;
        points[disciplineIndex]    = INF_BAD_POINTS;
        pointsMin[Discipline.COMB] = INF_BAD_POINTS;
        points[Discipline.COMB]    = INF_BAD_POINTS;
    }
    
    public boolean hasRanking(int disciplineIndex) {
        // if points are added: for ranking-points >= INF_BAD_POINTS in a basic discipline
        // then individual combined and all team ranking-points will automatically also
        // be >= INF_BAD_POINTS   !!! dangerous assumption if formula used !!!
        return points[disciplineIndex] < INF_BAD_POINTS;
    }
    
    //----------------------------------------------------------------------
    
    private boolean pointsDefinite(int disciplineIndex) {
        return Math.abs(points[disciplineIndex] - pointsMin[disciplineIndex]) < 0.0001;
    }
    
    public boolean rankDefinite(int disciplineIndex) {
        return rankDefinite[disciplineIndex];
    }
    
    public String pointsToString(int disciplineIndex) {
        if( pointsDefinite(disciplineIndex) ) {
            return ""+points[disciplineIndex];
        }
        else {
            return "["+pointsMin[disciplineIndex]+" "+points[disciplineIndex]+"]";
        }
    }
    
    public String rankToString(int disciplineIndex) {
        if( rankDefinite(disciplineIndex) ) {
            return hasRanking(disciplineIndex) ? ""+rank[disciplineIndex] : "-";
        }
        else {
            return "";
            //return "["+(hasRanking(disciplineIndex) ? ""+rank[disciplineIndex] : "-")+"]";
       }
    }
    
    String pointsAndRankToString() {
        String s = "";
        for(int i=Discipline.DR; i<=Discipline.LAST; i++) {
            s += Fmt.fmt(points[i],7) + Fmt.fmt(rank[i],4);
        }
        s += Fmt.fmt(points[Discipline.COMB],7) + Fmt.fmt(rank[Discipline.COMB],4);
        return s;
    }

}

