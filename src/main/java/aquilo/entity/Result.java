/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.entity;

import java.io.*;
import java.text.NumberFormat;
import java.util.Locale;
import aquilo.util.*;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;


public abstract class Result {

    private static final Logger logger = LoggerFactory.getLogger(Result.class);

    private int state;
    // values for state. Order not accidental: NO_RESULT always sorted last
    private final static String[] STATES;
    private final static int HAS_VALUE ;
    private final static int ELIMINATED;
    private final static int RETIRED   ;
    private final static int NO_RANKING;
    private final static int NO_RESULT ;

    // style, faults and time are all doubles. Interpretation in subclass.
    protected double[] result;
    protected double[] diff;

    final static Result INFINITELY_BAD = new ResultStyle();

    // provide some standard formatting for known result values
    final static NumberFormat fmtStyle,fmtFaults,fmtTime;
    final static String valueSep = "  ";

    //------------------------------------------------
    // initialization

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    static {
        STATES = new String[10];
        HAS_VALUE  = 0;   STATES[HAS_VALUE]   = "(value)";
        ELIMINATED = 1;   STATES[ELIMINATED]  = "elim";
        RETIRED    = 2;   STATES[RETIRED]     = "reti";
        NO_RANKING = 3;   STATES[NO_RANKING]  = "noRank";
        NO_RESULT  = 9;   STATES[NO_RESULT]   = "";

        INFINITELY_BAD.state = Integer.MAX_VALUE;

        Locale locale = Locale.US;
        fmtStyle = NumberFormat.getInstance(locale);
        fmtStyle.setMinimumIntegerDigits(1);
        fmtStyle.setMinimumFractionDigits(2);
        fmtFaults = NumberFormat.getInstance(locale);
        fmtFaults.setMinimumIntegerDigits(1);
        fmtFaults.setMinimumFractionDigits(2);
        fmtTime = NumberFormat.getInstance(locale);
        fmtTime.setMinimumIntegerDigits(2);
        fmtTime.setMinimumFractionDigits(2);
    }

    public Result() {
    	state = NO_RESULT;
        result = new double[noOfValues()];
        diff   = new double[noOfValues()];
    }

    //--------------------------------------------------
    // different interpretations of values in result[]:
    // Some of the following methods must or may be overridden in a subclass.
    // Users of this class (Ride,Round,RoundWindow,PointsAndRanking,..)
    // should use this interface and not care about its subclasses.

    public abstract int noOfValues();                   // 1 or 2

    public abstract String getDescription(int index);   // "Style","Faults", ..

    public abstract NumberFormat getFormat(int index);

    public abstract int compareDiff(Result r);

    protected abstract String formatResult();           // called by our toString()
    protected abstract String formatDiff();

    public boolean set(int index,String valueString) {
        int newState;
        double newValue;
        boolean changed = false;
        // allow empty string, 'e'liminate, 'r'etired or floating point number,
        valueString = valueString.trim();
        if( valueString.length()==0 )          { newState = NO_RESULT;  }
        else if( valueString.startsWith("e") ) { newState = ELIMINATED; }
        else if( valueString.startsWith("r") ) { newState = RETIRED;    }
        else if( valueString.startsWith("n") ) { newState = NO_RANKING; }
        else {
            valueString = valueString.replace(',','.');   // allow US and Europeean decimal separator
            try {
                newValue = Double.parseDouble(valueString);
            }
            catch( NumberFormatException e ) { return false; }
            newState = HAS_VALUE;
            changed = result[index] != newValue;
            result[index] = newValue;
        }
        changed = state != newState || changed;
        state = newState;
        return changed;
    }

    public double getResult(int index) {
        return result[index];
    }

    public double getDiff(int index) {
        return diff[index];
    }

    public int compare(Result r) {
        if( state != r.state )
            return (state > r.state) ? -1 : 1;
        return 0;
    }

    public void setDiffTo(Result r) {
        for(int i=0; i<noOfValues(); i++) {
            diff[i] = round(r.result[i] - result[i]);
        }
    }

    private static final double minUnit = 0.001;

    public static double round(double val) {
        double factor = 1000.0d;
        return Math.round(val*factor)/factor;
    }

    // !!!!! dangerous : SJ diffs can be negative for eliminated riders
    public boolean diffIsZeroOrNegative() {
        if( ! hasValue() ) return false;
        for(int i=0; i<noOfValues(); i++) {
            if( diff[i] >= minUnit ) return false;
        }
        return true;
    }
    
    //--------------------------------------------------

    public final boolean isAvailable() {
        return state != NO_RESULT;
    }

    public final boolean hasValue() {
        return state == HAS_VALUE;
    }

    public final boolean preventRanking() {
        return state == NO_RANKING;
    }

    public final String toString(int index) {
        if( state != HAS_VALUE ) return STATES[state];
        return getFormat(index).format(getResult(index));
    }

    public final String diffToString(int index) {
        if( state != HAS_VALUE ) return STATES[state];
        return getFormat(index).format(getDiff(index));
    }

    public final String toString() {
        if( state != HAS_VALUE ) return STATES[state];
        return formatResult();
    }

    public final String diffToString() {
        if( state != HAS_VALUE ) return STATES[state];
        return formatDiff();
        //return ""+diffToSingleValue();
    }

    //--------------------------------------------------
    // read / write this object as XML

	public void writeAsXml(XmlWriter xout) throws IOException {
	    String valAttrs = "";
        for(int i=0; i<noOfValues(); i++) {
            valAttrs += " "+getDescription(i)+"='"+result[i]+"'";
            //valAttrs += " "+getDescription(i)+"Diff='"+diff[i]+"'";
        }
	    xout.writeEmptyElement("Result","state='"+state+"'"+valAttrs);
	}

	public void readFromXml(ObjectElement oe) throws Exception {
        state = Integer.parseInt(oe.getAttribute("state"));

        for(int i=0; i<noOfValues(); i++) {
            result[i] = oe.getDoubleAttribute(getDescription(i));
            //diff[i]   = oe.getDoubleAttribute(getDescription(i)+"Diff");
        }
	}

}
