/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.entity;

import java.text.NumberFormat;


public class ResultFaultsStyle extends Result {

    public final static int        FAULTS=0, STYLE=1;
    static final String[] name = { "Faults", "Style" };
    public int noOfValues() {  return 2;  }

    public String getDescription(int index) {
        return name[index];
    }

    public int compare(Result r) {
        if( !hasValue() || !r.hasValue() )
            return super.compare(r);
        if( result[FAULTS] != r.result[FAULTS] )
            return (result[FAULTS] > r.result[FAULTS]) ? -1 : 1;
        if( result[STYLE] != r.result[STYLE] )
            return (result[STYLE] < r.result[STYLE]) ? -1 : 1;
        return 0;
    }

    public int compareDiff(Result r) {
        if( !hasValue() || !r.hasValue() )
            return super.compare(r) * -1;
        if( diff[FAULTS] != r.diff[FAULTS] )
            return (diff[FAULTS] > r.diff[FAULTS]) ? -1 : 1;
        if( diff[STYLE] != r.diff[STYLE] )
            return (diff[STYLE] < r.diff[STYLE]) ? -1 : 1;
        return 0;
    }

    public NumberFormat getFormat(int index) {
        if(index==0) return fmtFaults;
        if(index==1) return fmtStyle;
        return null;
    }

    protected String formatResult() {
        return fmtFaults.format(result[FAULTS]) +valueSep+ fmtStyle.format(result[STYLE]);
    }

    protected String formatDiff() {
        return fmtFaults.format(diff[FAULTS]) +valueSep+ fmtStyle.format(diff[STYLE]);
    }
}
