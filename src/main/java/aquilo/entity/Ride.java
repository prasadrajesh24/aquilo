/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.entity;

import java.io.*;
import aquilo.util.*;

/**
 * Belongs to a Round and brings together a startNo, Horse, Rider and Result.
 * 
 * A List of Rides constitutes the essential part of a Round. No special
 * functionality implemented here.
 */
public class Ride implements ResultToPoints {

	Round round;
	public int startNo;
	public int riderOnHorse;
	private Horse horse;
	private Rider rider;
	private Result result;

	// ------------------------------------------------
	// initialization

	public Ride(Round rnd) {
		this.round = rnd;
		this.rider = Rider.notDefined;
		if (rnd.resultType.equals(Round.STYLE))
			result = new ResultStyle();
		if (rnd.resultType.equals(Round.FAULTSSTYLE))
			result = new ResultFaultsStyle();
		if (rnd.resultType.equals(Round.FAULTSTIME))
			result = new ResultFaultsTime();
	}

	public Ride(Round rnd, int startNo, int riderOnHorse, Horse horse) {
		this(rnd);
		this.startNo = startNo;
		this.riderOnHorse = riderOnHorse;
		this.setHorse(horse);
	}

	// --------------------------------------------------
	// simple access methods

	public Round getRound() {
		return round;
	}

	public Rider getRider() {
		return rider;
	}

	public Result getResult() {
		return result;
	}

	public void setRider(Rider newRider) {
		rider.removeRide(round.getDiscipline().getIndex(), this);
		rider = newRider;
		rider.addRide(round.getDiscipline().getIndex(), this);
	}

	// --------------------------------------------------
	// implement ResultToPoints interface

	public String getResultInfo() {
		return result.diffToString();
	}

	public boolean resultPreventsRanking() {
		return result.preventRanking();
	}

	public void setPoints(double pointsMin, double points, int disciplineIndex) {
		rider.setPoints(pointsMin, points, disciplineIndex);
	}

	public void setNoRanking(int disciplineIndex) {
		rider.setNoRanking(disciplineIndex);
	}

	// --------------------------------------------------
	// ascii output and formatting special values

	public String toString() {
		String s = "";
		s += Fmt.fmt(startNo, 3) + "  ";
		s += Fmt.toOrdinal(riderOnHorse) + " on " + getHorse().toString();
		s += Fmt.fix(rider.toString(), 35);
		s += Fmt.extL(result.toString(), 8);
		s += Fmt.extL(result.diffToString(), 8);
		return s;
	}

	// --------------------------------------------------
	// read / write this object as XML

	public void writeAsXml(XmlWriter xout) throws IOException {
		String attrStr = "";
		attrStr += "startNo='" + Fmt.fmt(startNo, 2) + "' ";
		attrStr += "roh='" + riderOnHorse + "' ";
		attrStr += "horse='" + Fmt.fmt(getHorse().getHorseNo(), 2) + "' ";
		attrStr += "rider='" + Fmt.fmt(rider.bootNo, 2) + "'";
		xout.startGroup(getClass(), attrStr);
		result.writeAsXml(xout);
		xout.endGroup(getClass());
	}

	public void readFromXml(ObjectElement oe) throws Exception {
		startNo = oe.getIntAttribute("startNo");
		riderOnHorse = oe.getIntAttribute("roh");
		String hNo = oe.getAttribute("horse");
		int rNo = oe.getIntAttribute("rider");

		result.readFromXml(new ObjectElement(oe.getChildElement("Result")));

		setHorse(round.findHorseByNo(hNo.trim()));
		if (getHorse() == null)
			throw new Exception("Invalid <horse> \"" + hNo + "\" in rides of " + round.name);
		if (rNo == Rider.notDefined.bootNo) {
			setRider(Rider.notDefined);
		} else {
			Rider rider = Competition.findRiderByBootNo(rNo);
			if (rider == null)
				throw new Exception("Invalid <rider> \"" + rNo + "\" in rides of " + round.name);
			setRider(rider);
		}
	}

	public Horse getHorse() {
		return horse;
	}

	public void setHorse(Horse horse) {
		this.horse = horse;
	}

}
