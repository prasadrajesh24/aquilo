/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.entity;

import java.text.NumberFormat;

public class ResultStyle extends Result {

	public final static int STYLE = 0;
	static final String[] name = { "Style" };

	public int noOfValues() {
		return 1;
	}

	public String getDescription(int index) {
		return name[index];
	}

	public int compare(Result r) {
		if (!hasValue() || !r.hasValue())
			return super.compare(r);
		if (result[STYLE] != r.result[STYLE])
			return (result[STYLE] < r.result[STYLE]) ? -1 : 1;
		return 0;
	}

	public int compareDiff(Result r) {
		if (!hasValue() || !r.hasValue())
			return super.compare(r) * -1;
		if (diff[STYLE] != r.diff[STYLE])
			return (diff[STYLE] < r.diff[STYLE]) ? -1 : 1;
		return 0;
	}

	public NumberFormat getFormat(int index) {
		return fmtStyle;
	}

	protected String formatResult() {
		return fmtStyle.format(result[STYLE]);
	}

	protected String formatDiff() {
		return fmtStyle.format(diff[STYLE]);
	}

}
