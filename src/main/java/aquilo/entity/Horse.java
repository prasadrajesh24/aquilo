/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.entity;

import java.io.IOException;
import aquilo.util.*;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;


public final class Horse {

	private static final Logger logger = LoggerFactory.getLogger(Horse.class);

    private String horseNo;
    public String name;

    //--------------------------------------------------
    // initialization

    Horse() { }

    Horse( String horseNo, String name ) {
	    this.setHorseNo(horseNo);
	    this.name = name;
    }

    //--------------------------------------------------
    // read / write this object as XML

	void writeAsXml(XmlWriter xout) throws IOException {
	    xout.setMultiLine(false);
	    xout.startGroup(getClass(),"horseNo='"+Fmt.fmt(getHorseNo(),1)+"'");
	    xout.writeElement("name",name);
	    xout.endGroup(getClass());
	    xout.setMultiLine(true);
	}

	void readFromXml(ObjectElement oe) throws Exception {
	    setHorseNo(oe.getAttribute("horseNo").trim());
	    name    = oe.getField("name");
	}

    //--------------------------------------------------
    // ascii output

    public String toString() {
    	return Fmt.fmt(getHorseNo(),2)+' '+Fmt.fmt(name,15);
    }

	public String getHorseNo() {
		return horseNo;
	}

	private void setHorseNo(String horseNo) {
		this.horseNo = horseNo;
	}
}