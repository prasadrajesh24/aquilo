/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.entity;

import java.util.*;
import java.io.*;
import org.w3c.dom.NodeList;
import aquilo.util.*;

/**
    Team Data
*/

public final class Team extends RankedObject implements ResultToPoints {

    public String name;
    public String abbreviation;
    public Result dressageResult;
    public double teamDrPoints,teamDrPointsMin;
           ArrayList<Rider>   riders;

    public static Team notDefined;

    //-----------------------------------------------------
    // initialization

    static {
        notDefined = new Team("","");
    }

    public Team() {
	    super();
    	dressageResult = new ResultStyle();
    	riders = new ArrayList<>(3);
    }
    
    public Team(String name, String abbreviation) {
        this();
    	this.name = name;
    	this.abbreviation = abbreviation;
    }
    
    //--------------------------------------------------
    // access methods
    
    public void addRider(Rider r) {
        r.team = this;
        this.riders.add(r);
    }
    public Rider  getRider(int i)           {  return riders.get(i);  }
    public Result getResult()               {  return dressageResult;  }
    public void setResult(Result result)    { this.dressageResult = result; }
    public ArrayList<Rider>   getRiders()   {  return riders;  }
    public Iterator<Rider> riderIterator()  {  return riders.iterator();  }
    
    //--------------------------------------------------
    // implement ResultToPoints
    
    public String getResultInfo() {
        return ""+dressageResult;
    }
    
    public boolean resultPreventsRanking() {
        return dressageResult.preventRanking();
    }
    
    public void setPoints(double teamDressagePointsMin, double teamDressagePoints, int notUsed) {
        teamDrPointsMin = teamDressagePointsMin;
        teamDrPoints = teamDressagePoints;
    }

    public void setPenaltyPoints(double penaltyPoints_) {
        penaltyPoints = penaltyPoints_;
    }
    
    public void setNoRanking(int notUsed) {
        super.setNoRanking(Discipline.DR);
        teamDrPointsMin = teamDrPoints = INF_BAD_POINTS;
    }
    
    //--------------------------------------------------
    
    public static boolean dressageResultsComplete() {
        for (Team team : Competition.teams) {
            if (!team.getResult().isAvailable()) return false;
        }
        return true;
    }
    
    public double riderPointsSum(int disciplineIndex) {
        double Pts = 0.0;
        for(Iterator<Rider> i=riderIterator(); i.hasNext(); ) {
            Pts += (i.next()).points[disciplineIndex];
	    }
    	return Pts;
	}

    public double riderPointsMinSum(int disciplineIndex) {
        double PtsMin = 0.0;
        for(Iterator<Rider> i=riderIterator(); i.hasNext(); ) {
            PtsMin += (i.next()).pointsMin[disciplineIndex];
	    }
    	return PtsMin;
	}

    //--------------------------------------------------
    // read / write this object as XML
    
	public void writeAsXml(XmlWriter xout) throws IOException {
	    xout.startGroup(getClass());
	    xout.writeElement("name",name);
        xout.writeElement("abbreviation",abbreviation);
        
        dressageResult.writeAsXml(xout);
        //xout.writeElement("dressageResult",dressageResult);
        
        if(riders!=null) {
    	    xout.startGroup("riders");
            for(Iterator<Rider> i=riderIterator(); i.hasNext(); ) {
                Rider r = i.next();
                r.writeAsXml(xout);
            }
    	    xout.endGroup("riders");
	    }
	    xout.endGroup(getClass());
	}
	
	public void readFromXml(ObjectElement oe) throws Exception {
	    name            = oe.getField("name");
	    abbreviation    = oe.getField("abbreviation");

	    //dressageResult.set(0, oe.getField("dressageResult"));
        dressageResult.readFromXml(new ObjectElement(oe.getChildElement("Result")));

	    NodeList nl = oe.getElement().getElementsByTagName("Rider");
	    for(int i=0; i<nl.getLength(); i++ ) {
	        Rider r = new Rider();
	        r.readFromXml(new ObjectElement(nl.item(i)));
	        addRider(r);
	        Competition.riders.add(r);
	    }
	}
	
    //--------------------------------------------------
    // ascii output
    
    public String toString() {
    	return	Fmt.fmt(name,17) + Fmt.fmt(abbreviation,5) +
    		    Fmt.fmt(dressageResult.getResult(ResultStyle.STYLE),6) + Fmt.fmt(teamDrPoints,7) + Fmt.fmt(dressageResult.getDiff(ResultStyle.STYLE),6);
    }

    public String ridersToString(boolean showRides) {
        String str = "";
        for(Iterator<Rider> i=riderIterator(); i.hasNext(); ) {
            Rider r = i.next();
	        str += "  " + r + r.pointsAndRankToString()+'\n';
	        if(showRides) str += r.ridesToString();
	    }
	    return str;
    }
    
    public String ridersToStringList() {
        String str = "";
        for(Iterator<Rider> i=riderIterator(); i.hasNext(); ) {
            Rider rider = i.next();
	        str += rider.fullName();
	        if(i.hasNext()) str+=", ";
	    }
	    return str;
    }

}
