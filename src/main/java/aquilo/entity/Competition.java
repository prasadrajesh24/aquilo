/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.entity;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.w3c.dom.*;
import aquilo.util.*;
import aquilo.ranking.*;

/**
    Parent/root of all other data classes. 
*/

public final class Competition {

    private static final Logger logger = LoggerFactory.getLogger(Competition.class);

    // actual competition data
    public static List<Team> teams;
    public static List<Rider> riders;
    public static Discipline[] disciplines;
    
    // competition properties
    public static String name = "";
    public static int ridersPerTeam = 3;
    public static boolean isDHM = false;
    public static boolean QualiDHM2019 = false;

    private final static String SRNC = "SRNC";
    private final static String CHIU = "CHIU";
    private final static String WUEC = "WUEC";
    private final static String CHU_GER = "CHU_GER";
    private static String type = CHU_GER;
    
    //------------------------------------------------
    // access methods

    public static int noOfTeams()        { return teams.size(); }
    public static int noOfRiders()       { return riders.size(); }
    public static Rider getRider(int i)  { return riders.get(i); }
    
    public static String  getTypeId()    { return type; }
    public static boolean isSRNC()       { return type.equals(SRNC); }
    public static void    setTypeSRNC()  { type = SRNC;  Lang.set(Lang.EN); }
    public static boolean isCHIU()       { return type.equals(CHIU); }
    public static void    setTypeCHIU()  { type = CHIU;  Lang.set(Lang.EN); }
    public static boolean isWUEC()       { return type.equals(WUEC); }
    public static void    setTypeWUEC()  { type = WUEC;  Lang.set(Lang.EN); }
    public static boolean isCHU_GER()    { return type.equals(CHU_GER); }
    public static void    setTypeCHU_GER() { type = CHU_GER;  Lang.set(Lang.DE); }
    
    public static boolean hasMultipleTeamRankings() { return type.equals(SRNC) || type.equals(CHIU) || type.equals(WUEC); }
    public static boolean isInternational() { return type.equals(SRNC) || type.equals(CHIU) || type.equals(WUEC); }
    
    //------------------------------------------------
    // initialization

    public static void createEmptyCompetition(int noOfTeams, int  maxRidersForTwoPerHorse) {
        createEmptyTeamsAndRiders(noOfTeams);
        Discipline.createStandardDisciplines();
        disciplines[Discipline.DR].setRoundsInitParams(noOfTeams, maxRidersForTwoPerHorse);
        disciplines[Discipline.SJ].setRoundsInitParams(noOfTeams, maxRidersForTwoPerHorse);
    }

    private static void createEmptyTeamsAndRiders(int noOfTeams) {
        logger.info("Creating "+noOfTeams+" empty teams with "+ridersPerTeam+" riders each");
        teams  = new ArrayList<>(noOfTeams);
        riders = new ArrayList<>(noOfTeams*ridersPerTeam);

        int bootNum = 1;
        for(int t=0; t<noOfTeams; t++) {
            bootNum = addEmptyTeamWithEmptyRiders(bootNum);
        }
    }

    private static int addEmptyTeamWithEmptyRiders(int bootNum) {
        Team t = new Team("","");
        teams.add(t);
        for(int i=0; i<ridersPerTeam; i++) {
            Rider r = new Rider(bootNum++,"","");
            t.addRider(r);
            riders.add(r);
        }
        return bootNum;
    }
    
    public static boolean isCompleted()
    {
        return disciplines[Discipline.DR].isCompleted() 
            && disciplines[Discipline.SJ].isCompleted();
    } 

    public static String completedRounds()
    {
        return
            Competition.disciplines[Discipline.DR].completedRounds(",")+ " "
          + Competition.disciplines[Discipline.SJ].completedRounds(",");
    }
        
    public static boolean hasRoundsIgnoredForRanking()
    {
        return disciplines[Discipline.DR].roundsIgnoredForRanking() 
            || disciplines[Discipline.SJ].roundsIgnoredForRanking();
    } 
    
    public static String roundsConsideredForRanking()
    {
        return
            "Dressur bis "+disciplines[Discipline.DR].getIgnoreRoundsAbove()+". Runde, "+
            "Springen bis "+disciplines[Discipline.SJ].getIgnoreRoundsAbove()+". Runde";
    }

    //--------------------------------------------------
    // read / write this object as XML

	public static void writeAsXml(File file) throws IOException {

    	FileOutputStream ostream = new FileOutputStream(file);
	    XmlWriter xout = new XmlWriter(new PrintStream(ostream,true, StandardCharsets.UTF_8));
	    logger.info("Writing competition to "+file.getAbsolutePath());

	    xout.newline();
	    xout.writeComment(Meta.promotion());
	    xout.writeComment("WARNING!  Do not edit this file unless you really know what you are doing!");
	    xout.newline();
	    
	    String versionAttrs = "fileVersion='"+Meta.fileVersion+"' programVersion='"+Meta.programVersion+"'";
	    xout.startGroup("Competition","type='"+getTypeId()+"' "+versionAttrs);
	    xout.writeElement("name",Competition.name);

        xout.writeElement("isDHM",isDHM);
        xout.writeElement("qualiDHM2019",QualiDHM2019);

        if(disciplines!=null) {
            xout.startGroup("disciplines");
            for(int i=Discipline.COMB; i<=Discipline.LAST; i++ ) {
                disciplines[i].writeAsXml(xout);
            }
    	    xout.endGroup("disciplines");
    	}
	    xout.writeComment("===========================================");
        if(teams!=null) {
    	    xout.startGroup("teams");
            for (Team t : teams) {
                t.writeAsXml(xout);
            }
    	    xout.endGroup("teams");
    	}
    	// [riders saved within teams.writeAsXml]
	    xout.endGroup("Competition");
        ostream.close();
	}

	public static void readFromXml(File file) throws Exception {

	    XmlDomReader xin = new XmlDomReader();
	    logger.info("Reading competition from "+file.getAbsolutePath());
	    xin.parse(file);

	    ObjectElement competitionEl = new ObjectElement(xin.getDocumentRoot());
	    Competition.name = competitionEl.getField("name");
	    Competition.setTypeFromString( competitionEl.getAttribute("type") );

        try {
            Competition.isDHM = competitionEl.getBoolField("isDHM");
            Competition.QualiDHM2019 = competitionEl.getBoolField("qualiDHM2019");
        } catch (Exception ex) {
            logger.info("Old Data format... Assuming default values...");
        }

        logger.info("Reading teams and riders ..");
        Competition.teams = new ArrayList<>(18);
        Competition.riders = new ArrayList<>(3*18);
	    NodeList nl = competitionEl.getElement().getElementsByTagName("Team");
	    for(int i=0; i<nl.getLength(); i++ ) {
	        Team t = new Team();
	        t.readFromXml(new ObjectElement(nl.item(i)));
	        Competition.teams.add(t);
	    }
        logger.info("Reading disciplines, rounds and rides ..");
	    Competition.disciplines = new Discipline[Discipline.MAX_DISCIPLINE];
	    nl = competitionEl.getElement().getElementsByTagName("Discipline");
	    for(int i=0; i<nl.getLength(); i++ ) {
	        Discipline d = new Discipline(0,null);
	        d.readFromXml(new ObjectElement(nl.item(i)));
	        Competition.disciplines[d.getIndex()] = d;
	    }
	}

    // used by Ride.readFromXml to assign a rider
    public static Rider findRiderByBootNo(int bootNo) {
        for( int i=0; i<noOfRiders(); i++ ) {
            if(getRider(i).bootNo==bootNo) return getRider(i);
        }
        return null;
    }

    private static void setTypeFromString(String typeId) throws Exception {
        switch (typeId) {
            case SRNC: setTypeSRNC();break;
            case CHIU: setTypeCHIU();break;
            case WUEC: setTypeWUEC();break;
            case CHU_GER: setTypeCHU_GER();break;
            default:
                throw new Exception("Invalid competition type \"" + typeId + '"');
        }
    }
    
    //--------------------------------------------------
    // ascii output

    public static void printTeamsWithRiders(boolean showRides) {
        if(teams==null) return;
        PointsAndRanking.assignAll();
        PrintWriter out = new PrintWriter(System.out);
        for (Team t : teams) {
            out.println(t);
            out.print(t.ridersToString(showRides));
            out.println("                       Team:" + t.pointsAndRankToString());
            out.println();
        }
        out.flush();
	}

    public static void printAllRounds(boolean printRides) {
        if(disciplines==null) return;
        PointsAndRanking.assignAll();
        for(int i=Discipline.COMB; i<=Discipline.LAST; i++ ) {
            if(disciplines[i]==null) continue;
            logger.info("{}",disciplines[i]);
            disciplines[i].printRounds(printRides);
        }
	}

}

