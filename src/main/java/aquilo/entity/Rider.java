/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.entity;

import java.io.*;
import java.util.*;
import aquilo.util.*;
import aquilo.ranking.*;

/**
    Rider Data.
*/

public final class Rider extends RankedObject {

    public int          bootNo;
    public String       firstName;
    private String      lastName;
    public Team                team;
    SortedSet<Ride>[]         rides;   // one per Discipline, sorted by round, rider unique! in round

    public static Rider notDefined;

    //--------------------------------------------------
    // initialization

    static {
        notDefined = new Rider(999,"","");
        notDefined.team = Team.notDefined;
    }

    Rider() {
	    super();
	    rides = new SortedSet[Discipline.MAX_DISCIPLINE];
	    rides[Discipline.DR] = new TreeSet<>(new RideRoundComparator());
	    rides[Discipline.SJ] = new TreeSet<>(new RideRoundComparator());
    }

    Rider(int bootNo, String firstName, String lastName) {
	    this();
	    this.bootNo = bootNo;
	    this.firstName = firstName;
	    this.setLastName(lastName);
    }

    // sorts round IDs: DR1, DR2, ..
    final static class RideRoundComparator implements Comparator<Ride> {
        public int compare(Ride r1, Ride r2) {
            return r1.round.getId().compareTo(r2.round.getId());
        }
    }

    //--------------------------------------------------
    // access methods

    public Team getTeam() {
        return team;
    }

    public void addRide(int disciplineIndex, Ride r)  {
        rides[disciplineIndex].add(r);
    }
    
    public void removeRide(int disciplineIndex, Ride r)  {
        rides[disciplineIndex].remove(r);
    }

    public Iterator<Ride> ridesIterator(int disciplineIndex) {
        return rides[disciplineIndex].iterator();
    }

    public Ride rideInHighestRound(int disciplineIndex) {
        return rides[disciplineIndex].last();
    }

    public void setPoints(double pointsMin_, double points_, int disciplineIndex) {
        pointsMin[disciplineIndex] = pointsMin_;
        points[disciplineIndex] = points_;
        // adjust *points* for dependent rankings (respective *ranks* are NOT set)
        PointsAndRanking.assignIndividualCombinedPoints(this);
        if(disciplineIndex==Discipline.DR) PointsAndRanking.assignTeamDressagePoints(team);
        if(disciplineIndex==Discipline.SJ) PointsAndRanking.assignTeamShowjumpingPoints(team);
        //PointsAndRanking.assignTeamCombinedPoints(team);
    }

    //--------------------------------------------------
    // read / write this object as XML

    public void writeAsXml(XmlWriter xout) throws IOException {
        xout.setMultiLine(false);
        xout.startGroup(getClass(),"bootNo='"+bootNo+"'");
        xout.writeElement("firstname", firstName);
        xout.writeElement("lastname", getLastName());
        xout.endGroup(getClass());
        xout.setMultiLine(true);
    }

    public void readFromXml(ObjectElement oe) throws Exception {
        bootNo      = oe.getIntAttribute("bootNo");
        firstName = oe.getField("firstname");
        setLastName(oe.getField("lastname"));
        // team   assigned from Team.readFromXml
        // rides  saved under Round and then point here
    }

    //--------------------------------------------------
    // ascii output

    public String toString() {
        return Fmt.fmt(bootNo,3)+' '+Fmt.fmt(fullName(),22);
    }

    public String fullName() {
        return firstName +' '+ getLastName();
    }

    public String ridesToString() {
        String str = "";
        for(int di=Discipline.FIRST; di<=Discipline.LAST; di++ ) {
            for(Iterator<Ride> it=ridesIterator(di); it.hasNext(); ) {
                Ride ride = it.next();
                str += "      "+ride.round.getId()+"  "+ride+"\n";
            }
        }
        return str;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}

