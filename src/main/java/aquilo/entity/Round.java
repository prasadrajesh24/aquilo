/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.entity;

import java.io.*;
import java.util.*;

import aquilo.comparator.NamedComparator;
import aquilo.comparator.RideDifferenceComparator;
import aquilo.comparator.RideStartNoComparator;
import aquilo.gui.MessageWindow;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.w3c.dom.*;
import aquilo.util.*;

/**
 * Maintains a list of rides and horses.
 * 
 * A round always belongs to a discipline and can be identified by its id: DR1,
 * SJ3,... There are two stages in the lifetime of a round (which should
 * actually have gone into different classes): 1. In the init state the public
 * fields noOfRides, noOfHorses, horsesPerGroup, firstHorseId and resultType
 * hold information about what a round should look like. 2. Using
 * createEmptyStartingOrder() the lists of horses and rides are filled using the
 * above parameters.
 * 
 */

public class Round {

	private static final Logger logger = LoggerFactory.getLogger(Round.class);

	private final Discipline discipline; // parent discipline
	private String id; // DR1,DR2,.. SJ1,SJ2,..
	public String name;

	// init parameters for createEmptyStartingOrder
	public int noOfRides; // should be rides.size() after init
	public int noOfHorses;
	public int[] horsesPerGroup;
	public String firstHorseId;
	public String resultType;
	public final static String STYLE = "Style", FAULTSSTYLE = "FaultsStyle", FAULTSTIME = "FaultsTime";
	public int needsLL;

	// actual data
	private List<Horse> horses;
	private List<Ride> rides;
	private String ridesOrder;

	// --------------------------------------------------

	public String getId() {
		return id;
	}

	public Discipline getDiscipline() {
		return discipline;
	}

	public int getLevel() {
		return id.charAt(id.length() - 1) - 48;
	}

	public boolean isFirstRound() {
		return getLevel() == 1;
	}

	public boolean isLastRound() {
		return this.discipline.getNoOfRounds() == getLevel();
	}

	// accessing rides
	public Ride getRide(int i) {
		return rides.get(i);
	}

	public ArrayList<Ride> getRides() {
		return new ArrayList<>(rides);
	}

	public int noOfRides() {
		return rides.size();
	}

	private void addRide(Ride r) {
		r.round = this;
		rides.add(r);
	}

	private Iterator<Ride> ridesIterator() {
		return rides.iterator();
	}

	public void clearRides() {
		rides.clear();
	}

	public String getRidesOrder() {
		return ridesOrder;
	}

	// accessing horses
	public Horse getHorse(int i) {
		return horses.get(i);
	}

	public List<Horse> getHorses() {
		return horses;
	}

	public Iterator<Horse> horseIterator() {
		return horses.iterator();
	}

	public void clearHorses() {
		horses.clear();
	}

	// -----------------------------------------------------
	// initialization

	public Round(Discipline discipline) {
		this.discipline = discipline;
		this.rides = new ArrayList<>();
		this.horses = new ArrayList<>();
	}

	public Round(Discipline discipline, String id, String name, int noOfRides, int noOfHorses, int[] horsesPerGroup,
				 String resultType, String firstHorseId, int LuckyLoser) {
		this(discipline);
		this.id = id;
		this.name = name;
		this.noOfRides = noOfRides;
		this.noOfHorses = noOfHorses;
		if (horsesPerGroup == null) {
			this.horsesPerGroup = new int[1];
			this.horsesPerGroup[0] = noOfHorses;
		} else {
			this.horsesPerGroup = horsesPerGroup;
		}
		this.resultType = resultType;
		this.firstHorseId = firstHorseId;
		this.needsLL = LuckyLoser;
	}

	/**
	 * Create rides with Rider.notDefined and auto-assigned new, empty horses.
	 * Example: noOfRides=27 noOfHorses=9 firstHorseId="1" horsesPerGroup={5,4} =>
	 * horseNo: 12345 12345 12345 6789 6789 6789
	 */
	public void createEmptyStartingOrder() throws Exception {

		checkRidersDivisibleByHorses(noOfRides, noOfHorses);
		int ridersPerHorse = noOfRides / noOfHorses;
		checkSum(horsesPerGroup, noOfHorses);
		logger.info("Creating rides for: " + this);

		if (horses.isEmpty())
			createEmptyHorses(firstHorseId, noOfHorses);

		int startNo = 1, h0 = 0;
		for (int g = 0; g < horsesPerGroup.length; g++) {
			for (int riderOnHorse = 1; riderOnHorse <= ridersPerHorse; riderOnHorse++) {
				for (int h = h0; h < h0 + horsesPerGroup[g]; h++) {
					if (startNo > noOfRides)
						return;
					Horse horse = horses.get(h);
					Ride r = new Ride(this, startNo, riderOnHorse, horse);
					rides.add(r);
					startNo++;
					// logger.info(r);
				}
			}
			h0 += horsesPerGroup[g];
		}
		ridesOrder = new RideStartNoComparator().getOrderName();
	}

	public static void checkRidersDivisibleByHorses(int noOfRides, int noOfHorses) throws Exception {
		if (noOfRides % noOfHorses != 0)
			throw new Exception("Number of riders " + noOfRides + " not divisible by number of horses " + noOfHorses);
	}

	public static void checkSum(int[] intVals, int sum) throws Exception {
		if (intVals == null)
			return;
		int s = 0;
		for (int i = 0; i < intVals.length; i++)
			s += intVals[i];
		if (s != sum)
			throw new Exception("Total number of horses " + sum + " is not equal to the sum of horses in groups: " + s);
	}

	private void createEmptyHorses(String firstHorseId, int noOfHorses) {
		int firstHorseNo = Integer.parseInt(firstHorseId);
		for (int i = 0; i < noOfHorses; i++) {
			String horseNo = Integer.toString(firstHorseNo + (i % noOfHorses));
			horses.add(new Horse(horseNo, ""));
		}
	}

	// --------------------------------------------------
	// calculating differences for round

	private Result bestResultOnHorse(Horse h) {
		Result bestResult = Result.INFINITELY_BAD;
		for (int i = 0; i < rides.size(); i++) {
			Ride ride = getRide(i);
			if (ride.getHorse() != h)
				continue; // we're interested in horse h only
			if (ride.getResult().compare(bestResult) > 0)
				bestResult = ride.getResult();
		}
		return bestResult;
	}

	public List<Ride> assignDiffsForHorse(Horse h) {
		Result bestResult = bestResultOnHorse(h);
		List<Ride> bestAndEqualRides = new ArrayList<>(2);
		for (int i = 0; i < rides.size(); i++) {
			Ride ride = getRide(i);
			if (ride.getHorse() != h)
				continue; // we're interested in horse h only
			Result result = ride.getResult();
			if (result.isAvailable()) {
				if (result == bestResult && getLevel() == discipline.getIgnoreRoundsAbove()) {
					// special: calc negative diff to second best
					Result secondBestResult = secondBestResultOnHorse(h, bestResult);
					bestResult.setDiffTo(secondBestResult);
					if (result.compare(bestResult) == 0)
						bestAndEqualRides.add(ride);
				} else {
					// normal: calc diffs of knocked out riders
					result.setDiffTo(bestResult);
					if (result.compare(bestResult) == 0)
						bestAndEqualRides.add(ride);
				}
			}
		}
		return bestAndEqualRides;
	}

	public void assignDiffsForAllHorses() {
		for (int h = 0; h < horses.size(); h++) {
			assignDiffsForHorse(getHorse(h));
		}
	}

	// special: 'secondBest' needed to calculate negative diff of best rider on
	// horse
	private Result secondBestResultOnHorse(Horse h, Result bestResult) {
		Result secondBestResult = Result.INFINITELY_BAD;
		for (int i = 0; i < rides.size(); i++) {
			Ride ride = getRide(i);
			if (ride.getHorse() != h)
				continue; // we're interested in horse h only
			if (ride.getResult().compare(secondBestResult) > 0 && ride.getResult().compare(bestResult) < 0) {
				secondBestResult = ride.getResult();
			}
		}
		return secondBestResult;
	}

	// --------------------------------------------------
	// information about the progress of this round

	/** Find a rider and return the ride he is assigned to or null if not found. */
	public Ride findRider(Rider rider) {
		for (Iterator<Ride> it = ridesIterator(); it.hasNext();) {
			Ride ride = it.next();
			if (ride.getRider() == rider)
				return ride;
		}
		return null;
	}

	/** A round is complete if all rides have riders and results. */
	public boolean isCompleted() {
		if (rides.isEmpty())
			return false;
		for (Iterator<Ride> it = ridesIterator(); it.hasNext();) {
			Ride ride = it.next();
			if (ride.getRider() == Rider.notDefined || !ride.getResult().isAvailable()) {
				return false;
			}
		}
		return true;
	}

	public List<Rider> getQualifiedRiders() throws Exception {

		List<Rider> qualifiedRiders = new ArrayList<>(noOfHorses + needsLL);

		if (isCompleted()) {
			// how many rides available in next round?
			int nextRoundSize = discipline.getNextRoundSize(this);

			// sort current round by diff
			List<Ride> ridesList = new ArrayList<>(rides);
			ridesList.sort(new RideDifferenceComparator());
			// add the first nextRoundSize to qualifiedRiders
			for (int i = 0; i < nextRoundSize; i++) {
				Ride ride = ridesList.get(i);
				if (i + 1 == nextRoundSize) {
					// we are about to insert the last/worst qualified rider
					List<Ride> LuckyLosers = new ArrayList<>();
					Ride nextRide = ridesList.get(i + 1);
					int j = 1;
					while (ride.getResult().compareDiff(nextRide.getResult()) == 0){
						LuckyLosers.add(nextRide);
						j++;
						nextRide = ridesList.get(i+j);
					}
					boolean equalCandidates = !LuckyLosers.isEmpty();
					// skip if there is more than one candidate (equal minimal diffs)
					if (equalCandidates){
						LuckyLosers.add(ridesList.get(i));
						MessageWindow.showTooManyLuckyLosers(LuckyLosers);
						throw new Exception("too many Lucky Losers");
					}
				}
				qualifiedRiders.add(ride.getRider());
			}

		} else {
			// assumptions about qualified riders while round is running not supported
		}
		return qualifiedRiders;
	}

	public int countAvailableResultsForHorse(Horse h) {
		int cnt = 0;
		for (Iterator<Ride> i = ridesIterator(); i.hasNext();) {
			Ride r = i.next();
			if (r.getHorse() == h && r.getResult().isAvailable())
				cnt++;
		}
		return cnt;
	}

	/*
	 * public boolean allResultsAvailableForHorse(Horse h) { int ridersPerHorse =
	 * noOfRides/noOfHorses; return ridersPerHorse >=
	 * countAvailableResultsForHorse(h); }
	 */

	public boolean isIgnoredForRanking() {
		return getLevel() > discipline.getIgnoreRoundsAbove();
	}

	// --------------------------------------------------

	public void sortRides(NamedComparator comparator) {
		rides.sort(comparator);
		ridesOrder = comparator.getOrderName();
	}

	// --------------------------------------------------

	/**
	 * Special case: 1st round noOfTeams not divisible by 3 (eg WUEC 17):
	 * Discipline.setRoundsInitParams() and createEmptyStartingOrder() will create a
	 * round with the next multiple of 3 many rides. Here we delete the extra rides.
	 * This lets the user choose (in the GUI RoundWindow) where he only wants to
	 * have 2 riders on a horse.
	 */
	public void deleteRides(int[] selectedRides) {
		int delCount = 0;
		for (int i = 0; i < selectedRides.length; i++) {
			int delIndex = selectedRides[i] - delCount;
			rides.remove(delIndex);
			// decrement startNo for all following rides
			for (int j = delIndex; j < rides.size(); j++) {
				getRide(j).startNo--;
			}
			delCount++;
			noOfRides--;
		}
	}

	// --------------------------------------------------
	// read / write this object as XML

	void writeAsXml(XmlWriter xout) throws IOException {
		xout.startGroup(getClass(), "id='" + getId() + "'");
		xout.writeElement("name", name);
		String initParams = "noOfRides='" + noOfRides + "' " + "noOfHorses='" + noOfHorses + "' " + "horsesPerGroup='"
				+ Fmt.intArrayToString(horsesPerGroup) + "' " + "resultType='" + resultType + "' " + "firstHorseId='"
				+ firstHorseId + "' " + "hasLuckyLoser='" + needsLL + "'";
		xout.writeEmptyElement("initParams", initParams);
		if (horses != null) {
			xout.startGroup("horses");
			for (int i = 0; i < horses.size(); i++) {
				getHorse(i).writeAsXml(xout);
			}
			xout.endGroup("horses");
		}
		if (rides != null) {
			xout.startGroup("rides", "order='" + ridesOrder + "'");
			for (Iterator<Ride> i = ridesIterator(); i.hasNext();) {
				Ride r = i.next();
				xout.setMultiLine(false);
				r.writeAsXml(xout);
				xout.setMultiLine(true);
			}
			xout.endGroup("rides");
		}
		xout.endGroup(getClass());
	}

	void readFromXml(ObjectElement oe) throws Exception {
		id = oe.getAttribute("id");
		logger.info(id);
		name = oe.getField("name");
		ObjectElement initParams = new ObjectElement(oe.getChildElement("initParams"));
		noOfRides = initParams.getIntAttribute("noOfRides");
		noOfHorses = initParams.getIntAttribute("noOfHorses");
		horsesPerGroup = Fmt.parseIntList(initParams.getAttribute("horsesPerGroup"));
		firstHorseId = initParams.getAttribute("firstHorseId");
		resultType = initParams.getAttribute("resultType");
		try {
            needsLL = initParams.getIntAttribute("hasLuckyLoser");
        } catch (Exception hasLLex) {
			System.err.println(hasLLex + "Found needsLL Exception"); // TODO: 06.04.23 rename and fix 
//		    new MessageWindow().showNoLuckyLoserDetection(); // shows currently for every Round when an old file is loaded :/
        }
		NodeList nl = oe.getElement().getElementsByTagName("Horse");
		for (int i = 0; i < nl.getLength(); i++) {
			Horse h = new Horse();
			h.readFromXml(new ObjectElement(nl.item(i)));
			horses.add(h);
		}
		Element rides = oe.getChildElement("rides");
		ridesOrder = rides.getAttribute("order");
		nl = oe.getElement().getElementsByTagName("Ride");
		for (int i = 0; i < nl.getLength(); i++) {
			Ride r = new Ride(this);
			r.readFromXml(new ObjectElement(nl.item(i)));
			addRide(r);
		}
		if (noOfRides() > 0) {
			assignDiffsForAllHorses();
		}
	}

	/** used by Ride.readFromXml to assign a horse */
	Horse findHorseByNo(String horseNo) {
		for (int i = 0; i < horses.size(); i++) {
			if (getHorse(i).getHorseNo().equals(horseNo))
				return getHorse(i);
		}
		return null;
	}

	// --------------------------------------------------

	/** ASCII debug output */
	public String toString() {
		return Fmt.fmt(id, 4) + Fmt.fmt(name, 20) + "InitParams: " + Fmt.fmt(noOfRides, 2) + "riders,"
				+ Fmt.fmt(noOfHorses, 2) + "horses(" + Fmt.intArrayToString(horsesPerGroup) + ")," + resultType
				+ ",firstHorse#" + firstHorseId + ",LuckyLoser:" + needsLL;
	}

	/** ASCII debug output */
	void printRides() {
		if (rides == null)
			return;
		for (int i = 0; i < rides.size(); i++)
			logger.info("{}",getRide(i));
	}

}
