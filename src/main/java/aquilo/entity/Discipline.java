/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.entity;

import java.io.*;
import java.util.*;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.w3c.dom.NodeList;
import aquilo.util.*;


public class Discipline implements Serializable {

    private static final Logger logger = LoggerFactory.getLogger(Discipline.class);

    public  String name;
    private List<Round>   rounds;

    private int    index;       // a discipline must know its own index (see below)

    /** Use a preliminary round to calculate points and ranking.
        Diffs of winners to second best are used to order the qualified. 
        A high value means that no rounds are ignored.
    */
    private int ignoreRoundsAbove = 99;

    // Disciplines and rankedObject points/ranks are accessed sequentially as well as
    // randomly by name or corresponding indices.
    // => implementing data structure: array with index conventions for
    // Competition.disciplines[] and rankedObject.points[]/.rank[]
    public static final int COMB = 0;               // dummy, for rankedObjects only
    public static final int DR   = 1, FIRST = 1;    // dressage
    public static final int SJ   = 2;               // showjumping
    public static final int CC   = 3;               // cross-country (not supported)
    public static       int           LAST  = 2;    // may be set by app later

    // in the XML file and other cases we use a string id instead of the index:
    public final static String[] ID = { "COMB","DR","SJ" };

    public static final int MAX_DISCIPLINE = ID.length;   // to allocate discipline dep. arrays
    
    //--------------------------------------------------
    // access methods

    public int      getIndex()          { return index; }
    public String   getId()             { return ID[index]; }

    private void    addRound(Round r)   { rounds.add(r);  }
    public Round    getRound(int i)     { return rounds.get(i);  }
    public int      getNoOfRounds()     { return rounds.size();  }
    private Iterator<Round> roundIterator()     { return rounds.iterator();  }
    
    //--------------------------------------------------
    // initialization

    private Discipline() {  // no public constructor to force index and ID settings below
    	rounds = new ArrayList<>(5);
    }

    public Discipline(int index, String name) {
        this();
        this.index=index;
    	this.name=name;
    }

    // service routine called from Competition
    static void createStandardDisciplines() {
    	Competition.disciplines = new Discipline[MAX_DISCIPLINE];
    	Competition.disciplines[COMB] = new Discipline(COMB,Lang.t("Combined")); // dummy
    	Competition.disciplines[DR]   = new Discipline(DR,Lang.t("Dressage"));
    	Competition.disciplines[SJ]   = new Discipline(SJ,Lang.t("Showjumping"));
    }

    // For a given number of teams, generate default configuration parameters for all rounds.
    // eg DR1: 36riders,12horses(6,6),firstHorse#1
    //    DR2: 12riders, 4horses(4),firstHorse#13
    //    ...
    void setRoundsInitParams(int noOfTeams, int maxRidersForTwoPerHorse) {

        int nt = noOfTeams;                             // n_umber of t_eams
        if(nt>4)  nt = nextMultipleOf(3,noOfTeams);     // eg nt=16 -> nt=18. Delete extra rides later
        int nr = Competition.ridersPerTeam * nt;        // n_umber of r_ide(r)s in round
        logger.info("Creating default round params for "+name+", "+nt+" teams, "+nr+" riders");
        int rndCnt = 1;
        int firstHorseNo = 1;
        int LuckyLoser = 0;
        while( nr >= 2 ) {
            int nh;                                     // n_umber of h_orses
            int nnr;                                    // n_umber n_ext (round) r_iders
            // 3 or 2 riders per horse?
            if( nr <= maxRidersForTwoPerHorse) {
                nr = nextMultipleOf(2,nr);
                nh = nr/2;
                nnr = nh;
                nnr = nextMultipleOf(2,nnr);
                if (nnr!=nh && nh>1){
                    LuckyLoser = nnr-nh;
                }
            } else {
                nr = nextMultipleOf(3,nr);
                nh = nr/3;
                nnr = nh;
                if (nnr <= maxRidersForTwoPerHorse){
                    nnr = nextMultipleOf(2,nnr);
                }else {
                    nnr = nextMultipleOf(3, nnr);
                }
                if (nnr!=nh){
                    LuckyLoser = nnr-nh;
                }
            }
            if (Competition.isWUEC()){
                LuckyLoser = 0;
            }
            // prepare round properties
            String roundId        = getId()+rndCnt;
            String roundName      = rndCnt+". "+Lang.t("Round")+" "+name;
            int[]  horsesPerGroup = roundHorseGroupDistribution(roundId,nh);
            String resultType     = Round.STYLE;
            if (Competition.isCHU_GER() && getId().equals("SJ")) {
                if (nh==1) resultType = Round.FAULTSSTYLE;
            } else if(getId().equals("SJ")) {
                if      (nh==1) resultType = Round.FAULTSTIME;
                else if (nh<=3) resultType = Round.FAULTSSTYLE;
            }
            // create Round with its ini params and add to this Discipline. No rides yet!
            Round rnd = new Round(
                this, roundId, roundName,
                nr, nh, horsesPerGroup, resultType, Integer.toString(firstHorseNo), LuckyLoser);
            logger.info("{}",rnd);
            addRound(rnd);
            // what's left form this round for the next?
            nr = nh;
            firstHorseNo += nh;
            rndCnt++;
            LuckyLoser = 0;
        }
    }

    private static int nextMultipleOf(int factor, int number) {
        return ((number+factor-1)/factor)*factor;
    }

    // default first round horse groups
    private int[] roundHorseGroupDistribution(String roundId, int noOfHorses) {

        int[] horsesPerGroup;
        // first, eliminate all cases which our default tables don't cover
        // return "all horses in one group"
        if( !roundId.endsWith("1") || noOfHorses%3 != 0 || noOfHorses<6 || noOfHorses>30 ) {
            horsesPerGroup = new int[1];
            horsesPerGroup[0] = noOfHorses;
            return horsesPerGroup;
        }
        // pick group configurations form table(s)
        // (currently there are only equivalent defaults for DR1 and SJ1)
        int index = noOfHorses/3 - 2;
        return HORSE_GROUP_DISTRIBUTION[index];
    }

    private final static int[][] HORSE_GROUP_DISTRIBUTION = {
        /*  6 */ { 6 },
        /*  9 */ { 9 },
        /* 12 */ { 6,6 },
        /* 15 */ { 9,6 },
        /* 18 */ { 9,9 },
        /* 21 */ { 9,6,6 },
        /* 24 */ { 9,9,6 },
        /* 27 */ { 9,9,9 },
        /* 30 */ { 9,9,6,6 }
    };

    public boolean isCompleted() {
        for(Iterator<Round> i=roundIterator(); i.hasNext(); ) {
            Round rnd = i.next();
    	    if( ! rnd.isCompleted() ) return false;
    	}
    	return true;
    }
    
    public String completedRounds(String stringListSeparator) {
    	StringBuilder completedRounds = new StringBuilder();
        for(Iterator<Round> i=roundIterator(); i.hasNext(); ) {
            Round rnd = i.next();
    	    if( ! rnd.isCompleted() ) break;
    	    if( ! rnd.isFirstRound() ) completedRounds.append(stringListSeparator);
    	    completedRounds.append(rnd.getId());
    	}
    	return completedRounds.toString();
    }
    
    public Round getNextRound(Round currentRound) {
        for(Iterator<Round> i=roundIterator(); i.hasNext(); ) {
    	    if( i.next() == currentRound )
    	        return (i.hasNext()) ? i.next() : null;
    	}
    	return null;
    }
    
    public int getNextRoundSize(Round currentRound) {
        Round rnd = getNextRound(currentRound);
        return (rnd==null) ? 0 : rnd.noOfRides;
    }
    
    public int getIgnoreRoundsAbove() {
        return ignoreRoundsAbove;
    }

    public void setIgnoreRoundsAbove(int newVal){
        ignoreRoundsAbove = newVal;
    }
    
    public boolean roundsIgnoredForRanking() {
        return ignoreRoundsAbove<rounds.size();
    }
    
    //--------------------------------------------------
    // read / write this object as XML

	void writeAsXml(XmlWriter xout) throws IOException {
	    xout.writeComment("===========================================");
	    String disciplAttrs = "id='"+getId()+"'";
	    if(index!=COMB) disciplAttrs += " ignoreRoundsAbove='"+ignoreRoundsAbove+"'";
	    xout.startGroup(getClass(), disciplAttrs);
        xout.writeElement("name",name);
        if(rounds!=null) {
    	    xout.startGroup("rounds");
            for(Iterator<Round> i=roundIterator(); i.hasNext(); ) {
                Round r = i.next();
                r.writeAsXml(xout);
            }
    	    xout.endGroup("rounds");
	    }
	    xout.endGroup(getClass());
	}

	void readFromXml(ObjectElement oe) throws Exception {
	    index = findIndexForId( oe.getAttribute("id") );
	    readIgnoreRoundsAbove(oe);
	    name  = oe.getField("name");
	    logger.info(Fmt.fmt(name,15));
	    NodeList nl = oe.getElement().getElementsByTagName("Round");
	    for(int i=0; i<nl.getLength(); i++ ) {
	        Round r = new Round(this);
	        r.readFromXml(new ObjectElement(nl.item(i)));
	        addRound(r);
	    }
	}

	// for readability, in the XML file we have no index, only the ID string
	// eventually a DTD should check this for us
	private static int findIndexForId(String disciplineId) throws Exception {
	    for(int i=0; i<ID.length; i++) if(disciplineId.equals(ID[i])) return i;
	    String msg = "Bad id for Discipline: ["+disciplineId+"]";
	    System.err.println(msg);
	    throw new Exception(msg);
	}
	
	public static Discipline getForId(String disciplineId) throws Exception {
	    return Competition.disciplines[ findIndexForId(disciplineId) ];
    }
    
    private void readIgnoreRoundsAbove(ObjectElement oe) {
	    // ignoreRoundsAbove is optional
        String val = oe.getAttribute("ignoreRoundsAbove").trim();
        try {
            ignoreRoundsAbove = Integer.parseInt(val);
        } catch (NumberFormatException ignored) {
        }
        // if not available or garbage include all available rounds (=high round level)
	    if(ignoreRoundsAbove<1) ignoreRoundsAbove = 99;
    }    

    //--------------------------------------------------
    // ascii output

    public String toString() {
    	return "Discipline ["+index+"] "+getId()+' '+name+
    	        " has "+ rounds.size() +" rounds";
    }
    void printRounds(boolean printRides) {
        if(rounds==null) return;
    	for(Iterator<Round> it=roundIterator(); it.hasNext(); ) {
    	    Round rnd = it.next();
    	    logger.info("{}", rnd );
    	    if(printRides) rnd.printRides();
    	}
    }

    //=========================================================================

    public static void main(String[] args) throws Exception {

        int[] nt = { 6, 9,12,15,18,21,24,27,30,33,36 };


        for( int i=0; i<nt.length; i++ ) {
    	    Discipline dressage = new Discipline(DR,"Dressage");
            dressage.setRoundsInitParams(nt[i],4);
            //dressage.roundHorseGroupDistribution("DR1",nt[i]);
        }

    }
}
