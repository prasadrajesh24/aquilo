/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.htmlout;

import java.util.*;
import java.io.*;

import aquilo.comparator.RideHorseNoComparator;
import aquilo.comparator.RideStartNoComparator;
import aquilo.comparator.RideTeamComparator;
import aquilo.entity.*;
import aquilo.util.Lang;

/**
	Round as HTML. The order depends on the current order of the round.rides List.
	I.e. the effect of Round.sortByXXX() is show in HTML
*/

public final class RoundListing extends HtmlOutput {

    public static File writeHtmlFile(Round rnd, int[] selectedRides) throws IOException {

		File outFile = putInOutputDir("Round" + rnd.getId() + "by" + rnd.getRidesOrder() + ".html");
		PrintStream out = openHtmlOutputFile(outFile);

		if (selectedRides == null || selectedRides.length == 0) {
			// no selection specified => show all rides
			selectedRides = new int[rnd.noOfRides()];
			for (int i = 0; i < rnd.noOfRides(); i++) selectedRides[i] = i;
		}

		boolean teamsSeparate =
				rnd.getId().equals("DR1") && rnd.getRidesOrder().equals(new RideStartNoComparator().getOrderName())
						|| rnd.getRidesOrder().equals(new RideTeamComparator().getOrderName());
		boolean horsesSeparate = rnd.getRidesOrder().equals(new RideHorseNoComparator().getOrderName());

		out.append(htmlDocumentHead(rnd.name));
		String LuckyLoserStr;
		if (rnd.needsLL != 0 && !rnd.isCompleted()) {
			LuckyLoserStr = "<h3>" + Lang.t("ATTENTION: This Round needs ") + rnd.needsLL + " Lucky Loser!!!" + "</h3>";
		} else {
			LuckyLoserStr = "";
		}
		out.append(documentHeadArea(rnd.name, "Sorted by " + rnd.getRidesOrder(), LuckyLoserStr, "$Date"));

		out.println(tableTagSimpleBorder("RoundListing", "100%"));
		out.println("<style>" + nl + styleRules + "</style>");
		out.append(roundTableHeader(rnd, true));
		Team lastTeam = null;
		Horse lastHorse = null;
		int colSpan = /*(Competition.isInternational()) ? 10 :*/ 9;  // additional horse Order column?
		try {
			Ride createQualifiedList = rnd.getRide(selectedRides[0]);
			List<Rider> qRiders = createQualifiedList.getRound().getQualifiedRiders();
			for (int i = 0; i < selectedRides.length; i++) {
				Ride ride = rnd.getRide(selectedRides[i]);
				// separate teams or horses
				Team currentTeam = ride.getRider().getTeam();
				if (teamsSeparate && currentTeam != lastTeam) {
					if (i > 0) out.append(separatorDummyRow(colSpan, 8, "silver"));
					lastTeam = currentTeam;
				}
				Horse currentHorse = ride.getHorse();
				if (horsesSeparate && currentHorse != lastHorse) {
					if (i > 0) out.append(separatorDummyRow(colSpan, 8, "silver"));
					lastHorse = currentHorse;
				}
				// show ride
				out.append(rideToHtmlTableRow(ride, true, "1em", qRiders));
				}
			}
		catch (Exception ex){
			return null;
		}
		out.append(TABLEe);
		out.print(documentFootArea());
		out.close();
		return outFile;

    }

    public static String roundTableHeader(Round rnd, boolean showResultColumns) {
    	String headerLine = TR_GRAY+
			"<th width='4%' "+STYLE_FAT_BORDER_BOTTOM+">"+Lang.t("Start")+"<br>"+Lang.t("Total")+"</th>"+
			"<th width='4%' "+STYLE_FAT_BORDER_BOTTOM+">"+Lang.t("Start")+"<br>"+Lang.t("Horse")+"</th>"+
			"<th width='4%' "+STYLE_FAT_BORDER_BOTTOMLEFT+">"+Lang.t("Horse")+"<br>"+Lang.t("No")+"</th>"+
			"<th width='20%' "+STYLE_FAT_BORDER_BOTTOM+">"+Lang.t("Horse")+"</th>";
        //if(Competition.isInternational() && rnd.getId().equals("DR1")) headerLine +=
    	//	"<th width='4%' "+STYLE_FAT_BORDER_BOTTOM+">"+"Horse<br>Order"+"</th>";
        headerLine += 
			"<th width='4%' "+STYLE_FAT_BORDER_BOTTOMLEFT+">"+Lang.t("Boot")+"<br>"+Lang.t("No")+"</th>"+
			"<th width='30%' "+STYLE_FAT_BORDER_BOTTOM+">"+Lang.t("Rider")+"</th>";
        if(showResultColumns) headerLine +=
    		"<th width='20%' "+STYLE_FAT_BORDER_BOTTOM+">"+Lang.t("Team")+"</th>"+
    		"<th width='7%' "+STYLE_FAT_BORDER_BOTTOMLEFT+">"+Lang.t("Result")+"</th>"+
    		"<th width='7%' "+STYLE_FAT_BORDER_BOTTOM+">"+Lang.t("Diff")+"</th>";
    	headerLine += TRe+nl;
	    return headerLine;
    }

    public final static String styleRules = 
        "  td.startNo   { text-align: center; }"+nl+
        "  td.roh       { text-align: center; }"+nl+
        "  td.horseNo   { text-align: center; }"+nl+
        "  td.horseName { text-align: left; }"+nl+
        "  td.bootNo    { text-align: center; }"+nl+
        "  td.riderName { text-align: left; }"+nl+
        "  td.teamName  { text-align: left; }"+nl+
        "  td.result    { text-align: center; }"+nl+
        "  td.diff      { text-align: center; }"+nl;
    
    public static String rideToHtmlTableRow(Ride r,boolean showResultColumns, String rowHeight, List qualifiedRiders) {
        Rider rider = r.getRider();
    	String s = nl;

    	if(qualifiedRiders.contains(rider))
        	s += "<tr class='qualified'>";
        else
        	s += "<tr>";
        	
    	s += "<td class='startNo' height='"+rowHeight+"'>"    + r.startNo + TDe; 
    	s += "<td class='roh'>"                               + r.riderOnHorse + TDe;
	    s += "<td class='horseNo' "+STYLE_FAT_BORDER_LEFT+">" + r.getHorse().getHorseNo() + TDe;
	    s += "<td class='horseName'>"                         + r.getHorse().name + TDe;
	    //if(Competition.isInternational() && r.getRound().getId().equals("DR1")) 
	    //    s += "<td class='horseOrder'>"+NBSP+TDe;
	    s += "<td class='bootNo' "+STYLE_FAT_BORDER_LEFT+">"  + ((rider.bootNo==999)?NBSP:""+rider.bootNo) + TDe;
	    s += "<td class='riderName'>"                         + rider.firstName + NBSP + rider.getLastName() + TDe;
        if(showResultColumns) {
            Team t = rider.getTeam();
    	    s += "<td class='teamName'>" + ((t==Team.notDefined) ? NBSP : t.name) + TDe;
        	s += "<td class='result' "   + STYLE_FAT_BORDER_LEFT+">" + ( (r.getResult().isAvailable()) ? r.getResult().toString() : NBSP ) + TDe;
        	s += "<td class='diff'>"     + ( (r.getResult().isAvailable()) ? r.getResult().diffToString() : NBSP ) + TDe;
        	//if(showPoints) s += TDC + rider.points[r.getRound().disciplineIndex];
        }
        s += TRe;
    	return s;
    }

}