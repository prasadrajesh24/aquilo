/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.htmlout;

import aquilo.entity.Competition;
import aquilo.entity.Rider;
import aquilo.entity.Team;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import static java.nio.charset.StandardCharsets.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class LiabilityDisclosure extends HtmlOutput {

    public static File writeFile(String filename) throws IOException {

        if(filename==null) filename = "Haftungsausschluss_"+ Competition.name+".tex";
        File outFile = putInOutputDir(filename);
        PrintStream out = openHtmlOutputFile(outFile);

        List<Team> outOrder = new ArrayList<>(Competition.teams);

        String header = (
                "%Standard Header \n" +
                "\\documentclass[10pt,a4paper,toc=listof,toc=bibliography]{scrartcl}\n" +
                "\\usepackage[utf8]{inputenc}\n" +
                "\\usepackage[ngerman]{babel}\n" +
                "\\usepackage[T1]{fontenc}\n" +
                "\\usepackage{amsmath}\n" +
                "\\usepackage{amsfonts}\n" +
                "\\usepackage{amssymb}\n" +
                "\\usepackage[pdftex]{graphicx}\n" +
                "\\usepackage{epstopdf}\n" +
                "\\usepackage{geometry}\n" +
                "\\usepackage{multirow}\n" +
                "\\usepackage{pdfpages}\n" +
                "\\usepackage{textcomp}\t\t\t\t\t\t\t%degree celsius etc\n" +
                "\\usepackage{gensymb}\n" +
                "\\usepackage[hidelinks]{hyperref} \t\t\t\t%hidelinks removes red boxes from pdf\n" +
                "\\usepackage[babel, german=quotes]{csquotes}\n" +
                "\\usepackage[backend=biber, style=numeric, citestyle=authoryear]{biblatex}\n" +
                "\\usepackage{float}\n" +
                "\n" +
                "\\usepackage{color}\n" +
                "\\usepackage{listings}\t\t\t\t\t\t\t%einbinden von Sourcecode\n" +
                "\n" +
                "\n" +
                "\\KOMAoption{DIV}{9}\n" +
                "\n" +
                "\\sloppy\n" +
                "\n" +
                "\\setlength{\\parindent}{0pt}  \t\t%keine Einschuebe bei neuem Absatz\n" +
                "\n" +
                "%\\addtokomafont{disposition}{\\rmfamily} % serifes in headings\n" +
                "\n" +
                "\\pagestyle{empty}\n" +
                "\n" +
                "\\begin{document}"
        );

        byte[] headText = header.getBytes();
        String outHeader = new String(headText,UTF_8);

        out.println(outHeader);

        for (Team t : outOrder) {
            for (Iterator<Rider> rit = t.riderIterator(); rit.hasNext(); ) {
                Rider r = rit.next();
                out.println(riderText(r));
            }
        }

        out.println("\\end{document}");

        out.close();
        return outFile;
    }
    
    private static String riderText (Rider r){

        String bodyText = (
                "\\begin{center}\n" +
                        "\\begin{huge}\n" +
                        "\\textbf{Haftungsausschluss}\n" +
                        "\\end{huge}\n" +
                        "\\end{center}\n" +
                        "\n" +
                        "\\vspace{10pt}\n" +
                        "\n" +
                        "Veranstaltung: "+ Competition.name +" \\\\\\\\\n" +
                        "\n" +
                        "\\begin{large}\n" +
                        "Name Teilnehmer: " + r.fullName() + " \\hfill" + "(" + r.getTeam().name + ")" + "\\\\\\\\\n" +
                        "\\end{large}\n" +
                        "\n" +
                        "Die Teilnahme an der Veranstaltung erfolgt gem\u00e4\u00df den Bestimmungen der Leistungs-Pr\u00fcfungs-Ordnung (LPO) und auf eigene Gefahr und Risiko. " +
                        "Der Veranstalter und der Ausrichter \u00fcbernehmen keine Haftung f\u00fcr gesundheitliche Risiken des Teilnehmers im Zusammenhang mit der Teilnahme an der Veranstaltung. " +
                        "W\u00e4hrend des Reitens besteht Helmpflicht. Den Weisungen des Veranstalters ist Folge zu leisten. Zuwiderhandlungen f\u00fchren zum Ausschluss der Veranstaltung.\\\\\n" +
                        "\n" +
                        "%Der Teilnehmer wird darauf hingewiesen, dass er einen entsprechenden Versicherungsschutz mit notwendiger Deckung selbst abschlie\u00dfen muss.\\\\\n" +
                        "\n" +
                        "Der Veranstalter und der Ausrichter haften nicht f\u00fcr nicht wenigstens grob fahrl\u00e4ssig verursachte Sach- und Verm\u00f6genssch\u00e4den; ausgenommen von dieser Haftungsbegrenzung sind Sch\u00e4den, " +
                        "die auf der schuldhaften Verletzung einer vertraglichen Hauptleistungspflicht (Kardinalpflicht) des Veranstalters und des Ausrichters beruhen sowie Personensch\u00e4den " +
                        "in Form von Sch\u00e4den an Leben, Leib oder Gesundheit einer Person. Die vorstehenden Haftungsbeschr\u00e4nkungen erstrecken sich auch auf die Haftung der Angestellten, Vertreter, " +
                        "Erf\u00fcllungsgehilfen und Dritter, derer sich der Veranstalter und der Ausrichter im Zusammenhang mit der Durchf\u00fchrung der Veranstaltung bedienen bzw. mit denen sie zu diesem " +
                        "Zweck vertraglich verbunden sind.\\\\\n" +
                        "\n" +
                        "Der Teilnehmer stellt au\u00dferdem den Veranstalter, den Ausrichter sowie seine Erf\u00fcllungs- oder Verrichtungsgehilfen von jeglicher Haftung gegen\u00fcber Dritten frei und verzichtet " +
                        "auf eigene Anspr\u00fcche gegen\u00fcber dem Veranstalter und dem Ausrichter, soweit dies nach den obigen Bestimmungen statthaft ist. Dies gilt auch gegen\u00fcber Dritten, soweit diese Dritte " +
                        "Sch\u00e4den in Folge der Teilnahme erleiden.\\\\\n" +
                        "\n" +
                        "Hiermit best\u00e4tigt der oben genannte Teilnehmer, den Haftungsausschluss anzuerkennen:\\\\\\\\\n" +
                        "\n" +
                        "\\begin{tabbing}\n" +
                        "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\\=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\\kill\n" +
                        "\n" +
                        "$\\overline{\\text{Ort, Datum \\qquad}}$ \\> $\\overline{\\text{Unterschrift Teilnehmer \\qquad}}$\n" +
                        "\\end{tabbing}\n" + "\\newpage"
        );

        return bodyText;
    }

}









