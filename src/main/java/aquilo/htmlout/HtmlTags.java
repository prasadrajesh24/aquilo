/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

/*-----------------------------------------------------------------------------
    Define frequently used HTML tags als constants
    
    Andreas Steidle, december 1998
-----------------------------------------------------------------------------*/

package aquilo.htmlout;

/**
    Defines frequently used HTML tags als constants.
*/
public interface HtmlTags {

    String nl = System.lineSeparator();
    
    String HTML = "<html>"+nl; 
    String HEAD = "<head>"+nl; 
    String TITLE = "<title>", TITLEe = "</title>"+nl; 
    String HEADe = "</head>"+nl+nl; 
    String STYLE = "<style>",  STYLEe = "</style>"; 
    String BODY = "<body>"+nl; 
    String BODYe = nl+"</body>"; 
    String HTMLe = nl+"</html>"; 
    
    String BR = "<br>"+nl; 
    String HR = nl+"<hr><!-------------------------------------->"+nl; 
    String P = nl+"<p>"+nl; 
    String CENTER = nl+"<div style=\"text-align: center;\">", CENTERe = nl+"</div>"; 
    String RIGHT = nl+"<div style=\"text-align: right;\">",  RIGHTe = nl+"</div>";
    String DIVe = nl+"</div>";
    
    String EM = "<em>", EMe = "</em>"; 
    String STRONG = "<strong>", STRONGe = "</strong>"; 
    String B = "<b>", Be = "</b>"; 
    String I = "<i>", Ie = "</i>"; 
    String SMALL = "<small>", SMALLe = "</small>"; 
    String BIG = "<big>", BIGe = "</big>"; 
    String TT = "<tt>", TTe = "</tt>"; 
    String PRE = "<pre>", PREe = "</pre>"; 
    
    String TABLE = "<table>", TABLEe = nl+"</table>"+nl; 
    String TR = nl+"<tr>"; 
    String TRC = nl+"<tr align=\"center\">"; 
    String TRR = nl+"<tr align=\"right\">"; 
    String TRTOP = nl+"<tr valign=\"top\">"; 
    String TH = "<th>"; 
    String THe= "</th>"; 
    String TD = "<td>"; 
    String TDL = "<td style='text-align: left'>"; 
    String TDC = "<td style='text-align: center'>"; 
    String TDR = "<td style='text-align: right'>"; 
    String TDe = "</td>"; 
    String TRe = "</tr>"; 

    String NBSP = "&nbsp;";
    String COMMENTLINE = nl+
    	"<!-- ================================================================== -->"+nl;
}
