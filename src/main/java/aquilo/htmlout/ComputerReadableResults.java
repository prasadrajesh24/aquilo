/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.htmlout;

import java.util.*;
import java.io.*;

import aquilo.entity.*;
import aquilo.ranking.*;
import aquilo.util.Meta;

/**
    Write all results in simple machine readable format to a txt file:
    The file has the following format:
    
STUDENT RIDING COMPETITION RESULTS EXCHANGE FILE, VERSION 1.0
<CompetitionName>
TEAM
<TeamAbbreviationA>
<TeamNameA>
<TeamCombinedRank>
<TeamDressageRank>
<TeamShowjumpingRank>
<FirstnameRider1> <LastnameRider1>
<CombinedRank>
<DressageRank>
<ShowjumpingRank>
<FirstnameRider2> <LastnameRider2>
<CombinedRank>
<DressageRank>
<ShowjumpingRank>
<FirstnameRider3> <LastnameRider3>
<CombinedRank>
<DressageRank>
<ShowjumpingRank>
TEAM
<TeamAbbreviationB>
<TeamNameB>
<TeamCombinedRank>
<TeamDressageRank>
<TeamShowjumpingRank>
<FirstnameRider4> <LastnameRider4>
<CombinedRank>
<DressageRank>
<ShowjumpingRank>
...
*/

public final class ComputerReadableResults extends HtmlOutput {

    public static File writeFile(String filename) throws IOException {

        PointsAndRanking.assignAll();
        if(filename==null) filename = "Results of "+ Competition.name+".txt";
        File outFile = putInOutputDir(filename);
        PrintStream out = openHtmlOutputFile(outFile);

        out.println("STUDENT RIDING COMPETITION RESULTS EXCHANGE FILE, VERSION 1.0");
        out.println(Meta.nameVersionIssueDate());
        out.println(Competition.name);
        
        List<Team> outOrder = new ArrayList<>(Competition.teams);
        outOrder.sort(new TeamCombinedComparator());
        for(int i=0; i<outOrder.size(); i++) {
            out.println("TEAM");
            Team t = (Team)outOrder.get(i);
            out.println(t.abbreviation);
            out.println(t.name);
            out.println(rankOr999(t, Discipline.COMB));
            out.println(rankOr999(t,Discipline.DR));
            out.println(rankOr999(t,Discipline.SJ));
            for(Iterator<Rider> rit=t.riderIterator(); rit.hasNext(); ) {
                Rider r = rit.next();
                out.println(r.firstName +" "+r.getLastName());
                out.println(rankOr999(r,Discipline.COMB));
                out.println(rankOr999(r,Discipline.DR));
                out.println(rankOr999(r,Discipline.SJ));
            }
        }
        out.close();
        return outFile;
	}
	
	private static int rankOr999(RankedObject ro, int di) {
	    return (ro.hasRanking(di)) ? ro.rank[di] : 999;
    }
    
}