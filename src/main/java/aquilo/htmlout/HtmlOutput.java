/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.htmlout;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.io.*;
import javax.swing.*;

import aquilo.entity.Competition;
import aquilo.entity.RankedObject;
import aquilo.util.*;
import aquilo.gui.MainWin;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * Contains stuff needed by all HTML generating subclasses.
 * <p>
 * HTML files are placed into a directory next to the current location of the
 * data file. A file containing a HTML title template for all documents will be
 * placed in this file if non exists.
 */

public abstract class HtmlOutput implements HtmlTags {

	private static final Logger logger = LoggerFactory.getLogger(HtmlOutput.class);

	private static final String stdHtmlOutPath = "htmlOutput"; // relative to current save path
	public static final String stdExportPath = "export";

	public static boolean showHtmlMsg = true;
	public static boolean isExport = false;

	// ------------------------------------------------------------------------

	public static File putInOutputDir(String filename) throws IOException {

		// place "htmlOutput" dir next to data file myCompetition.chu
		File dataFile = MainWin.fileHandling.getFile();
		if (dataFile == null)
			throw new IOException("Please save your data first.");
		File outDir;
		if (isExport){
			outDir = new File(dataFile.getParent(),stdExportPath);
		}else {
			outDir = new File(dataFile.getParent(),stdHtmlOutPath);
		}
		// make sure the output dir exists
		if (!outDir.isDirectory())
			outDir.mkdir();
		return new File(outDir, filename);
	}

	public static PrintStream openHtmlOutputFile(File file) throws IOException {

		// XHtmlViewer.showDivAndWait("HtmlOutput");
		showHtmlOutputMsg(file.getParentFile());
		PrintStream out;
		try {
			out = new PrintStream(new FileOutputStream(file),true, StandardCharsets.UTF_8);
		} catch (IOException ex) {
			logger.warn(ex.getMessage(), ex);
			throw ex;
		}
		logger.info("Writing HTML to " + file.getAbsolutePath());
		return out;
	}

	public static void showHtmlOutputMsg(File outDir) {
		if (!showHtmlMsg || outDir == null)
			return;
		String msg = "\nAll output is generated as HTML. HTML is the native document format of the WWW.\n"
				+ "You can view it with a webbrowser, edit it in a text processor (MS Word) or\n"
				+ "publish it directly on the Internet. To print the document please use the Print\n"
				+ "function of your browser.\n\n"
				+ "All generated HTML files are saved in a directory next to your data file and\n"
				+ "opened automatically in your standard webbrowser. Currently the directory is:\n\n"
				+ outDir.getAbsolutePath() + "\n\n";
		// "For advanced users: If you wish to customize the title or style of all
		// created\n"+
		// "HTML documents, edit the file DocTitleTemplate.html in the directory
		// above.";
		JCheckBox checkbox = new JCheckBox("Show this message again", true);
		checkbox.setBorder(new javax.swing.border.EmptyBorder(10, 0, 10, 0));
		Object[] displayElements = { msg, checkbox };
		JOptionPane.showMessageDialog(null, displayElements, "Generating HTML Output", JOptionPane.INFORMATION_MESSAGE);
		showHtmlMsg = checkbox.isSelected();
	}

	// ------------------------------------------------------------------------
	// standardized parts to HTML output

	public static String htmlDocumentHead(String subTitle) {

		return "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">" + nl + "<html>" + nl + "<head>" + nl
				+ "  <meta http-equiv='content-type' content='text/html; charset=UTF-8'>" + nl
				+ "  <meta name='generator' content='" + Meta.name + " " + Meta.programVersion + "'>" + nl + "  <title>"
				+ Competition.name + ": " + subTitle + "</title>" + nl + "</head>" + nl + nl
				+ "<body style='text-align: center;'>" + nl;
	}

	// title template and common style elements

	public final static String DEFAULT_DOC_TITLE_TEMPLATE = "<style>" + nl
			+ "  body, p, div, h2, h3, th, td { font-family: Verdana, sans-serif; }" + nl
			+ "  body, p, div, th, td { font-size: 80%; }" + nl
			+ "  table.simpleBorder { border-collapse: collapse; empty-cells: show; border: 2px solid black; page-break-inside: avoid; }"
			+ nl + "  table.simpleBorder td, th { border: 1px solid black; }" + nl
			+ "  .bkgrGray { background-color: #b8b8b8; }" + nl + "  .qualified { background-color: #ae8; }" + nl
			+ "  td.points    { text-align: center; }" + nl + "  div.ptsMin   { text-align: left; font-size: 60%; }"
			+ nl + "  div.ptsMax   { text-align: right; font-size: 60%; }" + nl
			+ "  td.rank      { text-align: center; font-weight: bold; padding: 1px 8px; }" + nl + "</style>" + nl + ""
			+ nl + "<h2 style='margin-bottom: 0'>$CompetitionName : $Title</h2>" + nl + nl
			+ "<table width='100%' style='border: none'><tr>" + nl + "  <td width='33%' align='left'>$Text1</td>" + nl
			+ "  <td width='33%' align='center'>$Text2</td>" + nl + "  <td width='33%' align='right'>$Text3</td>" + nl
			+ "</tr></table>" + nl;

	public static String documentHeadArea(String title, String text1, String text2, String text3) throws IOException {

		StringBuffer docTemplate = readDocTitleTemplate();
		if (docTemplate == null) {
			return "<h3>" + Competition.name + " : " + title + "</h3>" + nl + nl;
		}
		// replace areas in document head area with arguments of this method
		replaceSubString(docTemplate, "$Title", title);
		replaceSubString(docTemplate, "$Text1", text1);
		replaceSubString(docTemplate, "$Text2", text2);
		replaceSubString(docTemplate, "$Text3", text3);
		// insert values for variables
		replaceVariables(docTemplate);

		return docTemplate.toString();
	}

	public static String replaceVariables(StringBuffer text) {

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("EEE dd. LLL. yyyy HH:mm");

		// $CompetitionName
		replaceSubString(text, "$CompetitionName", Competition.name);
		// $Date
		replaceSubString(text, "$Date", LocalDateTime.now().format(dtf));

		// $Preliminary
		String prelimPrefix = "";
		if (!Competition.isCompleted() && !Competition.hasRoundsIgnoredForRanking()) {
			prelimPrefix = Lang.t("Preliminary") + " ";
		}
		replaceSubString(text, "$Preliminary", prelimPrefix);
		// $CompletedRounds
		String completedRounds = "";
		if (!Competition.isCompleted()) {
			completedRounds = Lang.t("Completed Rounds") + ": " + Competition.completedRounds();
		}
		replaceSubString(text, "$CompletedRounds", completedRounds);
		return text.toString();
	}

	public static String documentFootArea() {

		return nl + "<p style='text-align: center; font-size: 70%;'>"+Meta.promotion()+"</p>" + nl +"</body>" + nl + "</html>";
	}

	public static String tableTagSimpleBorder(String id) {

		return "<table id='" + id + "' class='simpleBorder' align='center' border='1' cellspacing='0'>";
	}

	public static String tableTagSimpleBorder(String id, String width) {

		return "<table id='" + id + "' class='simpleBorder' width='" + width + "' border='1' cellspacing='0'>";
	}

	public final static String TR_GRAY = "<tr class='bkgrGray'>";

	// ------------------------------------------------------------------------
	// title template file handling

	private static StringBuffer readDocTitleTemplate() throws IOException {

		File docTitleTemplateFile = putInOutputDir("DocTitleTemplate.html");

		StringBuffer outBuf = new StringBuffer(1000);
		try {
			// if no title file write one
			if (!docTitleTemplateFile.exists()) {
				FileOutputStream outStream = new FileOutputStream(docTitleTemplateFile);
				PrintStream fw = new PrintStream(outStream,true, StandardCharsets.UTF_8);
				fw.append(DEFAULT_DOC_TITLE_TEMPLATE);
				fw.close();
			}
			// read title file
			outBuf.append(nl + "<!-- start inserted from DocTitleTemplate.html -->" + nl);
			BufferedReader in = new BufferedReader(new FileReader(docTitleTemplateFile));
			for (;;) {
				String line = in.readLine();
				if (line == null)
					break;
				outBuf.append(line).append(nl);
			}
			in.close();
			outBuf.append("<!-- end inserted from DocTitleTemplate.html -->" + nl + nl);
		} catch (IOException ex) {
			logger.warn(ex.getMessage(), ex);
			return new StringBuffer(DEFAULT_DOC_TITLE_TEMPLATE);
		}
		return outBuf;
	}

	// ------------------------------------------------------------------------
	// IE6 and NS6 do not fully support CSS2 table borders ("eye-catching borders"
	// priority) :-(
	// need to specify fat border directly in every(!) td :-(
	// or insert multi-column, dummy cell

	public final static String FAT_BORDER = "2px solid black";
	public final static String STYLE_FAT_BORDER_LEFT = "style='border-left: " + FAT_BORDER + "'";
	public final static String STYLE_FAT_BORDER_BOTTOM = "style='border-bottom: " + FAT_BORDER + "'";
	public final static String STYLE_FAT_BORDER_BOTTOMLEFT = "style='border-bottom: " + FAT_BORDER + "; border-left: "
			+ FAT_BORDER + "'";
	public final static String TH_BOTTOM = "<th style='border-bottom: " + FAT_BORDER + "'>";

	public static String tdBottom(String otherAttrs) {
		return "<td " + otherAttrs + " style='border-bottom: " + FAT_BORDER + "'>";
	}

	static String separatorDummyRow(int colspan, int height, String bkgrColor) {

		return "<tr>" + "<td colspan='" + colspan + "' style='height: " + height + "; padding: 0; background: "
				+ bkgrColor + "; font-size: 1px;'></td>" + "</tr>";
	}

	static String separatorDummyColumn(int rowspan, int width, String bkgrColor) {

		return "<td rowspan='" + rowspan + "' style='width: " + width + "px " + "; padding: 0; background: " + bkgrColor
				+ ";'></td>";
	}

	// ------------------------------------------------------------------------
	// utility

	private static boolean replaceSubString(StringBuffer buf, String find, String replace) {

		int start = buf.toString().indexOf(find);
		if (start < 0)
			return false;
		int end = start + find.length();
		buf.replace(start, end, replace);
		return true;
	}

	public static String separateIfNotEqual(double min, double max) {
		if (min != max) {
			return "<div class='ptsMin'>" + min + "</div> <div class='ptsMax'>" + max + "</div>";
		} else {
			return "" + max;
		}
	}

	public static String pointsToHtml(RankedObject ro, int disciplineIndex) {
		return separateIfNotEqual(ro.pointsMin[disciplineIndex], ro.points[disciplineIndex]);
	}

	public static String rankToHtml(RankedObject ro, int disciplineIndex) {
		if (ro.rankDefinite(disciplineIndex)) {
			return ro.rankToString(disciplineIndex);
		} else {
			return " ";
		}
	}

}
