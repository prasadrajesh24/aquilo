/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.htmlout;

import java.util.*;
import java.io.*;

import aquilo.comparator.RideHorseNoComparator;
import aquilo.comparator.RideStartNoComparator;
import aquilo.entity.Competition;
import aquilo.entity.Horse;
import aquilo.entity.Ride;
import aquilo.entity.Round;
import aquilo.util.*;
import aquilo.gui.MainWin;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 Drawing Lots of a round as HTML
*/
public final class DrawingLots extends HtmlOutput {

    private static final Logger logger = LoggerFactory.getLogger(DrawingLots.class);

    public static File writeHtmlFile(Round rnd) throws Exception {
        File outFile = putInOutputDir("Round"+rnd.getId()+"Lots.html");
        PrintStream out = openHtmlOutputFile(outFile);

        String lotTitle = Competition.name+" : "+rnd.name;
        int horsesPerLot = (rnd.getId().endsWith("1")) ? Competition.ridersPerTeam : 1;

        out.print( htmlDocumentHead(rnd.name) );
        
        // find rules in <style> section in docTitleTemplate
        String rulesFromTemplate = "";
        String str = documentHeadArea("", "","","");
        int styleBeg = str.indexOf("<style>");
        if( styleBeg != -1 ) {
            styleBeg += 7;
            int styleEnd = str.indexOf("</style>");
            rulesFromTemplate = str.substring(styleBeg,styleEnd);
        }
        
        out.println( 
            "<style>"+nl+
            rulesFromTemplate +
            "  th   { font-weight: bold; font-size: 10pt; }"+nl+
            "  td   { font-weight: bold; font-size: 11pt; }"+nl+
            RoundListing.styleRules+
            "</style>"+nl
        );

        Ride[] lots;
        if( rnd.getId().equals("SJ1") ) {
            // the really tricky special case
            lots = createSJ1lotDistribution(rnd); 
        } else {
            // all rounds except SJ1: simply iterate over the starting order
            List<Ride> rides = rnd.getRides();
            rides.sort(new RideStartNoComparator());
            lots = rides.toArray(new Ride[0]);
        }
        Set<Horse> horsesInGroup = new HashSet<>();
        int groupCnt = 1;
        for(int i=0; i<lots.length; ) {

            // CSS page-break-inside does work in any version 6 browser :-(
            // use page-break-before as workaround: 6 team lots or 9 single lots per page
            if( i>0 && ( rnd.isFirstRound() && i%(6*3) == 0  ||  !rnd.isFirstRound() && i%9 == 0 ) )
                out.println("<div style='page-break-before: always;'></div>" );

            out.println( tableTagSimpleBorder(rnd.getId()+"drawingLot","100%") );
            String groupStr = "";
            if(rnd.horsesPerGroup.length>1) {
                horsesInGroup.add(lots[i].getHorse());
                if(horsesInGroup.size()>rnd.horsesPerGroup[groupCnt-1]) {
                    groupCnt++;
                    horsesInGroup.clear();
                }
                groupStr = " - "+Lang.t("Group")+" "+groupCnt;
            }
            int colCnt = (Competition.isInternational()) ? 7 : 6;  // additional horse Order column?
            int LotNumber = (rnd.isFirstRound()) ? ((i/3)+1) : i+1;
            out.println( "<tr><th colspan='"+colCnt+"' "+STYLE_FAT_BORDER_BOTTOM+">" + lotTitle + groupStr + "<div style=\"float: right\">" + Lang.t("Lot Number: ")  + LotNumber + "</div>" + THe+TRe );
            out.print( RoundListing.roundTableHeader(rnd,false) );
            for(int j=0; j<horsesPerLot && i<lots.length; j++,i++ ) {

                List<?> nullList = new ArrayList<>(); //empty List is needed because ResultList needs to pass the List of Qualified Riders
                out.print( RoundListing.rideToHtmlTableRow(lots[i],false,"25", nullList) );

                // count horses to find beginning of a group
                horsesInGroup.add(lots[i].getHorse());
            }
            out.println(TABLEe + BR);
        }
        out.print( BODYe + HTMLe );
        out.close();
        return outFile;
    }

    private static Ride[] createSJ1lotDistribution(Round rnd) throws Exception {
        // This may seem a bit complicated but also works when noOfTeams is NOT divisible
        // by 3 (WUEC noOfTeams=19). I.e. there can be only 2 ridersPerHorse anywhere
        // in the starting order!!

        int horsesPerLot = Competition.ridersPerTeam;

        List<Ride> rides_ = rnd.getRides();
        rides_.sort(new RideHorseNoComparator());
        Ride[] rides = rides_.toArray(new Ride[0]);
        logger.info("{}",rides.length);
        /*
        sorted by horse&start a 10-team competition SJ1 rides[i] looks like this:

        i             0   1   2     3   4   5     6   7   8       9  10   11  12   13  14    15
        horse          Calvaro       RatinaZ      Abdullah          D        E        F
        riderOnHorse  1   2   3     1   2   3     1   2   3       1   2    1   2    1   2     1
                      <---------- horsesPerLot=3 --------->       <-- horsesPerLot=3 --->
                      <------->                                   <--->
                   ridersPerHorse=3                          ridersPerHorse=2

        horseCntStart 0                                           9                          15
        horseCnt      1   1   1     2   2   2     3   3   3       1   1    2   2    3   3     1
        lotOffset     0                                           3                           5
        lotSelect     0   1   2     2   0   1     1   2   0       0   1    1   0    0   1     0
                      <-------- a "lot group" ------------>       <--- a "lot group" --->
        */
        final int noOfLots = Competition.noOfTeams();
        int ridersPerHorse = 99;              // actual val only known at 2nd horse
        int horseCntStart = 0,  horseCnt = 0;
        Horse currentHorse = null;
        int lotOffset = 0,  lotSelect = 0;
        // use a temporary 2-dim array-list structure to place the rides
        List<List<Ride>> lots = new ArrayList<>(noOfLots);
        for(int l=0; l<noOfLots; l++){
            lots.add(new LinkedList<>());
        }

        for(int i=0; i < rides.length; i++) {

            if( currentHorse != rides[i].getHorse() ) {
                // 1st ride of (next) horse
                horseCnt++;
                if( horseCnt == 2 ) {
                    // at the beginning of the 2nd horse of a lot group we know:
                    ridersPerHorse = i - horseCntStart;    // usually =3 or =2
                }
                else {
                    // horseCnt==3,4: ridersPerHorse must be equal for all horses in lot group
                    if( (i-horseCntStart) % ridersPerHorse != 0 )
                        throw new Exception("Number of starts for "+
                        "horse "+rides[horseCntStart].getHorse().getHorseNo()+" "+rides[horseCntStart].getHorse().name+" and "+
                        "horse "+currentHorse.getHorseNo()+" "+currentHorse.name+" not equal\n"+
                        "Edit  "+MainWin.fileHandling.getFile().getAbsolutePath()+"  manually or use 'Reset Round' to recover");
                }
                currentHorse = rides[i].getHorse();
                if( horseCnt > horsesPerLot ) {
                    // we're at the beginning of the next lot group
                    horseCnt = 1;
                    horseCntStart = i;
                    lotOffset += ridersPerHorse;
                    lotSelect = 0;
                    ridersPerHorse = 99;
                }
                // lotSelect = lotSelect
            }
            else {
                // 2nd, 3rd, ..  ride of horse
                lotSelect = (lotSelect+1) % ridersPerHorse;
            }
            int lotIndex = lotOffset+lotSelect;
            //printSJ1params(i, rides[i], horseCnt, ridersPerHorse, lotOffset, lotSelect, lotIndex);
            if(lotIndex>=noOfLots) throw new Exception("lotIndex >= noOfLots");
            // place rides on lot so that they are ordered by start
            ListIterator<Ride> iter = lots.get(lotIndex).listIterator();  int pos = 0;
            while( iter.hasNext() && (iter.next()).startNo < rides[i].startNo ) pos++;
            lots.get(lotIndex).add(pos,rides[i]);
        }
        // check if we end OK
        if(horseCnt != horsesPerLot)
            throw new Exception("Rides missing at the end of "+rnd.name+"\n"+
            "Edit  "+MainWin.fileHandling.getFile().getAbsolutePath()+"  manually or use 'Reset Round' to recover");

        // dump the 2-dim array-list structure into a simple array
        Ride[] lotseq = new Ride[rides.length];
        int i = 0;
        for(int lotIndex=0; lotIndex<noOfLots; lotIndex++) {
            for (Ride ride : lots.get(lotIndex)) {
                //System.out.println(lotIndex+"  "+ride);
                lotseq[i++] = ride;
            }
        }
        return lotseq;
    }

    private static void printSJ1params(int i, Ride r, int horseCnt, int ridersPerHorse,
                                       int lotOffset, int lotSelect, int lotIndex) {
        logger.info(
                Fmt.fmt(i) + "  " + Fmt.fmt(r.getHorse().name, 18) + Fmt.fmt(r.riderOnHorse, 1) +
                        Fmt.fmt(horseCnt) + Fmt.fmt(ridersPerHorse) +
                        Fmt.fmt(lotOffset) + Fmt.fmt(lotSelect) + Fmt.fmt(lotIndex)
        );
    }

}