/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.htmlout;

import java.util.*;
import java.io.*;

import aquilo.entity.Discipline;
import aquilo.entity.Ride;
import aquilo.entity.Rider;
import aquilo.ranking.*;
import aquilo.util.Lang;

/**
    Individual ranking of DR or SJ with results for each round in columns side by side
*/
public final class BasicIndividualRanking extends HtmlOutput {
    
    public static File writeHtmlFile(Discipline discipline, String filename) throws IOException {
        
        if(filename==null) filename = "RankingIndividual"+discipline.name+".html";
        File outFile = putInOutputDir(filename);
        PrintStream out = openHtmlOutputFile(outFile);

        // document head, table top caption
        String title = Lang.t("Individual Ranking")+" "+discipline.name;
        out.print( htmlDocumentHead(title) );

        String textLeft = "";
        if( discipline.roundsIgnoredForRanking() )
            textLeft = "Nur "+discipline.getIgnoreRoundsAbove()+" Runden ber\u00fccksichtigt!";
        String completedRounds = "";
        if( ! discipline.isCompleted() && !discipline.roundsIgnoredForRanking() ) {
            completedRounds = Lang.t("Completed Rounds")+": "+discipline.completedRounds(",");
            title = Lang.t("Preliminary")+" "+title;
        }
        

        out.append( documentHeadArea(title,textLeft,completedRounds,"$Date") );
        out.println( tableTagSimpleBorder("RankingIndividual"+discipline.name,"100%") );
        String row = TR_GRAY;
        row += TH_BOTTOM+Lang.t("Rank")+THe;
        row += TH_BOTTOM+Lang.t("Rider")+THe;
        row += TH_BOTTOM+Lang.t("Team")+THe;
        for(int cnt=1; cnt<=discipline.getNoOfRounds(); cnt++ ) {
            row += "<th colspan='3' "+STYLE_FAT_BORDER_BOTTOM+">"+cnt+". "+Lang.t("Round")+THe;
        }
        row += TH_BOTTOM+Lang.t("Points")+THe;
        row += TRe+nl;
        out.println(row);

        // make sure the latest results are used
        List ranking = PointsAndRanking.baseDisciplineDiffsToPointsAndRanks(discipline);
        int di = discipline.getIndex();
        
        for(int i=0; i<ranking.size(); i++) {
            Rider rider = (Rider)ranking.get(i);
            row = TR;
            row += TDC+ B+rider.rankToString(di)+Be + TDe;
            row += TD + rider.firstName +' '+rider.getLastName() + TDe;
            row += TD + rider.getTeam().name + TDe;
            int rndColumn = 0;
            for(Iterator it=rider.ridesIterator(di); it.hasNext(); ) {
                Ride ride = (Ride)it.next();
                if(ride.getRound().isIgnoredForRanking()) break;
                if( ride.getRound().getDiscipline().getIndex() != di ) continue;
                row += TDC+ride.getHorse().getHorseNo() + TDe;
                row += TDR+ride.getResult().toString() + TDe;
                row += TDR+ride.getResult().diffToString() + TDe;
                rndColumn++;
            }
            int remainingRoundColumns = discipline.getNoOfRounds()-rndColumn;
            if(remainingRoundColumns>0)
                row += "<td colspan="+(3*remainingRoundColumns)+">&nbsp;";
            row += TDC+pointsToHtml(rider,di) + TDe;
            row += TRe;
            out.print(row);
        }
        out.print(TABLEe);
    	out.print( BODYe );
    	out.close();
    	return outFile;
    }

}