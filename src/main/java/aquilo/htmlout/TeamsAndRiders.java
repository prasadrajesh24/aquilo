/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.htmlout;

import java.util.*;
import java.io.*;

import aquilo.entity.Competition;
import aquilo.entity.Rider;
import aquilo.entity.Team;

/**
    Generate HTML table with teams, rider names and two empty columns for notes
*/
public final class TeamsAndRiders extends HtmlOutput {
    
    public static File writeHtmlFile() throws IOException {

        File outFile = putInOutputDir("TeamsAndRiders.html");
        PrintStream out = openHtmlOutputFile(outFile);

        String title = "All Teams and Riders";
        out.append( htmlDocumentHead(title) );
        out.append( documentHeadArea(title,"","","$Date") );
        if(Competition.teams==null) return outFile;
        
        out.println(
            "<style>"+nl+
            "  td.teamName  { font-weight: bold; }"+nl+
            "  td.bootNo    { text-align: center; }"+nl+
            "  td.riderName {  }"+nl+
            "  td.notes     {  }"+nl+
            "</style>"+nl
        );
        out.println( tableTagSimpleBorder("TeamsAndRiders","100%") );
        for(Iterator it=Competition.teams.iterator(); ; ) {
            Team t = (Team)it.next();
            out.println("<tbody>");
            out.println("<tr>");
            out.println("  <td class='teamName' width='40%' colspan='2'>"+t.name+" ("+t.abbreviation+")</td>");
            out.println("  <td class='notes' width='30%'>&nbsp;</td>");
            out.println("  <td class='notes' width='30%'>&nbsp;</td>");
            out.println("</tr>");
            for(Iterator rIt=t.riderIterator(); rIt.hasNext(); ) {
                Rider r = (Rider)rIt.next();
                out.println("<tr>");
                out.println("  <td class='bootNo'>"+r.bootNo+"</td>");
                out.println("  <td class='riderName'>"+r.firstName +" "+r.getLastName()+"</td>");
                out.println("  <td class='notes'>&nbsp;</td>");
                out.println("  <td class='notes'>&nbsp;</td>");
                out.println("</tr>");
            }
            if( ! it.hasNext() ) break;
            out.println(separatorDummyRow(4,6,"white"));
            out.println("</tbody>"+nl);
        }        
    	out.print(TABLEe);
    	out.print( documentFootArea() );
    	
        out.close();
        return outFile;
    }
        
}