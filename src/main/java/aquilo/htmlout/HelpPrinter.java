/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.htmlout;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

public final class HelpPrinter extends HtmlOutput {

    public static File writeHtmlFile(String fileName) throws IOException {

        // TODO: 28.08.22 Print file into aquilo.jar location (opens automatically --> user doesn't need file location)
        // TODO: 25.06.23 is there a possibility to know from start where the program is located --> help.html and start folder suggestion

        if (fileName == null) {
            fileName = "AquiloHelp.html";
        }

        File helpFile = new File("./" + fileName);
        if (helpFile.exists()) {
            return helpFile;
        }

        File outFile = new File(fileName);
        PrintStream out = new PrintStream(new FileOutputStream(helpFile), true, StandardCharsets.UTF_8);

        InputStream content = Objects.requireNonNull(HelpPrinter.class.getResourceAsStream("/help/AquiloHelp.html"));
        BufferedReader br = new BufferedReader(new InputStreamReader(content));
        String str;
        while ((str = br.readLine()) != null) {
            out.println(str);
        }

        out.close();
        return outFile;
    }

}
