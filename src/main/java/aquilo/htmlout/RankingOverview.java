/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.htmlout;

import java.util.*;
import java.io.*;

import aquilo.entity.*;
import aquilo.ranking.*;
import aquilo.util.Lang;

/**
    Individual and team rankings without details.
    
    One table for the three individual rankings and
    one for the three team rankings with the disciplines side by side
*/
public final class RankingOverview extends HtmlOutput {

    public static File writeHtmlFile(String filename) throws IOException {

        PointsAndRanking.assignAll();
        if(filename==null) filename = "RankingOverview.html";
        File outFile = putInOutputDir(filename);
        PrintStream out = openHtmlOutputFile(outFile);

        String title = Lang.t("Results Overview");
        out.print( htmlDocumentHead(title) );
        
        StringBuffer rndDateVars = new StringBuffer("$CompletedRounds &nbsp; $Date");
        String roundsAndDate = "<h4>"+replaceVariables(rndDateVars)+"</h4>";

        out.println( documentHeadArea("$Preliminary"+title, "","","") );
        out.println( roundsAndDate );
        out.println( "<h3>"+Lang.t("Individual Ranking")+"</h3>" );

        // Individual Rankings
        
        out.println( tableTagSimpleBorder("RankingOverviewIndividual") );
        String row = TR;
        if (Competition.isCHU_GER()) {
            row += "<th colspan='2'>" + Lang.t("Combined") + "</th> " + separatorDummyColumn(Competition.disciplines[Discipline.DR].getRound(1).noOfRides() + 1, 20, "white");
            row += "<th colspan='2'>" + Lang.t("Dressage") + "</th> " + separatorDummyColumn(Competition.disciplines[Discipline.DR].getRound(1).noOfRides() + 1, 20, "white");
        }else {
            row += "<th colspan='2'>" + Lang.t("Combined") + "</th> " + separatorDummyColumn(Competition.noOfRiders() + 1, 20, "white");
            row += "<th colspan='2'>" + Lang.t("Dressage") + "</th> " + separatorDummyColumn(Competition.noOfRiders() + 1, 20, "white");
        }
        row += "<th colspan='2'>"+Lang.t("Showjumping")+"</th> ";
        row += TRe+nl;
        out.print(row);

        List<ArrayList<RankedObject>> ranking = new ArrayList<>(Discipline.MAX_DISCIPLINE);
        for(int di=Discipline.COMB; di<=Discipline.LAST; di++) {
            ranking.add(di, new ArrayList<>(Competition.riders));
        }
        ranking.get(Discipline.COMB).sort(new IndividualCombinedComparator());
        ranking.get(Discipline.DR).sort(new IndividualDressageComparator());
        ranking.get(Discipline.SJ).sort(new IndividualShowjumpingComparator());

        int rnd2Size = Competition.disciplines[Discipline.DR].getRound(1).noOfRides();
        int maxRider = (Competition.hasMultipleTeamRankings()) ? Competition.noOfRiders() : rnd2Size;
        for(int i=0; i<maxRider; i++) {
            row = TR;
            String td1Tag = (i==rnd2Size-1) ? tdBottom("class='rank'") : "<td class='rank'>";
            String td2Tag = (i==rnd2Size-1) ? tdBottom("") : TD;
            for(int di=Discipline.COMB; di<=Discipline.LAST; di++) {
                Rider rider = (Rider) ranking.get(di).get(i);
                row += td1Tag + rider.rankToString(di) + TDe;
                row += td2Tag + rider.fullName() + " ("+rider.getTeam().abbreviation+')' +TDe;
            }
            row += TRe;
            out.print(row);
        }
        out.print(TABLEe);

        // Team Ranking(s)
        
        if( Competition.hasMultipleTeamRankings() ) {
            
            // all team rankings on an extra page, team names only
            
            out.println( nl+"<p style='page-break-before: always'>");
            out.println( documentHeadArea("$Preliminary"+title, "","","") );
            out.println( roundsAndDate );
            out.println( "<h3>"+Lang.t("Team Ranking")+"</h3>" );

            out.println( tableTagSimpleBorder("RankingOverviewTeam") );
            row = new String(TR);
            row += "<th colspan='2'>Combined</th> " + separatorDummyColumn(Competition.noOfTeams()+1,20,"white");
            row += "<th colspan='2'>Dressage</th> " + separatorDummyColumn(Competition.noOfTeams()+1,20,"white");
            row += "<th colspan='2'>Showjumping</th> ";
            row += TRe;
            out.print(row);
            for(int di=Discipline.COMB; di<=Discipline.LAST; di++) {
                ranking.set(di, new ArrayList<>(Competition.teams));
            }
            ranking.get(Discipline.COMB).sort(new TeamCombinedComparator());
            ranking.get(Discipline.DR).sort(new TeamDressageComparator());
            ranking.get(Discipline.SJ).sort(new TeamShowjumpingComparator());
            for(int i=0; i<Competition.noOfTeams(); i++) {
                row = TR;
                for(int di=Discipline.COMB; di<=Discipline.LAST; di++) {
                    Team team = (Team) ranking.get(di).get(i);
                    row += "<td class='rank'>" + team.rankToString(di) + TDe;
                    row += "<td class='teamName'>" + team.name+" ("+team.abbreviation+')' + TDe;
                }
                row += TRe;
                out.print(row);
            }
        }

        else {
            
            // combined team ranking only, with rider names
            
            out.println(BR);
            out.println("<h3>"+Lang.t("Team Ranking")+"</h3>");
            
            out.println( tableTagSimpleBorder("RankingOverviewTeam") );
            ArrayList<Team> teamCombRanking = new ArrayList<>(Competition.teams);
            teamCombRanking.sort(new TeamCombinedComparator() );
            if (Competition.isDHM){
                removeEinzelReiterTeams(teamCombRanking);
            }
            for(int i=0; i<teamCombRanking.size(); i++) {
                Team team = teamCombRanking.get(i);
                row = TR;
                row += "<td class='rank'>" + team.rankToString(Discipline.COMB) + TDe;
                row += "<td>" + team.name+" ("+team.abbreviation+") " + team.pointsToString(Discipline.COMB)+" "+Lang.t("Points") + BR;
                row += "<span style='font-size: 85%'>"+team.ridersToStringList() +"</span>" + TDe;
                row += TRe;
                out.print(row);
            }
        }
        out.print(TABLEe);

        //out.println("<p style='text-align: center; font-size: 70%;'>"+Meta.promotion()+"</p>" );
    	out.print( documentFootArea() );
    	out.close();
    	return outFile;
    }

    private static void removeEinzelReiterTeams(ArrayList<Team> teamComb){              // if we have a DHM ranking we do not need the Einzelreiterteams in the final ranking table

        for (Iterator<Team> iter = teamComb.listIterator(); iter.hasNext();) {
            String Abbrev = iter.next().abbreviation;

            if (Abbrev.equals("D1") || Abbrev.equals("D2") || Abbrev.equals("D3")){
                iter.remove();
            }
        }

        for (int i = 0; i < teamComb.size(); i++) {
            teamComb.get(i).rank[0] = i+1;
        }

    }

}