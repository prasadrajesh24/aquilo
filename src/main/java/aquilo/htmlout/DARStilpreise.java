/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.htmlout;

import java.io.*;

import aquilo.entity.Competition;
import aquilo.gui.StilpreisDialog;
import aquilo.ranking.*;

/**
 * Write the Stilpreiswinners to a File
 *  This is to ensure that the DAR gets the necessary Data of the Winners
 */

public final class DARStilpreise extends HtmlOutput {

    public static File writeFile(String filename) throws IOException{
        PointsAndRanking.assignAll();
        if(filename==null) filename = "Stilpreisgewinner "+ Competition.name+".txt";
        File outFile = putInOutputDir(filename);
        PrintStream out = openHtmlOutputFile(outFile);

        out.println("Stilpreisgewinner der Veranstaltung: " + Competition.name);
        out.println("Exchange File Version 1.0");
        out.println("\n");

        out.println("Legende: |\t Vorname: | Nachname: | Stra\u00dfe: | PLZ: | Stadt: ");
        out.println("\n");

        out.println(StilpreisDialog.getStilpreise());

        out.close();
        return outFile;
    }
}
