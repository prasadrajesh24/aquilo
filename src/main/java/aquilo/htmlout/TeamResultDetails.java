/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.htmlout;

import java.util.*;
import java.io.*;

import aquilo.entity.*;
import aquilo.ranking.*;
import aquilo.util.Lang;
import aquilo.util.Penalties;

/**
    Large table with teams and riders in input order (not ordered by any ranking).
    Shows for each rider results of all his rides in a small inner table.
*/
public final class TeamResultDetails extends HtmlOutput {

    public static final boolean showRiderResultsInResultDetail = true;
    private final static String teamrows = " rowspan='"+ Competition.ridersPerTeam+"'>";

    static final String TEAM_NAME       = "<td class='teamName'>";
    static final String RIDER_NAME      = "<td class='riderName' align='left'>";
    static final String RIDER_BOOTNO    = "<td class='riderBootNo'>";
    static final String POINTS          = "<td class='points'>";
    static final String RANK            = "<td class='rank'>";
    static final String RIDER_SUM       = "<td class='riderSum'>";
    static final String DR_TEAM_MARK    = "<td class='drTeamMark'>";
    static final String DR_TEAM_PTS     = "<td class='drTeamPts'>";

    //------------------------------------------------------------------------
    
    public static File writeHtmlFile(String filename, Comparator<RankedObject> teamComparator) throws IOException {

        PointsAndRanking.assignAll();
        if(filename==null) filename = "TeamResultDetails.html";
        File outFile = putInOutputDir(filename);
        PrintStream out = openHtmlOutputFile(outFile);

        boolean extraTeamRankings = Competition.hasMultipleTeamRankings();

        String title = Lang.t("Team Result Details");
        out.print( htmlDocumentHead(title) );

        // document head, table top caption

        String textLeft = "Team Pts* = "+PointsAndRanking.getTeamCombinedFormula();
        String textCenter = (Competition.hasRoundsIgnoredForRanking())
                          ? Competition.roundsConsideredForRanking()
                          : "$CompletedRounds";
        out.print( documentHeadArea("$Preliminary"+title,textLeft,textCenter,"$Date") );

        out.println("<style>table.riderResults td { border: none; }</style>"+nl);
        
        // table
        
        out.println( tableTagSimpleBorder("TeamResultDetails","100%") );
        out.println("<colgroup span=3 class='teamAndRider'>");
        out.println("<colgroup span=3 class='drColumns'></colgroup>");
        out.println("<colgroup span=3 class='sjColumns'></colgroup>");
        out.println("<colgroup span=8 class='combColumns'></colgroup>");

        //out.print("<thead>");  // would disable provisory use of separatorDummyColumn
        String row = TR_GRAY;
        row += "<th colspan=1 rowspan=3>"+Lang.t("Team")+THe;
        row += "<th colspan=2 rowspan=2>"+Lang.t("Rider")+THe;
        row += separatorDummyColumn(99,1,"black");
        row += "<th colspan="+((extraTeamRankings)?8:3)+" rowspan=1>"+Lang.t("Dressage")+THe;
        row += separatorDummyColumn(99,1,"black");
        row += "<th colspan="+((extraTeamRankings)?5:3)+" rowspan=1>"+Lang.t("Showjumping")+THe;
        row += separatorDummyColumn(99,1,"black");
        if (Penalties.penalties) {
            row += "<th colspan=" + ((extraTeamRankings) ? 4 : 9) + " rowspan=1>" + Lang.t("Combined") + THe;
        } else {
            row += "<th colspan=" + ((extraTeamRankings) ? 4 : 8) + " rowspan=1>" + Lang.t("Combined") + THe;
        }
        row += TRe;
        out.print(row);

        row = TR_GRAY;
        row += "<th colspan=3>"+Lang.t("Individual") + THe;
        if(extraTeamRankings) row += "<th colspan=5>"+Lang.t("Team")+THe;
        row += "<th colspan=3>"+Lang.t("Individual")+THe;
        if(extraTeamRankings) row += "<th colspan=2>"+Lang.t("Team")+THe;
        row += "<th colspan=2>"+Lang.t("Individual")+THe;
        if (Penalties.penalties) {
            row += "<th colspan=" + ((extraTeamRankings) ? 2 : 7) + ">" + Lang.t("Team") + THe;
        } else {
            row += "<th colspan=" + ((extraTeamRankings) ? 2 : 6) + ">" + Lang.t("Team") + THe;
        }
        row += TRe;
        out.println(row);

        row = TR_GRAY;
        row += TH+"No"+THe + TH+"Name"+THe;
        row += TH+Lang.t("Results")+THe + TH+"Pts"+THe + TH+"Rnk"+THe;      // DR indiv
        if(extraTeamRankings)  row += TH+"Pts"+THe + TH+"TMark"+THe + TH+"TPts"+THe + TH+"FPts"+THe + TH+"Rnk"+THe;
        row += TH+Lang.t("Results")+THe + TH+"Pts"+THe + TH+"Rnk"+THe;      // SJ indiv
        if(extraTeamRankings)  row += TH+"Pts"+THe + TH+"Rnk"+THe;
        row += TH+"Pts"+THe + TH+"Rnk"+THe;                     // COMB indiv
        if(!extraTeamRankings) {
            row += TH+"DrMark"+THe + TH+"DrTPts"+THe;            // COMB team
            row += TH+"DrPts"+THe + TH+"SjPts"+THe;
        }
        if (Penalties.penalties) {
            row += TH + Penalties.displayShortName + THe + TH + "Pts*" + THe + TH + "Rnk" + THe;
        } else {
            row += TH + "Pts*" + THe + TH + "Rnk" + THe;
        }
        row += TRe;
        out.println(row);
        //out.println("</thead>");

        String[] trows = new String[Competition.ridersPerTeam];

        List<Team> outOrder = new ArrayList<>(Competition.teams);
        if(teamComparator!=null){
            outOrder.sort(teamComparator);
        }
        for(int i=0; i<outOrder.size(); i++) {
            Team t = outOrder.get(i);
            out.println(nl+separatorDummyRow(20,1,"black"));
            //out.print("<tbody>");  // would disable provisory use of separatorDummyColumn
            int r = 0;
            for(Iterator<Rider> rit=t.riderIterator(); rit.hasNext(); ) {
                Rider rider = rit.next();
                trows[r] = TRC;
                trows[r] += ((r==0) ? "<td class=teamName"+teamrows+B+t.name+Be+TDe : "")+nl;
                trows[r] += RIDER_BOOTNO + rider.bootNo+TDe;
                trows[r] += RIDER_NAME + rider.firstName +NBSP+rider.getLastName() + TDe+nl;
                trows[r] += "<!-- DR -->"+disciplineColumns(r, t,rider, Discipline.DR)+nl;
                trows[r] += "<!-- SJ -->"+disciplineColumns(r, t,rider, Discipline.SJ)+nl;
                trows[r] += "<!-- CO -->"+disciplineColumns(r, t,rider, Discipline.COMB);
                r++;
            }
            for(r=0; r<Competition.ridersPerTeam; r++)  out.print(trows[r]);
            //out.println(nl+"</tbody>");
        }
        out.println(TABLEe);
        out.print( documentFootArea() );
        out.close();
        return outFile;
	}

	private static String disciplineColumns(int r, Team t,Rider rider, int disciplineIndex) {
        boolean extraTeamRankings = Competition.hasMultipleTeamRankings();
	    String cells = "";
        if(disciplineIndex!=Discipline.COMB && showRiderResultsInResultDetail) {
            cells += "<td class='riderResultTable'>" + riderResultsTable(rider,disciplineIndex)+nl+"           ";
        }
        cells += "<td class='points'>"+pointsToHtml(rider,disciplineIndex)+TDe;
        cells += "<td class='rank'>"+rankToHtml(rider,disciplineIndex)+TDe;
        if(r==0) {  // team values in first of three rows
            if( extraTeamRankings && disciplineIndex==Discipline.DR) {
                cells += riderSumToCell(t,Discipline.DR);       // SRNC: teamDr -> teamDR ranking
                cells += drTeamValuesToCells(t,Discipline.DR);
            }
            if( !extraTeamRankings && disciplineIndex==Discipline.COMB) {
                cells += drTeamValuesToCells(t,Discipline.DR);  // WUEC: teamDr -> TeamCOMB ranking
                cells += riderSumToCell(t,Discipline.DR);
                cells += riderSumToCell(t,Discipline.SJ);
            }
            if( extraTeamRankings || disciplineIndex==Discipline.COMB) {
                if (Penalties.penalties) {
                    cells += "<td class='penalty'"+teamrows + t.penaltyPoints + TDe;
                }
                cells += "<td class='points'"+teamrows + pointsToHtml(t,disciplineIndex) + TDe;
                cells += "<td class='teamRnk'"+teamrows + "<b>" + rankToHtml(t,disciplineIndex) + TDe;
            }
        }
        return cells;
	}

    private static String drTeamValuesToCells(Team t, int disciplineIndex) {
        String cells = "";
        String GerDiffString = "<div style=\"font-size:60%;display:inline\"> ";
        if (Competition.isCHU_GER()) {
            cells += "<td class='drTeamMark'" + teamrows + t.dressageResult + GerDiffString + t.dressageResult.diffToString() + "</div>" + TDe;
            cells += "<td class='drTeamPts'" + teamrows + separateIfNotEqual(t.teamDrPointsMin, t.teamDrPoints) + TDe;
        }else {
            cells += "<td class='drTeamMark'" + teamrows + t.dressageResult + TDe;
            cells += "<td class='drTeamPts'" + teamrows + separateIfNotEqual(t.teamDrPointsMin, t.teamDrPoints) + TDe;
        }
        return cells;
    }

    private static String riderSumToCell(Team t, int disciplineIndex) {
        double sum = t.riderPointsSum(disciplineIndex);
        double sumMin = t.riderPointsMinSum(disciplineIndex);
        return "<td class='riderSum'"+teamrows + separateIfNotEqual(sumMin,sum) + TDe;
    }
    
    //--------------------------------------------------------------------------

    static final String ROUND_ID        = "<td class='roundId'>";
    static final String HORSE_NO        = "<td class='horseNo' align='right'>";
    static final String RESULT          = "<td class='result' align='right'>";
    static final String DIFF            = "<td class='diff' align='right'>";

    public static String riderResultsTable(Rider rider, int disciplineIndex) {
        String tbl;
        tbl = "<table class='riderResults' width='100%' cellspacing='0' cellpadding='0' style='font-size: 80%'>";
        tbl += "<col width='13%'><col width='13%'><col width='37%'><col width='37%'>";
        for(Iterator<Ride> rit=rider.ridesIterator(disciplineIndex); rit.hasNext(); ) {
            Ride ride = rit.next();
            if(ride.getRound().isIgnoredForRanking()) break;
            tbl += "<tr>";
            tbl += ROUND_ID + ride.getRound().getId() + TDe;
            tbl += HORSE_NO + ride.getHorse().getHorseNo() + TDe;
            tbl += RESULT + ride.getResult().toString() + TDe;
            tbl += DIFF + ride.getResult().diffToString() + TDe;
            tbl += TRe;
        }
        tbl += "</table>";
        return tbl;
    }

}