/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.exception;

public class OldSaveFileException extends RuntimeException {

    public OldSaveFileException() {
        super();
    }

    public OldSaveFileException(String msg) {
        super(msg);
    }

    public OldSaveFileException(Throwable cause) {
        super(cause);
    }

    public OldSaveFileException(String msg, Throwable cause) {
        super(msg, cause);
    }

}
