/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.ranking;

import java.util.Comparator;

import aquilo.entity.Result;
import aquilo.entity.ResultToPoints;
import aquilo.entity.Team;

/**
    Sorts teams with best (=highest) team dressage mark first
*/

public class TeamDressageMarkComparator implements Comparator<ResultToPoints> {

    public int compare(ResultToPoints o1, ResultToPoints o2) {
        Result r1 = ((Team)o1).dressageResult;
        Result r2 = ((Team)o2).dressageResult;
        //System.out.println("Team1: " + o1.toString() + "\n" + "Team 2: " + o2.toString());
        return r1.compare(r2) * -1;
    }

    public int compareDiff(ResultToPoints o1, ResultToPoints o2){
        Result r1 = ((Team)o1).dressageResult;
        Result r2 = ((Team)o2).dressageResult;
        //System.out.println("Team1: " + o1.toString() + "\n" + "Team 2: " + o2.toString());
        return r1.compareDiff(r2);
    }

    public String getOrderName() {
        return "Difference";
    }
}



