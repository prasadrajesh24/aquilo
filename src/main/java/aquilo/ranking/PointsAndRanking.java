/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.ranking;

import java.util.*;

import aquilo.comparator.RideDifferenceComparator;
import aquilo.entity.*;
import aquilo.gui.MessageWindow;
import aquilo.util.*;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * Generic algorithms which work on Collections of basic datastructures in
 * aquilo.* like Ride, Rider, Team, RankableObject.
 * 
 * resultToPoints() converts differences or results to ranking points.
 * pointsToRank() converts ranking points to integer ranks.
 */

public final class PointsAndRanking {

	private static final Logger logger = LoggerFactory.getLogger(PointsAndRanking.class);

	/**
	 * Assign ranking points and ranks for individual and team in all
	 * disciplines.
	 */

	private static final int teamDR = -17;

	public static void assignAll() {

		Penalties.check(); //load Penalties File if present

		if (Team.dressageResultsComplete()) {
			logger.info("Converting TeamDressageMarks to points");
			// System.out.println(" Team List before Sort: " + Competition.teams.toString());
			resultToPoints(Competition.teams, new TeamDressageMarkComparator(), teamDR, 0);
			// System.out.println(" Team List after Sort: " + Competition.teams.toString());
		} else {
			logger.warn("Not all TeamDressageMarks available; assigning min/max points");
			if (Competition.isCHU_GER()) {
				MessageWindow.showNoTeamDrMarks();
			}
			assignMinMaxConstPoints(Competition.teams, teamDR);
		}

		baseDisciplineDiffsToPointsAndRanks(Competition.disciplines[Discipline.DR]);
		baseDisciplineDiffsToPointsAndRanks(Competition.disciplines[Discipline.SJ]);
		logger.info("Converting rider COMB points to ranks");
		pointsToRank(Competition.riders, new IndividualCombinedComparator(), Discipline.COMB);

		for (Team t : Competition.teams) {
			assignTeamCombinedPoints(t);
		}

		logger.info("Converting team points to ranks");
		pointsToRank(Competition.teams, new TeamDressageComparator(), Discipline.DR);
		pointsToRank(Competition.teams, new TeamShowjumpingComparator(), Discipline.SJ);
		pointsToRank(Competition.teams, new TeamCombinedComparator(), Discipline.COMB);

	}

	/**
	 * For a specified Discipline call resultToPoints() for all Rounds and then
	 * pointsToRank(). If a Round is not complete it is ignored and all higher
	 * rounds as well.
	 */

	public static List<? extends RankedObject> baseDisciplineDiffsToPointsAndRanks(Discipline discipline) {

		int di = discipline.getIndex();

		// reset all rider points in discipline
		assignMinMaxConstPoints(Competition.riders, di);

		// ride differences to rider-points
		int finalRound = discipline.getIgnoreRoundsAbove();
		for (int i = 0; i < discipline.getNoOfRounds(); i++) {
			Round rnd = discipline.getRound(i);
			if (!rnd.isCompleted() || rnd.getLevel() > finalRound)
				break;
			boolean ignoreHigherRounds = rnd.getLevel() == finalRound;
			int nextRndSize = (ignoreHigherRounds) ? 0 : discipline.getNextRoundSize(rnd);
			logger.info(rnd.getId() + " converting differences to points greater " + nextRndSize);
			resultToPoints(rnd.getRides(), new RideDifferenceComparator(), di, nextRndSize);
		}

		// rider-points to rider-ranks
		logger.info("Converting rider " + discipline.getId() + " points to ranks");

		Comparator<RankedObject> comp;
		switch (discipline.getIndex()) {
			case Discipline.DR:
				comp = new IndividualDressageComparator();
				break;
			case Discipline.SJ:
				comp = new IndividualShowjumpingComparator();
				break;
			default:
				throw new RuntimeException("baseDisciplineDiffsToRanks: invalid disciplineIndex");
		}

		return pointsToRank(Competition.riders, comp, discipline.getIndex());
	}

	// -------------------------------------------------------------------------------------

	private static void assignMinMaxConstPoints(List<?> qualified, int disciplineIndex) {

		double pointsMin = (Competition.isCHU_GER()) ? 0.0 : 1.0;
		double pointsMax = qualified.size();
		for (Object o : qualified) {
			if (o instanceof Rider)
				((Rider) o).setPoints(pointsMin, pointsMax, disciplineIndex);
			else if (o instanceof Ride)
				((Ride) o).setPoints(pointsMin, pointsMax, disciplineIndex);
			else if (o instanceof Team)
				((Team) o).setPoints(pointsMin, pointsMax, disciplineIndex);
		}
	}

	/**
	 * Use a List of ResultToPoints objects to assign ranking points to other
	 * objects.
	 * 
	 * This algorithm implements AIECs "4.1.2 Convert Ranking into Points"
	 * rule: It assigns average ranks to successive equal results
	 * (comparator==0). For a List of Rides this converts ascending differences
	 * to Rider ranking-points. For a List of Teams this converts descending
	 * results to Team.teamDrPoints. The Comparator must be able to handle the
	 * type of objects in resultList. disciplineIndex is needed to assign points
	 * to a rider.
	 */

	private static void resultToPoints(List<? extends ResultToPoints> resultList, Comparator<ResultToPoints> comp, int disciplineIndex, int qualifyCount) {
		if (resultList.size() == 0)
			return;

		List<? extends ResultToPoints> rList = new ArrayList<>(resultList);
		boolean isGermanCHU = Competition.isCHU_GER() && disciplineIndex == teamDR;

		if (isGermanCHU) {
			Round round1 = Competition.disciplines[Discipline.DR].getRound(0);
			GermanyTeamDressageMarkSorter differences = new GermanyTeamDressageMarkSorter();
			rList = differences.calculateDiffs(round1);
			rList.sort(new TeamDressageMarkComparatorGermany());
		} else {
			rList.sort(comp);
		}

		List<? extends ResultToPoints> qualified = rList.subList(0, qualifyCount);
		assignMinMaxConstPoints(qualified, disciplineIndex);

		int cnt = qualifyCount;
		LinkedList<ResultToPoints> equalPoints = new LinkedList<>();
		ResultToPoints last = rList.get(0);

		for (; cnt < resultList.size(); cnt++) {
			ResultToPoints curr = rList.get(cnt);

			int ord;
			if (isGermanCHU) {
				ord = new TeamDressageMarkComparator().compareDiff(last, curr);
			} else {
				ord = comp.compare(last, curr);
			}

			if (ord != 0) {
				last = curr;
				assignAveragePoints(equalPoints, cnt, disciplineIndex);
			}
			if (curr.resultPreventsRanking()) {
				curr.setNoRanking(disciplineIndex);
				continue;
			}

			equalPoints.addLast(curr);
		}

		assignAveragePoints(equalPoints, cnt, disciplineIndex);
	}


	private static void assignAveragePoints(LinkedList<? extends ResultToPoints> equalPoints, int cnt, int disciplineIndex) {

		// precondition: cnt = last rank of the one(s) to be assigned
		double avgPoints;
		if (Competition.isCHU_GER() && cnt == 1) {
			// german DAR rules oddity: winner gets 0.0 instead of 1.0 points
			avgPoints = 0.0;
		} else if (Competition.isCHU_GER() && cnt == 2 && equalPoints.size() == 2 && disciplineIndex != teamDR) {
			// assign round max points to all non-finished riders
			avgPoints = 2.0;
		} else if (cnt == equalPoints.size() & Competition.isCHU_GER()){
			avgPoints = ((0.5 * cnt * (cnt + 1)) -1) / equalPoints.size(); // if multiple Teams ranked first in the german teamDrRanking
		} else {
			avgPoints = cnt - 0.5 * (equalPoints.size() - 1);
		}
		// System.out.print(" " + avgPoints);
		while (!equalPoints.isEmpty()) {
			ResultToPoints r2p = equalPoints.removeFirst();
			// assign points to Rider/pointedObject for this rounds discipline
			r2p.setPoints(avgPoints, avgPoints, disciplineIndex);
		}
	}

	// -------------------------------------------------------------------------------------
	// rules for adding up points for combined and team
	// currently called from Rider.setPoints() :-(

	// all Competition types
	public static void assignIndividualCombinedPoints(Rider r) {
		r.pointsMin[Discipline.COMB] = r.pointsMin[Discipline.DR] + r.pointsMin[Discipline.SJ];
		r.points[Discipline.COMB] = r.points[Discipline.DR] + r.points[Discipline.SJ];
	}

	// defined for SRNC / CHIU only
	public static void assignTeamDressagePoints(Team t) {
		t.pointsMin[Discipline.DR] = t.riderPointsMinSum(Discipline.DR) + 3 * t.teamDrPointsMin;
		t.points[Discipline.DR] = t.riderPointsSum(Discipline.DR) + 3 * t.teamDrPoints;
	}

	// defined for SRNC / CHIU only
	public static void assignTeamShowjumpingPoints(Team t) {
		t.pointsMin[Discipline.SJ] = t.riderPointsMinSum(Discipline.SJ);
		t.points[Discipline.SJ] = t.riderPointsSum(Discipline.SJ);
	}

	// all Competition types
	private static String teamCombinedFormula;

	public static void assignTeamCombinedPoints(Team t) {
		double DrPts = t.riderPointsSum(Discipline.DR);
		double SjPts = t.riderPointsSum(Discipline.SJ);
		double DrPtsMin = t.riderPointsMinSum(Discipline.DR);
		double SjPtsMin = t.riderPointsMinSum(Discipline.SJ);
		double DrTPts = t.teamDrPoints;
		double DrTPtsMin = t.teamDrPointsMin;
		double TPenPts = t.penaltyPoints;

		if (Competition.isWUEC()) {
			t.points[Discipline.COMB] = Fmt.decimalDigits((DrPts + 3 * DrTPts) / 4.0 + SjPts / 3.0, 3);
			t.pointsMin[Discipline.COMB] = Fmt.decimalDigits((DrPtsMin + 3 * DrTPtsMin) / 4.0 + SjPtsMin / 3.0, 3);
			teamCombinedFormula = "(DrPts+3*DrTPts)/4 + SjPts/3";
		} else if (Competition.isCHU_GER()) {
			double drTPtsFactor;
			if (Competition.QualiDHM2019) {
				drTPtsFactor = 2.0;
			} else {
				drTPtsFactor = 3.0; //according to the Obleuteversammlung Warendorf 2019
			}
			if (Penalties.penalties) {
				t.points[Discipline.COMB] = Fmt.decimalDigits(DrPts + drTPtsFactor * DrTPts + SjPts + TPenPts, 3);
				t.pointsMin[Discipline.COMB] = Fmt.decimalDigits(DrPtsMin + drTPtsFactor * DrTPtsMin + SjPtsMin + TPenPts, 3);
				teamCombinedFormula = "DrPts+" + drTPtsFactor + "*DrTPts + SjPts + " + Penalties.displayName;
			} else {
				t.points[Discipline.COMB] = Fmt.decimalDigits(DrPts + drTPtsFactor * DrTPts + SjPts, 3);
				t.pointsMin[Discipline.COMB] = Fmt.decimalDigits(DrPtsMin + drTPtsFactor * DrTPtsMin + SjPtsMin, 3);
				teamCombinedFormula = "DrPts+" + drTPtsFactor + "*DrTPts + SjPts";
			}
		} else {
			// SRNC and CHIU
			t.points[Discipline.COMB] = DrPts + SjPts;
			t.pointsMin[Discipline.COMB] = DrPtsMin + SjPtsMin;
			teamCombinedFormula = "DrPts + SjPts";
		}
	}

	public static String getTeamCombinedFormula() {
		return teamCombinedFormula;
	}

	// -------------------------------------------------------------------------------------
	/**
	 * Convert ranking points of *any* discipline individual *or* team into
	 * ranks. The algorithm works on a Collection of RankedObjects (Riders,
	 * Teams) and relies on the various comparators in this package.
	 */

	private static List<? extends RankedObject> pointsToRank(List<? extends RankedObject> rankableObjects, Comparator<RankedObject> comp, int disciplineIndex) {

		// sort in temporary list
		List<? extends RankedObject> ranking = new ArrayList<>(rankableObjects);
		ranking.sort(comp);

		// run through sorted list and assign ranks
		RankedObject last =  ranking.get(0);
		last.rank[disciplineIndex] = 1;
		int rank = 1;
		for (int i = 2; i <= ranking.size(); i++) {
			RankedObject current = ranking.get(i - 1);
			if (comp.compare(last, current) != 0)
				current.rank[disciplineIndex] = rank = i;  // same as rank = i; current.rank = rank
			else if (comp.compare(last,current) == 0 && rank == 1 && current instanceof Rider) {
				//LPO oddity, if all final riders retire all get ranked at the last place of a round
				last.rank[disciplineIndex] = 2;
				current.rank[disciplineIndex] = 2;
			} else {
				current.rank[disciplineIndex] = rank;
			}
			last = current;
		}

		enableDefiniteRanks(ranking, disciplineIndex);

		return ranking;
	}

	private static void enableDefiniteRanks(List<? extends RankedObject> ranking, int di) {

		RankedObject prev, curr, next;
		curr = ranking.get(0);
		next = ranking.get(1);
		if (Competition.isCompleted()) {
			curr.rankDefinite[di] = curr.points[di] <= next.pointsMin[di]; // TODO: 10.10.2021 Write Test to validate!!!
		} else {
			curr.rankDefinite[di] = curr.points[di] < next.pointsMin[di]; // does this make sense??
		}
		logger.info("" + 0 + "\t" + curr.pointsMin[di] + " " + curr.points[di] + "\t" + curr.rankToString(di));
		int i = 2;
		for (; i < ranking.size(); i++) { // i = next
			prev = curr;
			curr = next;
			next = ranking.get(i);

			// compare with previous and next
			curr.rankDefinite[di] = prev.points[di] <= curr.pointsMin[di] && curr.points[di] <= next.pointsMin[di];

			// a small curr.pointsMin might affect multiple ranks *before* prev
			pointsMinLow_InvalidatesLowerRanks(ranking, di, i - 1);

			logger.info(
					"" + (i - 1) + "\t" + curr.pointsMin[di] + " " + curr.points[di] + "\t" + curr.rankToString(di));
		}
		prev = curr;
		curr = next;
		curr.rankDefinite[di] = prev.points[di] <= curr.pointsMin[di];
		logger.info(
				"" + (i - 1) + "\t" + curr.pointsMin[di] + " " + curr.points[di] + "\t" + curr.rankToString(di));
	}

	private static void pointsMinLow_InvalidatesLowerRanks(List<? extends RankedObject> ranking, int di, int currIndex) {

		RankedObject curr = ranking.get(currIndex);
		int j = currIndex - 1;
		while (j > 0) {
			RankedObject nprev = ranking.get(j);
			if (curr.pointsMin[di] <= nprev.points[di] && curr.points[di] != nprev.points[di]) {
				nprev.rankDefinite[di] = false;
				logger.info("" + (currIndex) + "\t\tj=" + j + " " + curr.pointsMin[di] + " " + nprev.points[di]
						+ " " + nprev.rankToString(di));
			} else {
				break;
			}
			j--;
		}
	}

	/*
        i    min  max ptsDef rankDef
        00   6.0  6.0   y      1
        01  10.0 10.0   y      2
        02  11.0 11.0   y      3
        03  13.0 15.0   n      4
        04  18.0 18.0   y      5
        05  22.0 22.0*  y      -    j=5
        06  22.5 22.5   y      -     ^
        07  23.0 23.0   y      -     |
        08  23.5 23.5   y      -     |
        09  23.5 23.5   y      -    j=9
        10 *22.0 24.0   n      -    curr
        11  25.0 25.0   y      12
        12  26.0 26.0   y      13
        13  28.0 28.0   y      14
        14  28.5 28.5   y      15
    */

}

/*
 * 
 * Shorter, but not so readable approach. Did not really get it to work for all
 * exceptions
 * 
 * public static void checkForDefiniteRanks2(List ranking, int di) {
 * 
 * for(int i=0; i<ranking.size(); i++) { RankedObject curr =
 * (RankedObject)ranking.get(i); curr.rankDefinite[di] = true; // check
 * (possibly multiple) previous maximum vs. current minimum int j = i-1; while(
 * j>0 ) { RankedObject prev = (RankedObject)ranking.get(j);
 * if(prev.points[di]>=curr.pointsMin[di] && curr.points[di]!=prev.points[di]) {
 * curr.rankDefinite[di] = false; prev.rankDefinite[di] = false;
 * System.out.println(""+i+"\t j="+j+" "+prev.pointsMin[di]+" "+prev.points[di]
 * +" "+prev.rankToString(di)); } else { curr.rankDefinite[di] = true;
 * prev.rankDefinite[di] = true; break; } j--; } // check current maximum vs.
 * next minimum if(i+1<ranking.size()) { RankedObject next =
 * (RankedObject)ranking.get(i+1); curr.rankDefinite[di] = curr.rankDefinite[di]
 * && (curr.points[di] <= next.pointsMin[di]); }
 * System.out.println(""+i+" "+curr.pointsMin[di]+" "+curr.points[di]+" "+curr.
 * rankToString(di)); } }
 */
