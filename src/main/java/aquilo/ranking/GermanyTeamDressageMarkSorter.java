/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.ranking;

import java.util.*;

import aquilo.comparator.RideHorseNoComparator;
import aquilo.entity.Ride;
import aquilo.entity.Round;
import aquilo.entity.Team;

class GermanyTeamDressageMarkSorter {

	ArrayList<Team> calculateDiffs(Round drRound1) {

		LinkedList<Ride> rides = new LinkedList<>(drRound1.getRides()); // all rides in new linked list

		rides.sort(new RideHorseNoComparator());

		int horseNo;
		for (Iterator<Ride> iter = rides.listIterator(); iter.hasNext();) { // We only need one Horse per Group
			horseNo = Integer.parseInt(iter.next().getHorse().getHorseNo());
			if (!(horseNo % 3 == 0)) {
				iter.remove();
			}
		}

		ArrayList<Team> teamList = new ArrayList<>();

		for (Ride value : rides) {
			teamList.add(value.getRider().getTeam());
		}

		for (int i = 0; i < teamList.size(); i += 3) {
			teamList.subList(i,i+3).sort(new TeamDressageMarkComparator());
			assignDiffs(teamList.subList(i,i+3));
		}

		return teamList;

	}

	private void assignDiffs (List<Team> subTeamList){
		subTeamList.get(0).dressageResult.setDiffTo(subTeamList.get(1).dressageResult);
		subTeamList.get(1).dressageResult.setDiffTo(subTeamList.get(0).dressageResult);
		subTeamList.get(2).dressageResult.setDiffTo(subTeamList.get(0).dressageResult);
	}

}
