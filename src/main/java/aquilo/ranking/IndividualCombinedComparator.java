/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.ranking;

import java.util.*;

import aquilo.entity.Discipline;
import aquilo.entity.RankedObject;

/**
    
*/

public final class IndividualCombinedComparator implements Comparator<RankedObject> {

    public int compare(RankedObject o1, RankedObject o2) {
        if(o1.points[Discipline.COMB] != o2.points[Discipline.COMB])
            return (o1.points[Discipline.COMB] < o2.points[Discipline.COMB]) ? -1 : 1;
        //System.out.println("COMB equal points: "+r1+"  rank: "+Fmt.fmt(r1.rank[Discipline.COMB],2));
        if(o1.points[Discipline.DR] != o2.points[Discipline.DR])
            return (o1.points[Discipline.DR] < o2.points[Discipline.DR]) ? -1 : 1;
        return 0;  // => equal points
    }
}
