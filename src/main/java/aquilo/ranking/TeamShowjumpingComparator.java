/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.ranking;

import java.util.*;

import aquilo.entity.Discipline;
import aquilo.entity.RankedObject;

/**
    
*/

public final class TeamShowjumpingComparator implements Comparator<RankedObject> {

    public int compare(RankedObject r1, RankedObject r2) {
        if(r1.points[Discipline.SJ] != r2.points[Discipline.SJ])
            return (r1.points[Discipline.SJ] < r2.points[Discipline.SJ]) ? -1 : 1;
        //System.out.println(" SJ  equal points: "+r1+"  rank: "+Fmt.fmt(r1.rank[Discipline.SJ],2));
        return 0;  // => equal ranks
    }
}
