/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo;

import aquilo.entity.Competition;
import aquilo.gui.MainWin;
import aquilo.gui.MessageWindow;
import aquilo.util.Meta;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Aquilo {

    private static final Logger logger = LoggerFactory.getLogger(Aquilo.class);

    public static void main(String[] args) {
        File autoOpen = null;
        boolean disableTimeLimit = false;

        OptionParser parser = new OptionParser(){{
            acceptsAll(asList("h", "help"), "show help");
            acceptsAll(asList("v", "version"), "show the " + Meta.name + " version");
            acceptsAll(asList("d","disableTimeLimit"),"disables time deprecation check");
            acceptsAll(asList("f", "file"), "loads file")
                    .withRequiredArg()
                    .describedAs("location of .chu file");
        }};

        OptionSet options = null;

        try {
            options = parser.parse(args);
        } catch (Exception ex) {
            logger.warn(ex.getMessage(), ex);
        }

        if ((options == null) || (options.has("h"))) {
            try {
                parser.printHelpOn(System.out);
            } catch (IOException ex) {
                logger.warn(ex.getMessage(), ex);
            }
            return;
        }

        if (options.has("v")) {
            System.out.println(Meta.nameVersionIssueDate() + "\n" + Meta.getCopyright());
            System.exit(0);
        }

        if (options.has("d")) {
            disableTimeLimit = true;
            System.out.println("Ignoring time limit");
        }

        if (options.has("f")) {
            try {
                autoOpen = new File((String) options.valueOf("f"));
                System.out.println("loading file " + autoOpen.getPath());
            } catch (Exception ex) {
                logger.warn(ex.getMessage(), ex);
            }
        }

        //UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );

        System.out.println("\n" + Meta.nameVersionIssueDate() + "\n" + Meta.getCopyright());
        logger.info(Meta.nameVersionIssueDate());
        logger.info(Meta.getCopyright());
        logger.debug("äöüß"); // Encoding-Test
        if (Competition.QualiDHM2019) {
            logger.info("Special Mode for the DHM-Qualification and DHM 2019");
        } else if (Competition.isDHM) {
            logger.info("DHM-Mode");
        }

        if (!disableTimeLimit && Meta.hasExpired()) {
            logger.warn("Version expired!");
            MainWin.showAboutDialog();
            MessageWindow.showVersionExpired();
            System.exit(0);
        }

        // Disables MACOS as it is error-prone
        String OS = System.getProperty("os.name");
        logger.info("OS is: " + OS);
        if (OS.toLowerCase().contains("mac")) {
            MessageWindow.showOSErrorMsg();
            logger.warn("OS is MACOS, aborting!");
            System.exit(0);
        }

        logger.debug("Java Class Path: " + System.getProperty("java.class.path"));
        logger.debug("Java Home: " + System.getProperty("java.home"));

//        XHtmlViewer.loadFileAndInitDialog(helpFile);

        new MainWin(autoOpen);
    }

    private static List<String> asList(String... params) {
        return Arrays.asList(params);
    }

}
