/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.gui;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import aquilo.entity.Competition;
import aquilo.util.CityComp;

public final class CompetitionPropertiesDialog {

    private static boolean okSelected = false;

    public static boolean show(JFrame parent, boolean newCompetition) {

        // arrange labels and textfields
        JPanel p = new JPanel();
        GridBagLayout gridbag = new GridBagLayout();
        p.setLayout(gridbag);
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.WEST;
        Insets normalInsets = new Insets(20,5,0,5);
        c.insets = normalInsets;
        JLabel lbl;
        c.gridy = 0;
            c.gridx = 0;
                p.add(new JLabel("Competition Name                   "),c);
            c.gridx = 1;
                String cn = (newCompetition) ? "" : Competition.name;
                final JTextField tfCompName = new JTextField(cn,26);
                p.add(tfCompName,c);
        c.gridy++;
            c.gridx = 0;
                p.add(new JLabel("Competition Type / Rules"),c);
            c.gridx = 1;
                String[] selRules = { 
                    " SRNC (3 team rankings: DR, SJ, COMB)",
                    " CHIU (same as SRNC)",
                    " WUEC (team combined only, WUEC formula)",
                    " CHU_GER (DAR Regeln, Ausgabe auf Deutsch)" 
                };
                final JComboBox<String> cbRules = new JComboBox<>(selRules);
                if     (Competition.isSRNC()) cbRules.setSelectedIndex(0);
                else if(Competition.isCHIU()) cbRules.setSelectedIndex(1);
                else if(Competition.isWUEC()) cbRules.setSelectedIndex(2);
                else if(Competition.isCHU_GER()) cbRules.setSelectedIndex(3);
                p.add(cbRules,c);
        c.gridy++;
            c.gridx = 0;
                p.add(new JLabel("Number of Teams"),c);
            c.gridx = 1;
                String not = ""+MainWin.noOfTeamsInit;
                final JTextField tfNoOfTeams = new JTextField(not,4);
                tfNoOfTeams.setEnabled(newCompetition);
                p.add(tfNoOfTeams,c);
        c.gridy++;
            c.gridx = 0;
            c.gridwidth = 2;
            c.fill = GridBagConstraints.BOTH;
            c.insets = new Insets(0,5,0,5);
                p.add(lbl = new JLabel(
                    "<html>The number of teams can not be changed later. "+
                    "A number not divisible by 3 is allowed."
                ),c);
                lbl.setPreferredSize(new Dimension(450,50));
        c.gridy++;
            c.gridx = 0;
            c.fill = GridBagConstraints.NONE;
            c.insets = normalInsets;
                p.add(new JLabel("Start \"2 riders per horse\" at "),c);
            c.gridx = 1;
                String[] selRiders = { "  2 riders","  4 riders","  6 riders","  8 riders"," 10 riders",
                             " 12 riders"," 14 riders"," 16 riders"," 18 riders"," 20 riders" };
                final JComboBox<String> cbMaxRidersFTPH = new JComboBox<>(selRiders);
                cbMaxRidersFTPH.setSelectedIndex(1);
                cbMaxRidersFTPH.setEnabled(newCompetition);
                p.add(cbMaxRidersFTPH,c);
        c.gridy++;
            c.gridx = 0;
            c.gridwidth = 2;
            c.fill = GridBagConstraints.BOTH;
            c.insets = new Insets(0,5,25,5);
                p.add(lbl = new JLabel(
                    "<html>Lower rounds usually have 3 riders per "+
                    "horse and higher rounds only 2 riders per horse. "+
                    "If there is only a certain number of riders left the program will create "+
                    "rounds with only 2 riders per horse automatically. "+
                    "This may influence the number of horses or rounds you need. "
                ),c);
                lbl.setPreferredSize(new Dimension(450,80));
        
        // put labels and textfields into a dialog with customized OK button
        JOptionPane op = new JOptionPane(p);
        JButton okBtn = new JButton(" OK ");
        okBtn.setEnabled(false);
        Object [] options = { okBtn, "Cancel" };
        op.setOptions(options);
        op.setInitialValue(okBtn); // sets enter as default
        
        String title = (newCompetition) ? "New Competition" : "Competition Properties";
        final JDialog dialog = op.createDialog(parent,title);
        okSelected = false;

        tfCompName.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                changed();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                changed();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                changed();
            }

            private void changed() {
                okBtn.setEnabled(!tfCompName.getText().isEmpty());
            }
        });

        // if closed with OK and sensible input:
        // create empty competition and show it in GUI
        okBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                if (cbRules.getSelectedIndex() == 3){
                    String compName = tfCompName.getText();
                    Competition.name = evaluateName(compName);
                }else {
                    Competition.name = tfCompName.getText();
                }
                MainWin.fileHandling.dataChanged();
                MainWin.self.setTitle(Competition.name);
                
                // map competition type list box selection to Competition.type
                switch (cbRules.getSelectedIndex()){
                    case 0: Competition.setTypeSRNC();break;
                    case 1: Competition.setTypeCHIU();break;
                    case 2: Competition.setTypeWUEC();break;
                    case 3: Competition.setTypeCHU_GER();break;
                    default: okSelected=false;return;
                }

                //disables non CHU Modes
                if(!(Competition.isCHU_GER() || MainWin.allowInter)){
                    MessageWindow.showNoInterDialog();
                    okSelected = false;
                    return;
                }
                
                // number of teams (may be invalid)
                try {
                    MainWin.noOfTeamsInit = Integer.parseInt(tfNoOfTeams.getText().trim());
                    String numStr = ((String)cbMaxRidersFTPH.getSelectedItem()).substring(0,4).trim();
                    MainWin.maxRidersForTwoPerHorse = Integer.parseInt(numStr);
                    //Competition.ridersPerTeam = Integer.parseInt(tfRpT.getText().trim());
                }
                catch(NumberFormatException ex) { return; }  // keep dialog open
                dialog.setVisible(false);
                okSelected = true;
            }
        });
        // [if closed with cancel do nothing]

        dialog.setVisible(true);
        return okSelected;
    }

    private static String evaluateName(String name){

        CityComp city = new CityComp();
        ArrayList<String> cities = city.getCities();

        if (name.contains("DHM") || name.contains("Quali")) {
            return name;
        }

        for (String s : cities) {
            if (name.contains(s)) {
                return "CHU " + s;
            }
        }
        return name;
    }
}
