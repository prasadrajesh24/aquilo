/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.gui;

import aquilo.entity.Discipline;
import aquilo.entity.Round;
import aquilo.util.*;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;

//============================================================================

public final class DisciplineDefPanel extends JPanel {

    static final Logger logger = LoggerFactory.getLogger(DisciplineDefPanel.class);

    private Discipline[] disciplines;
    private RoundsTable rt;

    public DisciplineDefPanel(Discipline[] disciplines) {
        super();
        this.disciplines = disciplines;
        setLayout(new BorderLayout());
        int noOfDisciplines = Discipline.LAST - Discipline.FIRST + 1;
        int initDiscipline = Discipline.FIRST;

        // left side: radiobuttons to select discipline
        JPanel p = new JPanel();
        p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
        p.setBorder(BorderFactory.createEmptyBorder(8, 15, 2, 15));
        p.add(new JLabel("Define Rounds:"));
        ButtonGroup bg = new ButtonGroup();
        for (int i = Discipline.FIRST; i <= Discipline.LAST; i++) {
            JRadioButton rb = new JRadioButton(disciplines[i].name);
            rb.setActionCommand(disciplines[i].getId());
            if (i == initDiscipline) rb.setSelected(true);
            bg.add(rb);
            p.add(rb);
            rb.addActionListener(new switchDisciplineAction());
        }
        add(p, BorderLayout.WEST);

        // right side: table with round definitions
        rt = new RoundsTable(disciplines[initDiscipline]);
        add(rt, BorderLayout.CENTER);
    }

    private final class switchDisciplineAction implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            Discipline d;
            try {
                d = Discipline.getForId(e.getActionCommand());
            } catch (Exception ex) {
                return;
            }
            rt.setDiscipline(d);
        }
    }

    public void updateTable() {
        rt.table.repaint();
    }

}

//============================================================================

class RoundsTable extends JScrollPane implements SwingConstants
{
    Discipline discipl;
    JTable table;

    // Table settings
    static String[] titles = {
        "ID", "Name", "Riders", "Horses", "HoPerGrp", "Result", "FirstHoNo",  null
    };
    static boolean[] editable = {
        false, true,   true,    true,     true,          true,     true,        false
    };
    static int[] columnPreferredWidth = {
        25,    150,     25,      25,        40,           43,       40,            50
    };
    static int[] columnAlignement = {
        CENTER, LEFT, CENTER,   CENTER,    LEFT,         CENTER,   CENTER,        LEFT
    };
    static String[] columnTipText = {
        "Round ID",
        "Round description, always editable",
        "Number of ride(r)s in this round",
        "Number of horses in this round",
        "<html>Multiple horse groups per round:<br>Number of horses per group separated by commas.<br>Example: 15 horses in first round = 6,6,3",
        "Result type: Style, FaultsStyle or FaultsTime",
        "Number of first horse in this round",
        "Open round window with starting order"
    };

    final static int openButtonColumn = titles.length-1;

    public Insets getInsets() { return new Insets(0,0,0,0); }

    public RoundsTable(Discipline discipl) {
        this.discipl = discipl;
        table = new JTable(new Model());
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        // set column properties
	    TableColumnModel tcm = table.getColumnModel();
        for(int c=0; c<table.getModel().getColumnCount(); c++ ) {
            TableColumn column = tcm.getColumn(c);
            //if(columnPreferredWidth[c]==-1) column.sizeWidthToFit();
            column.setPreferredWidth(columnPreferredWidth[c]);

	        if(c!=openButtonColumn) {
	            DefaultTableCellRenderer columnRenderer = new DefaultTableCellRenderer();
                if(c==0) columnRenderer.setBackground(Color.lightGray);
                columnRenderer.setHorizontalAlignment(columnAlignement[c]);
                columnRenderer.setToolTipText(columnTipText[c]);
                column.setCellRenderer(columnRenderer);
            }
	        else {
	            column.setCellRenderer(new OpenButtonRenderer());
            }
        }
        TableColumn resultTypeCol = table.getColumnModel().getColumn(5);
        JComboBox<String> comboBox = new JComboBox<>();
        comboBox.addItem(Round.STYLE);
        comboBox.addItem(Round.FAULTSSTYLE);
        comboBox.addItem(Round.FAULTSTIME);
        resultTypeCol.setCellEditor(new DefaultCellEditor(comboBox));

        table.addMouseListener(new OpenClickedAction());
        setViewportView(table);
        
        MainWin.jTableRegister.add(table);      // call editingStopped() before save
    }

    public void setDiscipline(Discipline d) {
        CellEditor editor = table.getCellEditor();
        if(editor!=null) editor.stopCellEditing();
        // 
        this.discipl = d;
        ((AbstractTableModel)table.getModel()).fireTableDataChanged();
    }

    public Dimension getPreferredSize() {
        int h = (table.getRowCount()+1)*(table.getRowHeight())+1;
        return new Dimension(600, h);
    }

    //--------------------------------------------------------------------------
    // Open button in last column opens RoundWindow

    class OpenButtonRenderer extends JButton implements TableCellRenderer {
        public Component getTableCellRendererComponent(JTable table, Object label,
            boolean isSelected, boolean hasFocus, int row, int column) {
            Round rnd = discipl.getRound(row);
            setText( (rnd.noOfRides() == 0) ? "Create" : "Open" );
            return this;
        }
    }

    class OpenClickedAction extends MouseAdapter {
        private static final Logger logger = LoggerFactory.getLogger(OpenClickedAction.class);
        public void mouseClicked(MouseEvent e) {
            if( table.columnAtPoint(e.getPoint()) != openButtonColumn ) return;
            int row = table.rowAtPoint(e.getPoint());
            Round rnd = discipl.getRound(row);

            // if open used 1st time there are no rides yet
            boolean createRound = rnd.noOfRides() == 0;
            if(createRound) {
                if(twoHorsesInFinal(rnd)) return;
            	try { 
            	    rnd.createEmptyStartingOrder();
                } 
                catch(Exception ex) {
                    logger.warn(ex.getMessage(), ex);
                    JOptionPane.showMessageDialog(MainWin.self,ex.getMessage(),
                                "Invalid Value", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }
            // check if round already open => moveToFront()
            JInternalFrame[] openRnds = MainWin.rndDesktop.getAllFrames();
            for(int i=0; i<openRnds.length; i++) {
                if(openRnds[i].getTitle().indexOf(rnd.name) > -1 ) {
                    openRnds[i].moveToFront();
                    return;
                }
            }
            logger.info("Opening round: "+rnd.getId());
	        RoundWindow rndWin = new RoundWindow(rnd);
	        MainWin.rndDesktop.add(rndWin);
	        rndWin.setLocation( 2+row*10, 2+row*60 );
	        rndWin.moveToFront();
            rndWin.setVisible(true);
            
            if(createRound) showCheckStartingOrderMsg(rnd);
        }
    }

    private static void showCheckStartingOrderMsg(Round rnd) {
        if( rnd.getLevel() > 2 ) return;
        //XHtmlViewer.showDiv("RunningRounds");
        String msg =
        "Before entering any data into the round check if the\n\n"+
        "    RESULT TYPE is correct for this round (Result)\n\n"+
        "    HORSE NUMBERS are as expected (FirstHoNo)\n"+
        "    Examples for 2nd round: 1,2,3,4 or 13,14,15,16 or 20,21,22,23\n\n"+
        "    GROUPS OF HORSES (ABTEILUNGEN) are distributed correctly (HoPerGrp)\n"+
        "    Example for 1st round: 15 horses = 6+9 = 9+6 = 6+6+3 = ...\n\n"+
        "If necessary click Reset Round and adjust the appropriate values in \n"+
        "the Define Rounds table before re-creating the round.\n";
        JOptionPane.showMessageDialog(MainWin.self,msg,"Check Starting Order!",
            JOptionPane.INFORMATION_MESSAGE);
    }

    private static boolean twoHorsesInFinal(Round rnd) {
        boolean isLastRound = rnd.getDiscipline().getNextRound(rnd) == null;
        if( isLastRound && rnd.noOfHorses > 1 ) {
            String msg =
            "You have specified two horses for the final.\n"+
            "There are many ways of combining the two results per rider and\n"+
            "the program can not know them all. Please specify only one horse\n"+
            "and add the results manually.\n\n"+
            "Remember for dressage: Freestyle and compulsory should be\n"+
            "converted to percent before they are added.\n";
            JOptionPane.showMessageDialog(MainWin.self,msg,"Combine results for 2 horses in final",
                JOptionPane.INFORMATION_MESSAGE);
            return true;
        }
        return false;
    }
    
    //--------------------------------------------------------------------------
    // TableModel mapping the Round objects in a Discipline to rows and columns

    public class Model extends AbstractTableModel {

        private static final Logger logger = LoggerFactory.getLogger(Model.class);

        public int getColumnCount() { return titles.length; }
        public int getRowCount() { return discipl.getNoOfRounds(); }

        public Object getValueAt(int row, int col) {
            Round r = discipl.getRound(row);
            switch( col ) {
                case 0: return r.getId();
                case 1: return (r.isIgnoredForRanking()) ? "[temporarily ignored for ranking]" : r.name;
                case 2: return r.noOfRides;
                case 3: return r.noOfHorses;
                case 4: return Fmt.intArrayToString(r.horsesPerGroup);
                case 5: return r.resultType;
                case 6: return r.firstHorseId;
                //case 7: Open-Button;
            }
            return null;
        }

        public String getColumnName(int col) { return titles[col]; }

        public boolean isCellEditable(int row, int col) {
            Round rnd = discipl.getRound(row);
            // allow editing only if no rides created yet
            if(rnd.noOfRides() == 0) return editable[col];
            // round name always editable
            if(col == 1) return true;
            // JOptionPane.showMessageDialog never closes if called here :-(
//            String msg =
//                "Once a round has been opened, you can not change its initialization parameters.\n\n"+
//                "If you need to change the parameters, the data of this round has to be deleted first,\n"+
//                "by opening it and clicking on \"Reset Round\".";
//            JOptionPane.showMessageDialog(MainWin.self,msg,"Round already open!",
//                    JOptionPane.INFORMATION_MESSAGE);
            return false;
        }

        public void setValueAt(Object newVal,int row, int col) {
            Round r = discipl.getRound(row);
            try {
                switch (col) {
                    case 1:
                        r.name = (String) newVal;
                        break;
                    case 2:
                        r.noOfRides = Integer.parseInt((String) newVal);
                        // todo: reset noOfHorses and horsesPerGroup when noOfRides is changed
                        break;
                    case 3:
                        int nh = Integer.parseInt((String) newVal);
                        Round.checkRidersDivisibleByHorses(r.noOfRides, nh);
                        r.noOfHorses = nh;
                        r.horsesPerGroup = new int[1];
                        r.horsesPerGroup[0] = r.noOfHorses;
                        break;
                    case 4:
                        int[] intVals = Fmt.parseIntList((String) newVal);
                        Round.checkSum(intVals, r.noOfHorses);
                        r.horsesPerGroup = intVals;
                        break;
                    case 5:
                        r.resultType = (String) newVal;
                        break;
                    case 6:
                        r.firstHorseId = (String) newVal;
                        break;
                }
            } catch (NumberFormatException ex) {
                logger.warn("Number Format Exception ",ex);
            } catch (Exception ex) {
                logger.warn("General Exception ",ex);
                JOptionPane.showMessageDialog(MainWin.self, ex.getMessage(),
                        "Invalid Value", JOptionPane.ERROR_MESSAGE);
            }
            logger.trace("{}",r);
            fireTableDataChanged();
            MainWin.fileHandling.dataChanged();
        }
    }

}


