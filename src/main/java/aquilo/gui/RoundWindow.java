/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.gui;

import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

import aquilo.entity.Competition;
import aquilo.entity.Rider;
import aquilo.entity.Round;
import aquilo.util.*;
import aquilo.htmlout.DrawingLots;
import aquilo.htmlout.RoundListing;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

//============================================================================

public final class RoundWindow extends JInternalFrame implements ListSelectionListener {

    private static final Logger logger = LoggerFactory.getLogger(RoundWindow.class);

    RoundWindow self;
    RidesTable rt;
    JScrollPane sp;
    JButton selPrintBut, delBut;

    public RoundWindow(Round rnd) {
        super(rnd.name, true, true, true, true);
        self = this;
        getContentPane().setLayout(new BorderLayout());

        rt = new RidesTable(rnd);
        rt.getSelectionModel().addListSelectionListener(this);
        sp = new JScrollPane(rt);
        getContentPane().add(sp, BorderLayout.CENTER);
        JPanel buttonBar = new JPanel();
        getContentPane().add(buttonBar, BorderLayout.SOUTH);

        JButton b = new JButton("Print Lots");
        b.addActionListener(new PrintLotsAction());
        buttonBar.add(b);
        selPrintBut = new JButton("Print Selected");
        selPrintBut.addActionListener(new RidesToHtmlAction());
        selPrintBut.setEnabled(false);
        buttonBar.add(selPrintBut);
        b = new JButton("Print All");
        b.addActionListener(new RidesToHtmlAction());
        buttonBar.add(b);
        // (requested noOfTeams)%3 != 0 => artificial riders => riders need to be deleted
        if (rnd.noOfRides() > Competition.noOfRiders()) {
            delBut = new JButton("Delete Selected");
            delBut.addActionListener(new DeleteRidesAction());
            delBut.setEnabled(false);
            buttonBar.add(delBut);
        }
        b = new JButton("Reset Riders"); // TODO: 30.10.2022 needs more refinements (e.g. remove already given marks or disable after first mark) 
        b.addActionListener(new DeleteRidersAction());
        buttonBar.add(b);

        b = new JButton("Reset Round...");
        b.addActionListener(new DeleteRoundAction());
        buttonBar.add(b);

        /*
        // workaround: changes of rider names not reflected in round view
        b = new JButton("[Update Data]");
        b.addActionListener( new UpdateAction() );
        buttonBar.add(b);
        */
        pack();
    }

    class PrintLotsAction implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            try {
                File writeLocation = DrawingLots.writeHtmlFile(rt.rnd);
                rt.model.fireTableDataChanged();
                WebBrowser.open(writeLocation);
            } catch (Exception ex) {
                logger.warn(ex.getMessage(), ex);
                JOptionPane.showMessageDialog(MainWin.self, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }

        }
    }

    class RidesToHtmlAction implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            int[] selectedRows;
            if (e.getSource() == selPrintBut)
                selectedRows = rt.getSelectedRows();
            else // print all
                selectedRows = null;

            try {
                File writeLocation = RoundListing.writeHtmlFile(rt.rnd, selectedRows);
                WebBrowser.open(writeLocation);
            } catch (NullPointerException NPEx) {
                logger.warn(NPEx.getMessage(), NPEx);
            } catch (Exception ex) {
                logger.warn(ex.getMessage(), ex);
                JOptionPane.showMessageDialog(MainWin.self, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    // (requested noOfTeams)%3 != 0 => artificial riders => riders need to be deleted
    class DeleteRidesAction implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            int[] selectedRows = rt.getSelectedRows();
            rt.rnd.deleteRides(selectedRows);
            rt.model.fireTableDataChanged();
            MainWin.discipDefPanel.updateTable();
        }
    }

    class DeleteRidersAction implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            for (int i = 0; i < rt.rnd.noOfRides; i++) {
                rt.rnd.getRide(i).setRider(Rider.notDefined);
            }
            rt.model.fireTableDataChanged();
        }
    }

    class DeleteRoundAction implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String title = "Reset round \"" + rt.rnd.name + "\" ?";
            String msg = "This will DELETE the complete round with all its " +
                    "horsenames, rider assignments and results.\n\n" +
                    "If you find that you have initialized this round with a bad " +
                    "number of riders, horses, etc. in the\n\"Define Rounds\" table click yes to " +
                    "allow changes of this rounds initialization parameters.";
            int option = JOptionPane.showConfirmDialog(MainWin.self, msg, title, JOptionPane.YES_NO_OPTION);
            if (option == JOptionPane.YES_OPTION) {
                rt.rnd.clearRides();
                rt.rnd.clearHorses();       // we could also keep the horses ?!?!
                self.setVisible(false);
                MainWin.rndDesktop.remove(self);
                MainWin.fileHandling.dataChanged();
                // change Open button -> Create button
                MainWin.discipDefPanel.updateTable();
            }
        }
    }

    // workaround: changes of rider names not reflected in round view
    class UpdateAction implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            rt.model.fireTableDataChanged();
        }
    }

    // enable / disable buttons which depend on a selection
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) return;
        ListSelectionModel lsm = rt.getSelectionModel();
        if (lsm.isSelectionEmpty()) {
            selPrintBut.setEnabled(false);
            if (delBut != null) delBut.setEnabled(false);
        } else {
            selPrintBut.setEnabled(true);
            if (delBut != null && rt.rnd.noOfRides() > Competition.noOfRiders())
                delBut.setEnabled(true);
        }
    }


}

