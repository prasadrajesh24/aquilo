/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.gui;

import aquilo.entity.Result;
import aquilo.entity.Ride;
import aquilo.entity.Rider;
import aquilo.entity.Round;
import aquilo.util.Meta;

import javax.swing.*;
import java.util.List;

public class MessageWindow {

    private static final String newFeatureMsg = "\n\n This is a new Feature!\n" +
            "If you think there is an error, ignore this warning \nand immediately notify the " + Meta.name + " maintainer!";

    static void showFileNotSaved() {
        JOptionPane.showMessageDialog(MainWin.self,
                "File is not saved! \n" +
                        "You have to save the file \n" +
                        "to export the competition!",
                "File not saved",
                JOptionPane.ERROR_MESSAGE);
    }

    public static void showCompetitionIsNotCompleted(){
        JOptionPane.showMessageDialog(MainWin.self,
                "The Competition is not completed!\n"
                + "There are missing marks in either DR,SJ or TeamDR",
                "Competition is not completed",
                JOptionPane.ERROR_MESSAGE);
    }

    public static void showNoTeamDrMarks() {
        JOptionPane.showMessageDialog(MainWin.self,
                "Achtung! \n" +
                        "Es sind nicht alle Manschaftsnoten eingetragen. \n" +
                        "Es werden die m\u00F6glichen Min/Max Punkte ausgegeben",
                "Fehlende Mannschaftsnoten Dressur" ,
                JOptionPane.WARNING_MESSAGE);
    }

    public static void showTooManyLuckyLosers(List<Ride> LuckyLosers) {
        String msg;
        msg = "You assigned " + LuckyLosers.size() + " Lucky Losers!!!\n\n";

        for (Ride luckyLoser : LuckyLosers) {
            Ride r = luckyLoser;
            msg += r + "\n";
        }

        msg+="\nModify the result of the eventually non-qualified rider(s) so that\n"+
                "he/she/they will be ranked behind the qualified Lucky Loser, e.g.\n"+
                "the result is only slightly worse than that of the qualified rider.\n";

        JOptionPane.showMessageDialog(MainWin.self,
                msg,
                "Too many Lucky Losers",
                JOptionPane.WARNING_MESSAGE);
    }

    static void showNoInterDialog(){
        JOptionPane.showMessageDialog(MainWin.self,
                "This Aquilo Version is only for CHU's in Germany! \n"+
                        "If you want to create an SRNC, CHIU or WUEC, please refer \n"+
                        "to Version 0.97 available at studentriding.com/downloads",
                "This Version is for CHU's only",
                JOptionPane.ERROR_MESSAGE
        );
    }

    public static void ExportSuccessful(String FileLocation) {
        JOptionPane.showMessageDialog(MainWin.self,
                "Export Successful! \n" +
                        "You can find the ZipFile in: \n" +
                        FileLocation + "\n\n" +
                        "Please send this file via mail to:\n" +
                        "ergebnisrueckmeldung@dar-online.de",
                "Export Successful",
                JOptionPane.WARNING_MESSAGE);
    }

    public static int showNoStilpreis(){
        Object[] options = {"Sicher keine Stilpreise eingeben","Zur\u00fcck zur Eingabe"};
            String msg = "Es wurden keine Stilpreisgewinner eingeben!\n\n" +
                    "Falls keine eingegeben werden sollen, \n" +
                    "sind die Gewinner der Stilpreise mit Name und Anschrift\n" +
                    "in der Ergebnisr\u00fcckmeldung zu nennen!";
            int selected = JOptionPane.showOptionDialog(MainWin.self,
                    msg,
                    "Keine Stilpreise eingegeben",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.WARNING_MESSAGE,null,options,options[0]);
            return selected;
    }

    static int showFalseMaxRidersForTwoPerHorse(int maxRidersForTwoPerHorse){
        Object[] options = {"Standard Einstellung","Eigene Eingabe"};
        String msg = "Auf CHU's wird normalerweise ab dem Halbfinale\n" +
                "im KO-System geritten. Das bedeutet das nur noch 2 Reiter\n" +
                "in der jeweiligen Disziplin auf einem Pferd reiten. (2 Riders per Horse = 4)\n\n" +
                "Die gew\u00e4hlte Einstellung ist,\n" +
                "dass ab " + maxRidersForTwoPerHorse +
                " Reitern im KO-System geritten wird.\n";
        int selected = JOptionPane.showOptionDialog(MainWin.self,
                msg,
                "KO-System Fehler",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE,null,options,options[0]);
        return selected;
    }

    static int setRoundsIgnoreAbove (Integer initRound) {
        final JTextField tf = new JTextField(initRound.toString());
        String msg = "Rounds ignore above: ";
        Object[] objects = {msg,tf};
        JOptionPane.showMessageDialog(MainWin.self,
                objects,
                "Select Rounds Ignore Above: ",
                JOptionPane.INFORMATION_MESSAGE);
        return Integer.parseInt(tf.getText());
    }

    static int showResultTooSmall(Result result) {
        String resultString = result.toString();
        Object[] options = {"eliminated","retired",resultString};
        String msg = "Das eingegebene Ergebnis (" + resultString + ")\n" +
                "ist sehr klein. Sollte der Reiter die Prüfung nicht beendet haben,\n" +
                "ist entsprechend der LPO einzutragen ob er/sie \n" +
                "ausgeschieden ist oder aufgegeben hat. Die Eintragung\n" +
                "der Wertnote 0,0 ist nur zul\u00e4ssig, sollte nach allen Abzügen\n" +
                "keine positive Wertnote verbleiben.\n\n" +
                "Bitte weiteres vorgehen ausw\u00e4hlen: ";
        int selected = JOptionPane.showOptionDialog(MainWin.self,
                msg,
                "Sehr kleine Wertnote",
                JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.WARNING_MESSAGE,null,options,options[2]);
        return selected;
    }

    static int showCityNotFound(int state){
        Object[] options = {"MIX","Cancel"};
        String[] opt = {"city","abbreviation"};
        String msg = "The choosen " + opt[state] + " is not listed.\n" +
                "Please look up if you entered a valid \n" +
                opt[state] +" or set the Team as a MIX-Team.";
        int selected = JOptionPane.showOptionDialog(MainWin.self,
                msg,"Invalid " + opt[state],
                JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE,null,options,options[0]);
        return selected;
    }

    public static int showOverwriteFileDialog(){
        Object[] options = {"Overwrite","Cancel"};
        String msg = "There is already an existing\n" +
                "Penalties File! \n" +
                "Do you want to overwrite the old one?";
        int sel = JOptionPane.showOptionDialog(MainWin.self,msg,
                "Overwrite existing File?",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE,
                null,options,options[1]);
        return sel;
    }

    public static void showOSErrorMsg() {
        JOptionPane.showMessageDialog(null,
                "Your Operating System is not supported! \n" +
                        Meta.name + " only supports Windows and Linux Systems!\n" +
                        "The program will now quit!",
                "OS not supported!",
                JOptionPane.ERROR_MESSAGE);
    }

    public static void showVersionExpired() {
        JOptionPane.showMessageDialog(null,
                "The version you are using expired on " + Meta.expireDateToString() + "\n" +
                        "Please download the latest version at:\n\n" + Meta.website + "\n\n" +
                        "The program will now quit.",
                "Version expired",
                JOptionPane.ERROR_MESSAGE);
    }

    public static int showRiderNotInNextRound(Rider rider, Round round) {
        Object[] options = {"Clear Rider", "Ignore"};
        int sel = JOptionPane.showOptionDialog(MainWin.self,
                "The rider you just entered is not qualified for this round!\n\n" + rider + "\n\n" +
                        "Please enter a rider that qualified for " + round.getId() + "." + newFeatureMsg,
                "Rider not qualified for next Round",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.ERROR_MESSAGE,
                null, options, options[0]);
        return sel;
    }
}
