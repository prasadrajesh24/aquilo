/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.gui;

import aquilo.entity.Competition;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class DebugMenuWindow {

    private static boolean okSelected = false;

    static boolean show(JFrame parent) {

        JPanel p = new JPanel();

        BoxLayout boxLay = new BoxLayout(p,BoxLayout.Y_AXIS);
        p.setLayout(boxLay);


        JLabel lbl = new JLabel("Select Options");

        JCheckBox allowInter = new JCheckBox("Internationale Turniere",MainWin.allowInter);
        JCheckBox QualiDHM2019ranking = new JCheckBox("Wertungssystem 2018/19",Competition.QualiDHM2019);
        JCheckBox DHM = new JCheckBox("DHM Zusatzoptionen", Competition.isDHM);

        p.add(lbl,boxLay);
        p.add(allowInter,boxLay);
        p.add(QualiDHM2019ranking,boxLay);
        p.add(DHM,boxLay);

        String title = ("Debug Options");

        JOptionPane op = new JOptionPane(p);JButton okBtn = new JButton(" OK ");
        Object [] options = { okBtn, "Cancel" };
        op.setOptions(options);
        okSelected = false;

        final JDialog dialog = op.createDialog(parent,title);

        okBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                MainWin.allowInter = allowInter.isSelected();
                Competition.QualiDHM2019 = QualiDHM2019ranking.isSelected();
                Competition.isDHM = DHM.isSelected();

                dialog.setVisible(false);
                okSelected = true;
            }
        });
        // [if closed with cancel do nothing]

        dialog.setVisible(true);
        return okSelected;

    }


    public static void main(String[] args) {
        DebugMenuWindow.show(null);
    }
}