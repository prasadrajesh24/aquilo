/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.gui;

import aquilo.entity.Competition;

import javax.swing.*;
import java.awt.event.*;
import java.io.*;
import java.time.LocalDate;
import java.time.ZoneId;


public final class FileMenuAndActions {

    private JFrame parentFrame;
    private JMenu fileMenu;
    private JMenuItem saveMenuItem, saveAsMenuItem;

    private File    file;
    private String  fileExtension = "chu";
    private String  fileDescription = "Student Riding Competitions";
    private boolean dataChanged;

    private FileActionListener application;

    //-------------------------------------------------------------------------

    public FileMenuAndActions(FileActionListener application, JFrame parentFrame) {

        this.parentFrame = parentFrame;
        this.application = application;
        file = null;
        dataChanged = false;

        fileMenu = new JMenu("File");
        JMenuItem menuItem;

        fileMenu.add(menuItem = new JMenuItem("New Competition ..."));
        menuItem.addActionListener(ae -> actionNew());

        fileMenu.add(menuItem = new JMenuItem("Open ..."));
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_DOWN_MASK));
        menuItem.addActionListener(ae -> actionOpen());

        fileMenu.add(saveMenuItem = new JMenuItem("Save"));
        saveMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));
        saveMenuItem.setEnabled(false);
        saveMenuItem.addActionListener(ae -> actionSave());

        fileMenu.add(saveAsMenuItem = new JMenuItem("Save As ..."));
        saveAsMenuItem.setEnabled(false);
        saveAsMenuItem.addActionListener(ae -> actionSaveAs());

        fileMenu.addSeparator();
        fileMenu.add(menuItem = new JMenuItem("Quit"));
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, InputEvent.CTRL_DOWN_MASK));
        menuItem.addActionListener(ae -> {
            int option = offerSaveIfDataChanged();
            if(option!=JOptionPane.CANCEL_OPTION) System.exit(0);
        });
    }

    public JMenu getMenu() { return fileMenu; }
    public File getFile() { return file; }

    // called by app if it wants to open a specific file, eg a cmd-line argument
    public void open(File file) {

        if(offerSaveIfDataChanged()==JOptionPane.CANCEL_OPTION) return;
        this.file = file;
        try {
            application.fileOpen(file);
        } catch(Exception ex) {
            JOptionPane.showMessageDialog(parentFrame,ex.getMessage(),
            	"Error reading file  "+file.getPath(),JOptionPane.ERROR_MESSAGE);
            return;
        }
        dataHasSavedState();
    }

    //--------------------------------------------------------------------
    // when is save allowed / needed?

    // called by app if save should be allowed
    public void dataChanged() {
        //System.out.println("dataChanged()");
        dataChanged = true;
        saveMenuItem.setEnabled(true);
        saveAsMenuItem.setEnabled(true);
    }

    // called by us after data has been saved
    private void dataHasSavedState() {
        //System.out.println("dataHasSavedState()");
        dataChanged = false;
        saveMenuItem.setEnabled(true);
        saveAsMenuItem.setEnabled(true);  // SaveAs should always work
    }

    //--------------------------------------------------------------------
    // file menu operations

    private void actionNew() {

        if(offerSaveIfDataChanged()==JOptionPane.CANCEL_OPTION) return;

        file = null;
        application.fileNew();
    }

    private boolean actionSave() {

        if(file==null)
            if( ! selectNewLocation() ) return false;
        try {
            application.fileSave(file);
        } catch(Exception ex) {
            JOptionPane.showMessageDialog(parentFrame,ex.getMessage(),
            	"Error writing file "+file.getPath(),JOptionPane.ERROR_MESSAGE);
        }
        dataHasSavedState();
        return true;
    }

    private void actionSaveAs() {

        if( ! selectNewLocation() ) return;
        actionSave();
    }

    private void actionOpen() {

        if( offerSaveIfDataChanged() == JOptionPane.CANCEL_OPTION)
        	return;
        JFileChooser fc = new JFileChooser(defaultFilePath());
        //fc.addChoosableFileFilter(new ExtensionFileFilter(fileExtension, fileDescription));
        if( fc.showOpenDialog(parentFrame) != JFileChooser.APPROVE_OPTION )
        	return;
        file = fc.getSelectedFile();
        try {
            application.fileOpen(file);
        } catch(Exception ex) {
            JOptionPane.showMessageDialog(parentFrame,ex.getMessage(),
            	"Error reading file  "+file.getPath(),JOptionPane.ERROR_MESSAGE);
            return;
        }
        dataHasSavedState();
    }

    // this may also be called from outside e.g. on App.close
    int offerSaveIfDataChanged() {

        String msg = (file==null) ? "Save changes?"
                                  : "Save changes to "+file.getPath()+"?";
        if(dataChanged) {
            int opt = JOptionPane.showConfirmDialog(parentFrame,
                        msg, "Closing current competition",
                        JOptionPane.YES_NO_CANCEL_OPTION);
            if(opt == JOptionPane.YES_OPTION) {
                if(!actionSave()) return JOptionPane.CANCEL_OPTION;
            }
            return opt;
        }
        return JOptionPane.NO_OPTION;
    }

    int offerSaveIfDataChangedExport(){

        String msg = (file==null) ? "Save changes?"
                                  : "Save changes to "+file.getPath()+"?";
        if(dataChanged) {
            int opt = JOptionPane.showConfirmDialog(parentFrame,
                    msg, "Export current competition",
                    JOptionPane.YES_NO_CANCEL_OPTION);
            if(opt == JOptionPane.YES_OPTION) {
                if(!actionSave()) return JOptionPane.CANCEL_OPTION;
            }
            return opt;
        }
        return JOptionPane.YES_OPTION;
    }

    private boolean selectNewLocation() {
        JFileChooser fc = new JFileChooser(defaultFilePath());
        fc.setSelectedFile(new File(Competition.name + " " + LocalDate.now(ZoneId.systemDefault()).getYear())); // File Name suggestion
        if( fc.showSaveDialog(parentFrame) != JFileChooser.APPROVE_OPTION )  return false;
        file = fc.getSelectedFile();
        String extension = getFileExtension(file);
        if (extension == null || !extension.equals(fileExtension)) {
            file = new File(file.getPath()+"."+fileExtension);
        }
        application.fileNameChanged(file);
        return true;
    }
    
    private File defaultFilePath() {
        File userPath = new File(System.getProperty("user.dir"));

            // for Windows in DE/EN try to open "My Documents"
//            File myDocsDE = new File(userPath,"Desktop");
//            if(myDocsDE.exists()) return myDocsDE;
//            File myDocsEN = new File(userPath,"Documents");
//            if(myDocsEN.exists()) return myDocsEN;

            return userPath;

    }

    //--------------------------------------
    // Some file related utilities

    private static String getFileExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');
        if (i > 0 &&  i < s.length() - 1)  ext = s.substring(i+1).toLowerCase();
        return ext;
    }

    class ExtensionFileFilter extends javax.swing.filechooser.FileFilter
    {
        private String extension;
        private String description;

        public ExtensionFileFilter(String extension, String description) {
            this.extension = extension;
            this.description = description;
        }

        public boolean accept(File f) {
            if (f.isDirectory()) return true;
            String ext = getFileExtension(f);
            return (ext != null && ext.equals(extension));
        }

        public String getDescription() {
            return description+" (*."+extension+")";
        }
    }

}
