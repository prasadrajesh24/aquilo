/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.gui;

import javax.swing.*;
import javax.swing.table.TableCellEditor;
import java.awt.*;
import java.awt.event.*;
import java.net.URL;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.List;
import java.io.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import aquilo.entity.Competition;
import aquilo.entity.Discipline;
import aquilo.entity.RankedObject;
import aquilo.util.*;
import aquilo.ranking.*;
import aquilo.htmlout.*;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public final class MainWin extends JFrame implements FileActionListener {

    private static final Logger logger = LoggerFactory.getLogger(MainWin.class);

    static MainWin self;

    public static FileMenuAndActions fileHandling;

    private JMenuBar menuBar;
    private JMenu competMenu, resultMenu, helpMenu, debugMenu;
    private JCheckBoxMenuItem showHelpStepsMenuItem;
    private JMenuItem teamDrMenuItem, teamSjMenuItem, ExportMenuItem, selectRoundsItem, penaltyMenuItem, disclosureMenuItem;
    private JLabel clockLbl = new JLabel();

    static JSplitPane mainSplitPane;
    static TeamRiderListPanel teamView;
    static JSplitPane discipSplitPane;
    static DisciplineDefPanel discipDefPanel;
    static JDesktopPane rndDesktop;

    static boolean allowInter = true;

    static int noOfTeamsInit = 12;          // override these in new competition dialog
    static int maxRidersForTwoPerHorse = 8;

    static Object dndObject;                //dnd = Drag and Drop

    static List<JTable> jTableRegister;   // need to call all table.stopEditing() before save

    //-------------------------------------------------------------------------

    public MainWin(File autoOpen) {
        super("Welcome to " + Meta.name);

        // set Icon Image
        URL iconURL = Objects.requireNonNull(getClass().getResource("/icon/Equestrian_pictogram.png"));
        ImageIcon icon = new ImageIcon(iconURL);
        super.setIconImage(icon.getImage());

        self = this;

        jTableRegister = new ArrayList<>();

        fileHandling = new FileMenuAndActions(this, this);
        setupClock();
        createMenuBar();

        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                int option = fileHandling.offerSaveIfDataChanged();
                if (option != JOptionPane.CANCEL_OPTION) System.exit(0);
            }
        });

        if (autoOpen == null) {
            showAboutDialog();
            // window empty after startup; splitPanes added after New/Open
            VersionCheck.checkVersion(); // starts UpdateChecker
        } else {
            VersionCheck.checkVersion();
            fileHandling.open(autoOpen);
        }

        setVisible(true);
        pack();             // calls preferredSize()

        //XHtmlViewer.showDivAndWait("HelpOnHelp");
        //XHtmlViewer.showDivAndWait("BeforeYouStart");
        //XHtmlViewer.showDiv("HowToStart");
    }

    public Dimension getPreferredSize() {
        return new Dimension(1024, 680);
    }

    private void createMenuBar() {
        menuBar = new JMenuBar();
        setJMenuBar(menuBar);

        menuBar.add(fileHandling.getMenu());

        JMenuItem menuItem;
        competMenu = new JMenu("Competition");
        competMenu.setEnabled(false);
        menuBar.add(competMenu);
        competMenu.add(menuItem = new JMenuItem("Competition Properties"));
        menuItem.addActionListener(ae -> CompetitionPropertiesDialog.show(MainWin.self, false));

        resultMenu = new JMenu("Ranking");
        resultMenu.setEnabled(false);
        menuBar.add(resultMenu);
        resultMenu.add(menuItem = new JMenuItem("Individual Dressage Ranking"));
        menuItem.setActionCommand(Discipline.ID[Discipline.DR]);
        menuItem.addActionListener(ae -> {
            Discipline d;
            try {
                d = Discipline.getForId(ae.getActionCommand());
            } catch (Exception ex) {
                return;
            }
            disciplineRankingToHtml(d);
        });

        resultMenu.add(menuItem = new JMenuItem("Individual Showjumping Ranking"));
        menuItem.setActionCommand(Discipline.ID[Discipline.SJ]);
        menuItem.addActionListener(ae -> {
            Discipline d;
            try {
                d = Discipline.getForId(ae.getActionCommand());
            } catch (Exception ex) {
                return;
            }
            disciplineRankingToHtml(d);
        });
        resultMenu.addSeparator();

        resultMenu.add(menuItem = new JMenuItem("Team Result Details - Input Order"));
        menuItem.addActionListener(ae -> detailedResultsToHtml(null));

        resultMenu.add(menuItem = new JMenuItem("Team Result Details by Team Combined Ranking"));
        menuItem.addActionListener(ae -> detailedResultsToHtml(new TeamCombinedComparator()));

        resultMenu.add(teamDrMenuItem = new JMenuItem("Team Result Details by Team Dressage Ranking"));
        teamDrMenuItem.addActionListener(ae -> detailedResultsToHtml(new TeamDressageComparator()));

        resultMenu.add(teamSjMenuItem = new JMenuItem("Team Result Details by Team Showjumping Ranking"));
        teamSjMenuItem.addActionListener(ae -> detailedResultsToHtml(new TeamShowjumpingComparator()));

        resultMenu.addSeparator();

        resultMenu.add(menuItem = new JMenuItem("All Final Rankings, Overview"));
        menuItem.addActionListener(ae -> rankingOverviewToHtml());

        resultMenu.addSeparator();
        resultMenu.add(menuItem = new JMenuItem("Results in Computer-Readable Format"));
        menuItem.addActionListener(ae -> resultsForComputer());

        resultMenu.add(ExportMenuItem = new JMenuItem("Export Competition"));
        if (Competition.isCHIU() || Competition.isCHU_GER()) {
            ExportMenuItem.setEnabled(true);
        }
        ExportMenuItem.addActionListener(e -> {
            int opt = fileHandling.offerSaveIfDataChangedExport();
            switch (opt) {
                case 0:
                    CompetitionExporter.exportCompetition();
                    break;
                case 1:
                    MessageWindow.showFileNotSaved();
                    break;
                case 2:
                    break;
            }
        });

        resultMenu.addSeparator();
        resultMenu.add(selectRoundsItem = new JMenuItem("[DHM] Select Rounds Ignore Above"));
        selectRoundsItem.addActionListener(e -> setIgnoreRoundsAbove());
        selectRoundsItem.setEnabled(Competition.isDHM);

        helpMenu = new JMenu("Help");
        helpMenu.add(menuItem = new JMenuItem("Abbreviation Map"));
        menuItem.addActionListener(ae -> showAbbreviationMap());
        menuBar.add(helpMenu);

        debugMenu = new JMenu("...");
        debugMenu.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        debugMenu.add(menuItem = new JMenuItem("Debug Options"));
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_DOWN_MASK + InputEvent.SHIFT_DOWN_MASK));
        menuItem.addActionListener(e -> {
            DebugMenuWindow.show(self);
            selectRoundsItem.setEnabled(Competition.isDHM);
        });

        debugMenu.add(penaltyMenuItem = new JMenuItem("Generate Penalty Points Template"));
        penaltyMenuItem.addActionListener(ae -> {
            try {
                Penalties.generate();
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(MainWin.self, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }

        });
        penaltyMenuItem.setEnabled(false);

        debugMenu.add(disclosureMenuItem = new JMenuItem("Print Disclosure of Liability (LaTeX required)"));
        disclosureMenuItem.addActionListener(e -> {
            try {
                HtmlOutput.showHtmlMsg = false;
                LiabilityDisclosure.writeFile(null);
                HtmlOutput.showHtmlMsg = true;
            } catch (Exception ex) {
                logger.warn(ex.getMessage(), ex);
                JOptionPane.showMessageDialog(MainWin.self, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        });
        disclosureMenuItem.setEnabled(false);
        menuBar.add(Box.createHorizontalGlue());
        menuBar.add(clockLbl);
        menuBar.add( Box.createHorizontalStrut( 10 ) );
        menuBar.add(debugMenu);

        /*
        menuItem = new JMenuItem("Show Last Topic");
        menuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                XHtmlViewer.showLast();
            }
        });
        helpMenu.add(menuItem);
        */

        menuItem = new JMenuItem("Open Help in Webbrowser");
        menuItem.addActionListener(ae -> {
            try {
                File writeLocation = HelpPrinter.writeHtmlFile(null);
                WebBrowser.open(writeLocation);
            } catch (Exception ex) {
                logger.warn(ex.getMessage(), ex);
                JOptionPane.showMessageDialog(MainWin.self, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        });
        helpMenu.add(menuItem);
        helpMenu.addSeparator();

        /*
        XHtmlViewer.addTopicsToMenu(helpMenu);
        helpMenu.addSeparator();

        showHelpStepsMenuItem = new JCheckBoxMenuItem("Enable Step By Step Popups",true);
        XHtmlViewer.setShowSteps(showHelpStepsMenuItem.getState());  // sync init state
        showHelpStepsMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                XHtmlViewer.setShowSteps(showHelpStepsMenuItem.getState());
            }
        });
        helpMenu.add(showHelpStepsMenuItem);
        helpMenu.addSeparator();
        */

        helpMenu.add(menuItem = new JMenuItem("About..."));
        menuItem.addActionListener(ae -> showAboutDialog());
    }

    private void setIgnoreRoundsAbove() {
        int roundsIgnore = Competition.disciplines[Discipline.DR].getIgnoreRoundsAbove();
        try {
            roundsIgnore = MessageWindow.setRoundsIgnoreAbove(Competition.disciplines[Discipline.DR].getIgnoreRoundsAbove());
        } catch (java.lang.Exception langEx) {
            logger.warn(langEx.getMessage(), langEx);
        }
        for (int i = 0; i < Competition.disciplines.length; i++) {
            Competition.disciplines[i].setIgnoreRoundsAbove(roundsIgnore);
            for (int k = 0; k < Competition.disciplines[i].getNoOfRounds(); k++) {
                Competition.disciplines[i].getRound(k).assignDiffsForAllHorses();
            }
        }
        discipDefPanel.updateTable();
    }

    private void showCompetitionData() {

        // if main win not empty remove what's currently displayed
        if (mainSplitPane != null)
            getContentPane().remove(mainSplitPane);

        setTitleFromData();

        // left-hand side of main win: teams and riders
        teamView = new TeamRiderListPanel();

        // right-hand side of main win: discipline defs and rounds
        discipDefPanel = new DisciplineDefPanel(Competition.disciplines);
        rndDesktop = new JDesktopPane();
        discipSplitPane = new JSplitPane(
                JSplitPane.VERTICAL_SPLIT, discipDefPanel, rndDesktop);
        discipSplitPane.setOneTouchExpandable(true);
        discipSplitPane.setDividerLocation(100);

        // put left and right side together into the main frame
        mainSplitPane = new JSplitPane(
                JSplitPane.HORIZONTAL_SPLIT, teamView, discipSplitPane);
        mainSplitPane.setOneTouchExpandable(true);
        mainSplitPane.setDividerLocation(300);
        getContentPane().add(mainSplitPane, BorderLayout.CENTER);

        competMenu.setEnabled(true);
        resultMenu.setEnabled(true);
        penaltyMenuItem.setEnabled(true);
        disclosureMenuItem.setEnabled(true);

        pack();
    }

    private void setTitleFromData() {
        String s = Competition.name;
        File f = fileHandling.getFile();
        if (f != null) {
            s += "  [" + f.getAbsolutePath() + "]";
        } else {
            s = Meta.name;
        }
        setTitle(s);
    }

    //--------------------------------------------------------------------
    // implement FileActionListener operations

    public void fileNew() {

        if (!CompetitionPropertiesDialog.show(this, true)) return;
        if (Competition.isCHU_GER() && maxRidersForTwoPerHorse != 4 && !Competition.isDHM) {
            int changed = MessageWindow.showFalseMaxRidersForTwoPerHorse(maxRidersForTwoPerHorse);
            switch (changed) {
                case 0:
                    maxRidersForTwoPerHorse = 4;
                    break;
                case 1:
                    break;
            }
        }
        Competition.createEmptyCompetition(noOfTeamsInit, maxRidersForTwoPerHorse);
        fileHandling.dataChanged();
        showCompetitionData();
        //XHtmlViewer.showDiv("EmptyCompetition");
    }

    public void fileOpen(File file) throws Exception {

        Competition.readFromXml(file);
        //Competition.printTeamsWithRiders(true);
        showCompetitionData();

        boolean extraTeamRankings = Competition.isSRNC() || Competition.isCHIU();
        teamDrMenuItem.setEnabled(extraTeamRankings);
        teamSjMenuItem.setEnabled(extraTeamRankings);

        selectRoundsItem.setEnabled(Competition.isDHM);
    }

    public void fileSave(File file) throws Exception {
        // make sure edits in all tables are saved
        for (int i = 0; i < jTableRegister.size(); i++) {
            //t.editingStopped(null);
            saveEditingValueInTable(jTableRegister.get(i));
        }
        Competition.writeAsXml(file);
    }

    public void fileNameChanged(File newFile) {
        setTitleFromData();
    }

    // helper method for fileSave only
    // myTable.editingStopped(null) should be all we need; but buggy Impl in JTable
    private static void saveEditingValueInTable(JTable t) {
        // reimplementation JTable.editingStopped()
        TableCellEditor editor = t.getCellEditor();
        if (editor == null) return;
        int row = t.getEditingRow();
        int col = t.getEditingColumn();
        editor.stopCellEditing();  // this call is missing in JTable.editingStopped();
        Object value = editor.getCellEditorValue();
        t.setValueAt(value, row, col);
        //System.out.println("table.setValueAt("+value+","+row+","+col+")");
        t.removeEditor();
    }

    //-------------------------------------------------------------------------
    // some of the Competition menu operations

    private void disciplineRankingToHtml(Discipline d) {
        try {
            File writeLocation = BasicIndividualRanking.writeHtmlFile(d, null);
            WebBrowser.open(writeLocation);
        } catch (Exception ex) {
            logger.warn(ex.getMessage(), ex);
            JOptionPane.showMessageDialog(MainWin.self, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void detailedResultsToHtml(Comparator<RankedObject> teamComparator) {
        //Competition.printTeamsWithRiders(true);
        try {
            File writeLocation = TeamResultDetails.writeHtmlFile(null, teamComparator);
            WebBrowser.open(writeLocation);
        } catch (Exception ex) {
            logger.warn(ex.getMessage(), ex);
            JOptionPane.showMessageDialog(MainWin.self, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void rankingOverviewToHtml() {
        try {
            File writeLocation = RankingOverview.writeHtmlFile(null);
            WebBrowser.open(writeLocation);
        } catch (Exception ex) {
            logger.warn(ex.getMessage(), ex);
            JOptionPane.showMessageDialog(MainWin.self, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void resultsForComputer() {
        try {
            File writeLocation = ComputerReadableResults.writeFile(null);
            WebBrowser.open(writeLocation);
        } catch (Exception ex) {
            logger.warn(ex.getMessage(), ex);
            JOptionPane.showMessageDialog(MainWin.self, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    //-------------------------------------------------------------------------
    // About Dialog

    private static final String licenseStartText =
            Meta.name + " " + Meta.description + "\n" +
            Meta.getCopyright() + "\n\n" +
            " This program is free software: you can redistribute it and/or modify\n" +
            " it under the terms of the GNU General Public License as published by\n" +
            " the Free Software Foundation, either version 3 of the License, or\n" +
            " (at your option) any later version.\n" +
            " \n" +
            " This program is distributed in the hope that it will be useful,\n" +
            " but WITHOUT ANY WARRANTY; without even the implied warranty of\n" +
            " MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n" +
            " GNU General Public License for more details.\n" +
            " \n" +
            " You should have received a copy of the GNU General Public License\n" +
            " along with this program.  If not, see <https://www.gnu.org/licenses/>.";

    private static final String contributors = "\n\nContributors:"
            + "\n2018-" + Meta.getIssueYear() + ": " + Meta.maintainer + " <philipp.tegtemeyer@alumni.fh-aachen.de>"
            + "\n2018-2019: Johannes Pavel <pavel.johannes@gmail.com>"
            + "\n1999-2018: Andreas Steidle <andreas@equinis.com>";

    public static void showAboutDialog() {
        String text = licenseStartText + contributors;
        JTextArea textArea = new JTextArea(text, 10, 40);
        textArea.setEditable(false);
        Object[] aboutElements = {"\n", Meta.nameVersionIssueDate(), "\n",
                Meta.website, "\n",
                new JScrollPane(textArea), "\n",
                Meta.expireMessage(), "\n"};

        JOptionPane.showMessageDialog(null,
                aboutElements,
                "Welcome to " + Meta.name,
                JOptionPane.INFORMATION_MESSAGE);
    }

    private void showAbbreviationMap() {
        JTextArea textArea = new JTextArea(new CityComp().CityString(),25,25);
        textArea.setEditable(false);

        JOptionPane.showMessageDialog(MainWin.self, new JScrollPane(textArea), "Abbreviation Map",
                JOptionPane.INFORMATION_MESSAGE);

    }

    private void setupClock() {
        Clock clock = Clock.tickSeconds(ZoneId.systemDefault());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");

        final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

        Runnable r = () -> {
            String time = LocalDateTime.now(clock).toLocalTime().format(formatter);
            SwingUtilities.invokeLater(() -> clockLbl.setText(time));
        };

        scheduler.scheduleAtFixedRate(r, 0, 1, TimeUnit.SECONDS);
    }

}

