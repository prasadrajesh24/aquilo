/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.gui;

import java.util.ArrayList;
import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.dnd.*;
import java.awt.datatransfer.*;

import aquilo.entity.Rider;

/**
    Small JTable containing the 3 riders of a team.
    
    Has no headers.
    Supports dragging riders to round windows.
    Used by TeamPanel.
    
    @see TeamPanel, chiupro.Rider
*/

final class RiderTable extends JTable {

    private ArrayList<Rider> riders;

    private DragRiderListener       draggl;
    private DragSource              dragSource;

    private static final String[] columnTipText = {
        "<html>riders boot number<br>drag this into round window to assign rider to horse ",
        "riders firstname",
        "riders lastname" };
    private static final int[] columnPreferredWidth = { 2, 10, 15 };

    //-------------------------------------------------------------------------
    
    RiderTable(ArrayList<Rider> riders) {
        
        this.riders = riders;
        TableModel model = new Model();
        setModel(model);
        setRowSelectionAllowed(false);
        // set column properties
        TableColumnModel tcm = getColumnModel();
        for(int c=0; c<model.getColumnCount(); c++ ) {
            TableColumn column = tcm.getColumn(c);
            column.setPreferredWidth(columnPreferredWidth[c]*6);
            DefaultTableCellRenderer columnRenderer = new DefaultTableCellRenderer();
            if(c==0) {
                columnRenderer.setBackground(Color.lightGray);
                columnRenderer.setHorizontalAlignment(SwingConstants.CENTER);
            }
            columnRenderer.setToolTipText(columnTipText[c]);
            column.setCellRenderer(columnRenderer);
        }
        dragSource = new DragSource();
        draggl = new DragRiderListener();
        dragSource.createDefaultDragGestureRecognizer(this,DnDConstants.ACTION_MOVE,draggl);
        
        ((DefaultCellEditor)getDefaultEditor(Object.class)).setClickCountToStart(1);
        
        MainWin.jTableRegister.add(this);      // call editingStopped() before save
    }
    
    //-------------------------------------------------------------------------
    
    final class Model extends AbstractTableModel {

        public int getRowCount()    { return riders.size(); }
        public int getColumnCount() { return 3; }

        public Object getValueAt(int row, int col) {
            Rider r = riders.get(row);
            switch( col ) {
                case 0: return r.bootNo;
                case 1: return r.firstName;
                case 2: return r.getLastName();
            }
            return null;
        }
        public boolean isCellEditable(int row, int col) {
            return col != 0;
        }
        public void setValueAt(Object newVal, int row, int col) {
            if( newVal.equals(getValueAt(row,col)) )  return;
            Rider r = riders.get(row);
            switch( col ) {
                case 0: return;
                case 1: r.firstName = ((String)newVal).trim(); break;
                case 2: r.setLastName(((String)newVal).trim()); break;
            }
    	    fireTableCellUpdated(row, col);
            MainWin.fileHandling.dataChanged();
        }
    }

    //-------------------------------------------------------------------------
    // implementation of DragGestureListener: a drag gesture has been initiated

    final class DragRiderListener implements DragGestureListener {

        /**
            A drag occurred on a RiderTable.
            Allow drag only on first column (bootNo); Drag on editable columns would leave
            cells in edit mode.
        */
        public void dragGestureRecognized(DragGestureEvent event) {
            
            int col = columnAtPoint(event.getDragOrigin());
            if(col != 0) return;
            int row = rowAtPoint(event.getDragOrigin());
            Rider rider = (Rider)riders.get(row);
            //System.out.println("Source: dragGestureRecognized:"+selectedRider);
            if ( rider != null ) {
                MainWin.dndObject = rider;
                StringSelection dummy = new StringSelection("");
                dragSource.startDrag(event, DragSource.DefaultMoveDrop, dummy, null);
            } 
        }
    }
}
