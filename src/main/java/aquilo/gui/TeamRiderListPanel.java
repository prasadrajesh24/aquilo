/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.File;

import aquilo.entity.Competition;
import aquilo.entity.Team;
import aquilo.util.WebBrowser;
import aquilo.htmlout.*;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * Stacks multiple TeamPanels in a vertical ScrollPane.
 * 
 * Label at the top. Button to print all teams with their riders at the bottom.
 * Used in left hand side of the MainWin.
 * 
 * @see TeamPanel
 * @see TeamsAndRiders
 */
final class TeamRiderListPanel extends JPanel {

	private static final Logger logger = LoggerFactory.getLogger(TeamRiderListPanel.class);

	TeamRiderListPanel() {
		super(new BorderLayout());

		JLabel lbl = new JLabel("Teams and Riders");
		lbl.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 8));
		add(lbl, BorderLayout.NORTH);

		Box teambox = new Box(BoxLayout.Y_AXIS);
		// teambox.add(Box.createRigidArea(new Dimension(0, 5)));
		for (Iterator<Team> i = Competition.teams.iterator();;) {
			teambox.add(new TeamPanel(i.next()));
			if (!i.hasNext())
				break;
			teambox.add(Box.createRigidArea(new Dimension(0, 15)));
		}
		teambox.add(Box.createRigidArea(new Dimension(0, 5)));
		JScrollPane teamScroller = new JScrollPane(teambox);
		teamScroller.getVerticalScrollBar().setUnitIncrement(31);
		add(teamScroller, BorderLayout.CENTER);

		JPanel btnPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 5));
		// btnPanel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
		add(btnPanel, BorderLayout.SOUTH);

		JButton btnPrint = new JButton("Print Team/Rider Table");
		btnPrint.setToolTipText("All teams&riders in a table with empty columns for notes");
		btnPrint.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Competition.printTeamsWithRiders(false);
				try {
					File writeLocation = TeamsAndRiders.writeHtmlFile();
					WebBrowser.open(writeLocation);
				} catch (Exception ex) {
					logger.warn(ex.getMessage(), ex);
					JOptionPane.showMessageDialog(MainWin.self, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		btnPanel.add(btnPrint);

	}
}
