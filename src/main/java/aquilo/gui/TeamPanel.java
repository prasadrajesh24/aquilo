/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.gui;

import javax.swing.*;
import java.awt.event.*;

import aquilo.entity.Competition;
import aquilo.entity.ResultStyle;
import aquilo.entity.Team;
import aquilo.util.CityComp;
import aquilo.util.Lang;

/**
    Panel showing Team attributes and its 3 riders in a RiderTable
    
    @see RiderTable
    @see TeamRiderListPanel
    chiupro.Team
*/

public final class TeamPanel extends Box {

    private CityComp city = new CityComp();

    private Team team;
    private JTextField  tfName;
    private JTextField  tfAbbreviation;
    private JTextField  tfDressTeamMark;
    private JTable      tblRiders;

    private final String tfNameInit = "Team Name";                  //todo: change init field colors to light gray
    private final String tfAbbreviationInit = Lang.t("Abb.");
    private final String tfDressTeamMarkInit = Lang.t("Mark");

    private static int nrOfMixTeams = 0;                            // TODO: 07.03.2021 change to List to keep track of add/sub of MixTeams 

    static final String[] toolTipText = {
            "team name",
            "team abbreviation",
            "<html>dressage team mark<br>'e' for eliminate<br>'r' for retired during ride<br>'n' no (team) ranking"};

    //-------------------------------------------------------------------------

    TeamPanel(final Team t) {
        super(BoxLayout.Y_AXIS);
        team = t;
        JPanel pnlTeamData = new JPanel();

        tfName = new JTextField(t.name,13);
        tfName.setToolTipText(toolTipText[0]);
        if(t.name.isEmpty()){
            tfName.setText(tfNameInit);
        }

        tfName.addFocusListener( new FocusListener() {
            public void focusGained(FocusEvent e) {
                if (tfName.getText().equals(tfNameInit)){
                    tfName.setText("");
                }
            }

            public void focusLost(FocusEvent e) {
                if (tfName.getText().isEmpty()){
                    tfName.setText(tfNameInit);
                }
                if( t.name.equals(tfName.getText()) )  return;
                if (tfName.getText().equals(tfNameInit)){
                    t.name="";
                }else {
                    t.name = tfName.getText();
                }
                if (!tfName.getText().equals(tfNameInit)){
                    String cityAbbrev = city.getAbbreviation(tfName.getText().strip());
                    if(cityAbbrev != null) {
                        tfAbbreviation.setText(cityAbbrev);
                        t.abbreviation = tfAbbreviation.getText();
                    } else if (Competition.isCHU_GER() && !Competition.isDHM){
                        int res = MessageWindow.showCityNotFound(0);
                        switch (res){
                            case 0:
                                nrOfMixTeams++;
                                tfAbbreviation.setText("MIX " + nrOfMixTeams);
                                t.abbreviation = tfAbbreviation.getText();
                                tfName.setText("Mixed Team " + nrOfMixTeams);
                                t.name = tfName.getText();
                                break;
                            case 1:
                                tfAbbreviation.setText(tfAbbreviationInit);
                                t.abbreviation = "";
                                tfName.setText(tfNameInit);
                                t.name = "";
                                break;
                        }
                    }
                }
                MainWin.fileHandling.dataChanged();
            }
        });
        pnlTeamData.add(tfName);

        tfAbbreviation = new JTextField(t.abbreviation,5);
        tfAbbreviation.setToolTipText(toolTipText[1]);
        if(t.abbreviation.isEmpty()){
            tfAbbreviation.setText(tfAbbreviationInit);
        }

        tfAbbreviation.addFocusListener( new FocusListener() {
            public void focusGained(FocusEvent e) {
                if (tfAbbreviation.getText().equals(tfAbbreviationInit)){
                    tfAbbreviation.setText("");
                }
            }

            public void focusLost(FocusEvent e) {
                if (tfAbbreviation.getText().isEmpty()){
                    tfAbbreviation.setText(tfAbbreviationInit);
                }
                if (t.abbreviation.equals(tfAbbreviation.getText())) {
                    return;
                }
                if (tfAbbreviation.getText().equals(tfAbbreviationInit)){
                    t.abbreviation="";
                }else {
                    t.abbreviation = tfAbbreviation.getText().toUpperCase();
                    tfAbbreviation.setText(t.abbreviation);
                }
                if (!tfAbbreviation.getText().equals(tfAbbreviationInit)) {
                    String cityName = city.getCityName(tfAbbreviation.getText().strip());
                    if(cityName != null) {
                        tfName.setText(cityName);
                        t.name = tfName.getText();
                    } else if (Competition.isCHU_GER() && !Competition.isDHM) {
                        int res = MessageWindow.showCityNotFound(1);
                        switch (res) {
                            case 0:
                                nrOfMixTeams++;
                                tfAbbreviation.setText("MIX " + nrOfMixTeams);
                                t.abbreviation = tfAbbreviation.getText();
                                tfName.setText("Mixed Team " + nrOfMixTeams);
                                t.name = tfName.getText();
                                break;
                            case 1:
                                tfAbbreviation.setText(tfAbbreviationInit);
                                t.abbreviation = "";
                                tfName.setText(tfNameInit);
                                t.name = "";
                                break;
                        }
                    }
                }
                MainWin.fileHandling.dataChanged();
            }
        });
        pnlTeamData.add(tfAbbreviation);

        tfDressTeamMark = new JTextField(t.dressageResult.toString(),4);
        tfDressTeamMark.setToolTipText(toolTipText[2]);
        if(t.dressageResult.toString().isEmpty()){
            tfDressTeamMark.setText(tfDressTeamMarkInit);
        }

        tfDressTeamMark.addFocusListener( new FocusListener() {
            public void focusGained(FocusEvent e) {
                if (tfDressTeamMark.getText().equals(tfDressTeamMarkInit)){
                    tfDressTeamMark.setText("");
                }
            }

            public void focusLost(FocusEvent e) {
                String numString = tfDressTeamMark.getText();
                boolean changed = t.dressageResult.set(ResultStyle.STYLE,numString);
                if( changed )  MainWin.fileHandling.dataChanged();
                tfDressTeamMark.setText( t.dressageResult.toString() );
                if (t.dressageResult.toString().isEmpty()){
                    tfDressTeamMark.setText(tfDressTeamMarkInit);
                }
            }
        });
        pnlTeamData.add(tfDressTeamMark);

        tblRiders = new RiderTable(team.getRiders());

        add(pnlTeamData);
        add(tblRiders);
    }

}

