/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.gui;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class StilpreisDialog {

    private static final Logger logger = LoggerFactory.getLogger(StilpreisDialog.class);

    private static boolean ignored;
    private static String Stilpreisgewinner;

    private static JTextField  DRSurname,  DRLastname,  DRStreet,  DRZipCode,  DRCity,  SJSurname,  SJLastname,  SJStreet,  SJZipCode,  SJCity;

    public boolean getStilpreisData(){

        final JPanel Data = new JPanel();

        Data.setLayout(new GridBagLayout());
        GridBagConstraints gbc;
        Insets globalinsets = new Insets(10,10,0,10);

        final  JLabel HeaderLabel = new JLabel();
        HeaderLabel.setText("Gewinner Stilpreise:");
        HeaderLabel.setFont(HeaderLabel.getFont().deriveFont(Font.BOLD,22));
        gbc = new GridBagConstraints();
        gbc.gridx = 6;
        gbc.gridy = 0;
        gbc.gridwidth = 8;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.insets = new Insets(0,0,10,0);
        Data.add(HeaderLabel,gbc);

        final JLabel label1 = new JLabel();
        label1.setText("Dressur:");
        label1.setFont(label1.getFont().deriveFont(Font.BOLD,16));
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = globalinsets;
        Data.add(label1, gbc);

        final JLabel label3 = new JLabel();
        label3.setText("Vorname:");
        gbc = new GridBagConstraints();
        gbc.gridx = 7;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = globalinsets;
        Data.add(label3, gbc);
        
        DRSurname = new JTextField();
        DRSurname.setColumns(14);
        gbc = new GridBagConstraints();
        gbc.gridx = 9;
        gbc.gridy = 1;
        gbc.gridwidth = 2;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = globalinsets;
        Data.add(DRSurname, gbc);

        final JLabel label4 = new JLabel();
        label4.setText("Nachname:");
        gbc = new GridBagConstraints();
        gbc.gridx = 12;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = globalinsets;
        Data.add(label4, gbc);

        DRLastname = new JTextField();
        DRLastname.setColumns(14);
        gbc = new GridBagConstraints();
        gbc.gridx = 14;
        gbc.gridy = 1;
        gbc.gridwidth = 3;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = globalinsets;
        Data.add(DRLastname, gbc);

        final JLabel label5 = new JLabel();
        label5.setText("Stra\u00dfe:");
        gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 3;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = globalinsets;
        Data.add(label5, gbc);
        
        DRStreet = new JTextField();
        DRStreet.setColumns(26);
        gbc = new GridBagConstraints();
        gbc.gridx = 5;
        gbc.gridy = 3;
        gbc.gridwidth = 10;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = globalinsets;
        Data.add(DRStreet, gbc);

        final JLabel label6 = new JLabel();
        label6.setText("PLZ:");
        gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 5;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = globalinsets;
        Data.add(label6, gbc);

        gbc = new GridBagConstraints();
        gbc.gridx = 5;
        gbc.gridy = 5;
        gbc.gridwidth = 3;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = globalinsets;
        DRZipCode = new JTextField();
        Data.add(DRZipCode, gbc);

        gbc = new GridBagConstraints();
        gbc.gridx = 10;
        gbc.gridy = 5;
        gbc.gridwidth = 5;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = globalinsets;
        DRCity = new JTextField();
        Data.add(DRCity, gbc);

        final JLabel label7 = new JLabel();
        label7.setText("Ort:");
        gbc = new GridBagConstraints();
        gbc.gridx = 9;
        gbc.gridy = 5;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = globalinsets;
        Data.add(label7, gbc);

        final JLabel label8 = new JLabel();
        label8.setText("Springen:");
        label8.setFont(label8.getFont().deriveFont(Font.BOLD,16));
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 10;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = globalinsets;
        Data.add(label8, gbc);

        final JLabel label10 = new JLabel();
        label10.setText("Vorname:");
        gbc = new GridBagConstraints();
        gbc.gridx = 7;
        gbc.gridy = 10;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = globalinsets;
        Data.add(label10, gbc);
        
        SJSurname = new JTextField();
        SJSurname.setColumns(14);
        gbc = new GridBagConstraints();
        gbc.gridx = 9;
        gbc.gridy = 10;
        gbc.gridwidth = 2;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = globalinsets;
        Data.add(SJSurname, gbc);

        final JLabel label11 = new JLabel();
        label11.setText("Nachname:");
        gbc = new GridBagConstraints();
        gbc.gridx = 12;
        gbc.gridy = 10;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = globalinsets;
        Data.add(label11, gbc);

        SJLastname = new JTextField();
        SJLastname.setColumns(14);
        gbc = new GridBagConstraints();
        gbc.gridx = 14;
        gbc.gridy = 10;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = globalinsets;
        Data.add(SJLastname, gbc);

        final JLabel label12 = new JLabel();
        label12.setText("Stra\u00dfe:");
        gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 12;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = globalinsets;
        Data.add(label12, gbc);

        SJStreet = new JTextField();
        SJStreet.setColumns(26);
        gbc = new GridBagConstraints();
        gbc.gridx = 5;
        gbc.gridy = 12;
        gbc.gridwidth = 10;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = globalinsets;
        Data.add(SJStreet, gbc);

        final JLabel label13 = new JLabel();
        label13.setText("PLZ:");
        gbc = new GridBagConstraints();
        gbc.gridx = 3;
        gbc.gridy = 14;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = globalinsets;
        Data.add(label13, gbc);

        gbc = new GridBagConstraints();
        gbc.gridx = 5;
        gbc.gridy = 14;
        gbc.gridwidth = 3;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = globalinsets;
        SJZipCode = new JTextField();
        Data.add(SJZipCode, gbc);

        gbc = new GridBagConstraints();
        gbc.gridx = 10;
        gbc.gridy = 14;
        gbc.gridwidth = 5;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = globalinsets;
        SJCity = new JTextField();
        Data.add(SJCity, gbc);

        final JLabel label14 = new JLabel();
        label14.setText("Ort:");
        gbc = new GridBagConstraints();
        gbc.gridx = 9;
        gbc.gridy = 14;
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = globalinsets;
        Data.add(label14, gbc);

        final JSeparator separator1 = new JSeparator();
        gbc = new GridBagConstraints();
        gbc.gridx = 1;
        gbc.gridy = 8;
        gbc.gridwidth = 14;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.insets=globalinsets;
        Data.add(separator1, gbc);

        JOptionPane op = new JOptionPane(Data);
        JButton okBut = new JButton(" OK ");
        JButton ignoreBut = new JButton("Cancel");
        Object [] options = { okBut, ignoreBut };
        op.setOptions(options);

        final JDialog dialog = op.createDialog(MainWin.self,"Gewinner Stilpreis");

        okBut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean success;
                success = generateOutput(DRSurname,DRLastname,DRStreet,DRZipCode,DRCity,SJSurname,SJLastname,SJStreet,SJZipCode,SJCity);
                if (!success){
                    dialog.setVisible(false);
                    getStilpreisData();
                    return;
                }
                ignored = false;
                dialog.setVisible(false);
            }
        });

        ignoreBut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ignored = true;
                dialog.setVisible(false);
            }
        });

        dialog.setVisible(true);
        return ignored;
    }

    private static boolean generateOutput(JTextField drSurname, JTextField drLastname, JTextField drStreet, JTextField drZipCode, JTextField drCity,
                                       JTextField sjSurname, JTextField sjLastname, JTextField sjStreet, JTextField sjZipCode, JTextField sjCity){

        try{
            try {
                String DRSurname = drSurname.getText();
                String DRLastName = drLastname.getText();
                String DRStreet = drStreet.getText();
                int DRZipCode = Integer.parseInt(drZipCode.getText());
                String DRCity = drCity.getText();

                String SJSurname = sjSurname.getText();
                String SJLastName = sjLastname.getText();
                String SJStreet = sjStreet.getText();
                int SJZipCode = Integer.parseInt(sjZipCode.getText());
                String SJCity = sjCity.getText();

                String sp = " | ";

                Stilpreisgewinner = "Dressur: " + DRSurname + sp + DRLastName + sp + DRStreet + sp + DRZipCode + sp + DRCity + "\n\n"+
                        "Springen: " +  SJSurname + sp + SJLastName + sp + SJStreet + sp + SJZipCode + sp + SJCity;


            }
            catch (NumberFormatException NumEx){
                String msg;
                if (drZipCode.getText().equals("")||sjZipCode.getText().equals("")){
                    msg = "Nichts eingegeben!!!";
                }else {
                    msg = "Im Feld PLZ d\u00fcrfen nur Zahlen stehen!";
                }
                throw new Exception(msg);
            }
            catch (Exception ex){
                String msg = "Error";
                throw new Exception(msg);
            }

        }catch (Exception ex){
            JOptionPane.showMessageDialog(MainWin.self,ex.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
            logger.warn(ex.getMessage(), ex);
            return false;
        }
        return true;
    }

    public static String getStilpreise(){
        return Stilpreisgewinner;
    }


    public static void main(String[] args) {
        boolean complete = new StilpreisDialog().getStilpreisData();
        String message = (complete) ? "Success":"Error";
        System.out.println(message);
        System.out.println(Stilpreisgewinner);
        System.exit(0);
    }

}