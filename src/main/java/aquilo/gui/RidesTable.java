/*
 * This file is part of Aquilo.
 *
 *  Aquilo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Aquilo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Aquilo.  If not, see http://www.gnu.org/licenses/.
 */

package aquilo.gui;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.awt.dnd.*;
import java.awt.datatransfer.*;
import java.util.List;

import aquilo.comparator.*;
import aquilo.entity.*;
import aquilo.util.*;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

//============================================================================

class RidesTable extends JTable implements SwingConstants {

    private static final Logger logger = LoggerFactory.getLogger(RidesTable.class);

    private RidesTable self;
    Round rnd;

    private List<String> columnTitles;   // init by createRideColumns, used by Model
    private List<Boolean> columnEditable;
    private List<Object> columnComparator;
    AbstractTableModel model;

    private final static int COLUMNINDEX_START = 0;
    private final static int COLUMNINDEX_HORSENO = 1;
    private final static int COLUMNINDEX_HORSE = 2;
    private final static int COLUMNINDEX_RIDER = 3;
    private final static int COLUMNINDEX_TEAM = 4;
    private final static int COLUMNINDEX_1stRESULT = 5;
    private int COLUMNINDEX_1stDIFF;       // depends on no of individual result values

    //----------------------------------------

    RidesTable(Round rnd) {

        super();
        self = this;
        this.rnd = rnd;
        columnTitles = new ArrayList<>();
        columnEditable = new ArrayList<>();
        columnComparator = new ArrayList<>();

        // customize our table
        model = new Model();
        setModel(model);
        createRideColumns(rnd.getRide(0));
        setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        addMouseListenerToTableHeader();

        // table dimension; will influence dim of RoundWindow
        int initWidth = getColumnModel().getTotalColumnWidth();
        int initHeigth = (getRowCount() + 1) * (getRowHeight()) + 4;
        if (initHeigth > 440) initHeigth = 440;
        setPreferredScrollableViewportSize(new Dimension(initWidth, initHeigth));

        initDragDropSupport();
        initKeyBindings();

        MainWin.jTableRegister.add(this);      // call editingStopped() before save
    }

    private void createRideColumns(Ride dummyRide) {
        TableColumn column;
        DefaultTableCellRenderer cellRenderer;
        Color NO_EDIT_GRAY = new Color(0xeeeeee);

        columnTitles.add(COLUMNINDEX_START, "Start");
        columnEditable.add(COLUMNINDEX_START, Boolean.FALSE);
        cellRenderer = new DefaultTableCellRenderer();  // subclass of JLabel!!
        cellRenderer.setBackground(NO_EDIT_GRAY);
        cellRenderer.setHorizontalAlignment(CENTER);
        cellRenderer.setToolTipText("starting order");
        column = new TableColumn(COLUMNINDEX_START, 30, cellRenderer, null);
        addColumn(column);
        columnComparator.add(COLUMNINDEX_START, new RideStartNoComparator());

        columnTitles.add(COLUMNINDEX_HORSENO, "HorseNo");
        columnEditable.add(COLUMNINDEX_HORSENO, Boolean.FALSE);
        cellRenderer = new DefaultTableCellRenderer();
        cellRenderer.setBackground(NO_EDIT_GRAY);
        cellRenderer.setHorizontalAlignment(CENTER);
        cellRenderer.setToolTipText("..th rider on horse number..");
        column = new TableColumn(COLUMNINDEX_HORSENO, 60, cellRenderer, null);
        addColumn(column);
        columnComparator.add(COLUMNINDEX_HORSENO, new RideHorseNoComparator());

        columnTitles.add(COLUMNINDEX_HORSE, "Horse");
        columnEditable.add(COLUMNINDEX_HORSE, Boolean.TRUE);
        cellRenderer = new DefaultTableCellRenderer();
        cellRenderer.setToolTipText("enter horses name here");
        column = new TableColumn(COLUMNINDEX_HORSE, 110, cellRenderer, null);
        addColumn(column);
        columnComparator.add(COLUMNINDEX_HORSE, new RideHorseNoComparator());

        columnTitles.add(COLUMNINDEX_RIDER, "Rider");
        columnEditable.add(COLUMNINDEX_RIDER, Boolean.TRUE);
        cellRenderer = new DefaultTableCellRenderer();
        cellRenderer.setToolTipText("<html>enter riders bootNo or<br>drag a rider here to assign him to his horse");
        column = new TableColumn(COLUMNINDEX_RIDER, 170, cellRenderer, null);
        addColumn(column);
        columnComparator.add(COLUMNINDEX_RIDER, new RideRiderBootNoComparator());

        columnTitles.add(COLUMNINDEX_TEAM, "Team");
        columnEditable.add(COLUMNINDEX_TEAM, Boolean.FALSE);
        cellRenderer = new DefaultTableCellRenderer();
        column = new TableColumn(COLUMNINDEX_TEAM, 90, cellRenderer, null);
        addColumn(column);
        columnComparator.add(COLUMNINDEX_TEAM, new RideTeamComparator());

        int colindex = COLUMNINDEX_1stRESULT;
        Result dummyResult = dummyRide.getResult();
        JTextField tf = new JTextField();
        tf.setHorizontalAlignment(JTextField.RIGHT);
        tf.setBorder(BorderFactory.createEmptyBorder());
        TableCellEditor cellEditor = new DefaultCellEditor(tf);
        for (int i = 0; i < dummyResult.noOfValues(); i++) {
            columnTitles.add(colindex, dummyResult.getDescription(i));
            columnEditable.add(colindex, Boolean.TRUE);
            cellRenderer = new DefaultTableCellRenderer();
            cellRenderer.setHorizontalAlignment(RIGHT);
            cellRenderer.setToolTipText("<html>enter result of ride here<br>'e' for eliminate<br>'r' for retired during ride<br>'n' no (team) ranking");
            column = new TableColumn(colindex, 57, cellRenderer, cellEditor);
            addColumn(column);
            columnComparator.add(colindex, new RideResultComparator());
            colindex++;
        }

        COLUMNINDEX_1stDIFF = colindex;
        for (int i = 0; i < dummyResult.noOfValues(); i++) {
            columnTitles.add(colindex, dummyResult.getDescription(i) + "Diff");
            columnEditable.add(colindex, Boolean.FALSE);
            cellRenderer = new DefaultTableCellRenderer();
            cellRenderer.setBackground(NO_EDIT_GRAY);
            cellRenderer.setHorizontalAlignment(RIGHT);
            cellRenderer.setToolTipText("difference to best rider on same horse");
            column = new TableColumn(colindex, 57, cellRenderer, null);
            addColumn(column);
            columnComparator.add(colindex, new RideDifferenceComparator());
            colindex++;
        }
    }

    //--------------------------------------------------------------------------
    // TableModel mapping the Ride objects in a Round to rows and columns

    public class Model extends AbstractTableModel {

        public int getColumnCount() {
            return columnTitles.size();
        }

        public int getRowCount() {
            return rnd.noOfRides();
        }

        public Object getValueAt(int row, int col) {
            Ride ride = rnd.getRide(row);
            switch (col) {
                case COLUMNINDEX_START:
                    return ride.startNo;
                case COLUMNINDEX_HORSENO:
                    return Fmt.toOrdinal(ride.riderOnHorse) + " on " + Fmt.propFontExtL(ride.getHorse().getHorseNo());
                case COLUMNINDEX_HORSE:
                    return ride.getHorse().name;
                case COLUMNINDEX_RIDER:
                    Rider rd = ride.getRider();
                    if (rd == Rider.notDefined) return "";
                    return ' ' + Fmt.propFontExtL(Integer.toString(rd.bootNo)) +
                            "  " + rd.firstName + ' ' + rd.getLastName();
                case COLUMNINDEX_TEAM:
                    return ride.getRider().getTeam().name;
                default:
                    Result result = ride.getResult();
                    if (COLUMNINDEX_1stRESULT <= col && col < COLUMNINDEX_1stDIFF)
                        return result.toString(col - COLUMNINDEX_1stRESULT);
                    if (COLUMNINDEX_1stDIFF <= col)
                        return result.diffToString(col - COLUMNINDEX_1stDIFF);
                    else return null;
            }
        }

        public String getColumnName(int col) {
            return columnTitles.get(col);
        }

        public boolean isCellEditable(int row, int col) {
            return columnEditable.get(col);
        }

        public void setValueAt(Object newValue, int row, int col) {
            Ride ride = rnd.getRide(row);
            String newVal = ((String) newValue).trim();
            switch (col) {
                case COLUMNINDEX_HORSE:
                    ride.getHorse().name = newVal;
                    break;
                case COLUMNINDEX_RIDER:
                    try {
                        int noEnd = newVal.indexOf(' ');
                        if (noEnd < 0) noEnd = newVal.length();
                        String sBootNo = newVal.substring(0, noEnd);
                        if (sBootNo.isEmpty()) {
                            ride.setRider(Rider.notDefined);
                        }
                        int iBootNo = Integer.parseInt(sBootNo);
                        Rider rider = Competition.findRiderByBootNo(iBootNo);

                        try {
                            if (!rnd.getDiscipline().getRound(rnd.getLevel() - 2).getQualifiedRiders().contains(rider)) {
                                int ignoreWarning = MessageWindow.showRiderNotInNextRound(rider,rnd);
                                System.out.println(ignoreWarning);
                                if (ignoreWarning == 0) {
                                    rider = null;
                                }
                            }
                        } catch (Exception ex) {
                            logger.warn(ex.getMessage(), ex);
                        }

                        if (rider != null && rider != ride.getRider()) {
                            setRider_CheckDuplicate(ride, rider, true);
                        }
                    } catch (NumberFormatException e) {
                        logger.warn(e.getMessage() ,e);
                    }
                    break;
                default:
                    Result result = ride.getResult();
                    if (COLUMNINDEX_1stRESULT <= col && col < COLUMNINDEX_1stDIFF) {
                        boolean resultAssigned = ride.getResult().set(col - COLUMNINDEX_1stRESULT, newVal);
                        if (resultAssigned) {
                            if (Competition.isCHU_GER()) {
                                checkSmallStyleResult(result, ride);
                            }
                            List<Ride> bestAndEqual = rnd.assignDiffsForHorse(ride.getHorse());
                            if (bestAndEqual.size() > 1) {
                                if (rnd.isLastRound()) {
                                    String msg = "Confirm no rider finished the final round?\n\n" +
                                            "If true, all riders of the round will be ranked last in this round.\n" +
                                            "If false, correct the result that one rider wins the round.";
                                    JOptionPane.showMessageDialog(MainWin.self, msg, "Warning", JOptionPane.WARNING_MESSAGE);
                                } else {
                                    String msg = bestAndEqual.size() + " rides with the best and equal result:\n\n";
                                    for (int i = 0; i < bestAndEqual.size(); i++) {
                                        Ride r = bestAndEqual.get(i);
                                        msg += r + "\n";
                                    }
                                    msg +=
                                            "\nModify the result of the eventually non-qualified rider(s) so that\n" +
                                                    "he/she/they will be ranked before all other non-qualified riders, i.e.\n" +
                                                    "their result is only slightly worse than that of the qualified rider.\n" +
                                                    "(See rules for \"best and equal results\")\n";
                                    JOptionPane.showMessageDialog(MainWin.self, msg, "Warning", JOptionPane.WARNING_MESSAGE);
                                }
                            }
                        }
                    }
                    if (col == COLUMNINDEX_1stDIFF) {
                        // r.setDiff((String)newVal);
                    }
            }
            //System.out.println(ride);
            fireTableDataChanged();
            MainWin.fileHandling.dataChanged();
        }
    }

    private void checkSmallStyleResult(Result result, Ride ride) {
        if (rnd.resultType.equals(Round.STYLE) && result.getState() == 0) {
            if (Double.parseDouble(result.toString()) < 3.5) {
                int selected = MessageWindow.showResultTooSmall(result);
                switch (selected) {
                    case 0:
                        ride.getResult().setState(1);
                        break; //elim_inated
                    case 1:
                        ride.getResult().setState(2);
                        break; //reti_red
                    default:
                        break;                             //apply initial result
                }
            }
        }
    }

    private boolean setRider_CheckDuplicate(Ride ride, Rider newRider, boolean askDeleteDuplicate) {
        if (newRider != Rider.notDefined) { // multiple undef always allowed
            Ride duplicate = rnd.findRider(newRider);
            if (duplicate != null) {
                if (askDeleteDuplicate) {
                    String msg = "Rider " + newRider.bootNo + " \"" + newRider.fullName() + "\" is already used at StartNo " + duplicate.startNo + " in " + rnd.name + "\n" +
                            "Delete the above occurrence and use rider at new position?";
                    int option = JOptionPane.showConfirmDialog(MainWin.self, msg, "Duplicate Rider in Round", JOptionPane.OK_CANCEL_OPTION);
                    if (option == JOptionPane.CANCEL_OPTION) return false;
                }
                // delete at old position
                duplicate.setRider(Rider.notDefined);
            }
        }
        ride.setRider(newRider);
        model.fireTableDataChanged();   // update old and new position
        MainWin.fileHandling.dataChanged();
        return true;
    }

    //--------------------------------------------------------------------------
    // drag n drop riders from/to a row in a RidesTable

    private DragSource dragSource;
    private DragGestureListener dragListener;
    private DropTarget dropTarget;
    private DropTargetListener dropListener;
    private RidesTable dragSourceTable;         // to see if source and target is same table

    private void initDragDropSupport() {

        dragSource = new DragSource();
        dragListener = new DragRiderListener();
        dragSource.createDefaultDragGestureRecognizer(this, DnDConstants.ACTION_MOVE, dragListener);
        dropListener = new DropRiderListener();
        dropTarget = new DropTarget(this, dropListener);
    }

    final class DragRiderListener implements DragGestureListener {

        /**
         * Of all drags started on a RidesTable Component only those in the
         * rider column will initiate a drag. All other column should result
         * in a normal row selection.
         */
        public void dragGestureRecognized(DragGestureEvent event) {

            int viewColumn = columnAtPoint(event.getDragOrigin());
            int modelColumn = convertColumnIndexToModel(viewColumn);
            if (modelColumn != COLUMNINDEX_RIDER) return;

            clearSelection();
            dragSourceTable = self;
            int sourceRow = rowAtPoint(event.getDragOrigin());
            Ride sourceRide = rnd.getRide(sourceRow);
            Rider rider = sourceRide.getRider();
            //System.out.println("Source: dragGestureRecognized:"+rider);
            if (rider == null) return;
            MainWin.dndObject = rider;
            StringSelection dummy = new StringSelection("");
            dragSource.startDrag(event, DragSource.DefaultMoveDrop, dummy, null);
        }
    }

    final class DropRiderListener extends DropTargetAdapter {

        /**
         * A rider has been dropped onto a RidesTable.
         * Assign that rider to a ride/horse.
         * An existing occurrence of the ride in that round is automatically
         * deleted if the drag originates in the same RidesTable.
         */
        public void drop(DropTargetDropEvent event) {

            Rider rider = (Rider) MainWin.dndObject;
            int targetRow = rowAtPoint(event.getLocation());
            Ride targetRide = rnd.getRide(targetRow);
            //System.out.println("    Target: drop  "+r+" -> row "+targetRow);
            event.getDropTargetContext().dropComplete(true);
            boolean askDeleteDuplicate = !(dragSourceTable == self);
            setRider_CheckDuplicate(targetRide, rider, askDeleteDuplicate);
            dragSourceTable = null;
        }
    }

    /*
    final class DragSourceListenerRT extends DefaultDragSourceListener {

        // what happens to the drag-source after drop
        public void dragDropEnd(DragSourceDropEvent event) {
            //System.out.println("Source: dargDropEnd");
            if(source==target) {    // same table: move / other round: copy
                sourceRide.setRider(Rider.notDefined);
                model.fireTableRowsUpdated(sourceRow,sourceRow);
            }
            setRowSelectionAllowed(true);
        }
    }
    */

    //--------------------------------------------------------------------------

    private void addMouseListenerToTableHeader() {

        setColumnSelectionAllowed(false);
        MouseAdapter ml = new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                TableColumnModel columnModel = getColumnModel();
                int viewColumn = columnModel.getColumnIndexAtX(e.getX());
                int sortColumn = convertColumnIndexToModel(viewColumn);
                if (e.getClickCount() == 1 && sortColumn != -1) {
                    NamedComparator comparator = (NamedComparator) columnComparator.get(sortColumn);
                    if (comparator == null) return;
                    logger.info("Sorting rides by " + comparator.getOrderName() + " ...");
                    rnd.sortRides(comparator);
                    model.fireTableDataChanged();
                }
            }
        };
        JTableHeader th = getTableHeader();
        th.setToolTipText("click to sort by this column");
        th.addMouseListener(ml);
    }

    private void initKeyBindings() {
        InputMap inputMap = self.getInputMap(WHEN_FOCUSED);
        ActionMap actionMap = self.getActionMap();

        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "delete");
        actionMap.put("delete", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int[] row = self.getSelectedRows();
                int[] col = self.getSelectedColumns();
                for (int i : row) {
                    for (int k : col) {
                        if (i >= 0 && k >= 0) {
                            int selRow = self.convertRowIndexToModel(i);
                            int selCol = self.convertColumnIndexToModel(k);
                            self.getModel().setValueAt("", selRow, selCol);
                        }
                    }
                }
            }
        });

    }

}


