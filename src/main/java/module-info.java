module aquilo {
    requires java.base;
    requires java.desktop;
    requires org.json;
    requires jdk.crypto.ec;
    requires org.semver4j;
    requires joptsimple;
    requires org.slf4j;
    requires org.tinylog.api;
    requires org.tinylog.api.slf4j;
    requires org.tinylog.impl;

    exports aquilo;
}